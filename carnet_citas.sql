/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 100137
 Source Host           : localhost:3306
 Source Schema         : carnet_citas

 Target Server Type    : MySQL
 Target Server Version : 100137
 File Encoding         : 65001

 Date: 14/08/2019 17:30:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for a_familiar
-- ----------------------------
DROP TABLE IF EXISTS `a_familiar`;
CREATE TABLE `a_familiar`  (
  `id_afamiliar` int(11) NOT NULL AUTO_INCREMENT,
  `id_expediente` int(11) NULL DEFAULT NULL,
  `obesidad` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `diabetes` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `hta` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `hipertrigliceridemia` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `hipercolesterolemia` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cancer` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_afamiliar`) USING BTREE,
  INDEX `id_expediente`(`id_expediente`) USING BTREE,
  CONSTRAINT `a_familiar_ibfk_1` FOREIGN KEY (`id_expediente`) REFERENCES `expedientes` (`id_expediente`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of a_familiar
-- ----------------------------
INSERT INTO `a_familiar` VALUES (11, 34, 'No', 'No', 'No', 'No', 'No', 'No');
INSERT INTO `a_familiar` VALUES (12, 35, 'Si', 'Si', 'Si', 'Si', 'Si', 'Si');
INSERT INTO `a_familiar` VALUES (13, 36, 'Si', 'No', 'No', 'Si', 'No', 'No');

-- ----------------------------
-- Table structure for a_ginecologico
-- ----------------------------
DROP TABLE IF EXISTS `a_ginecologico`;
CREATE TABLE `a_ginecologico`  (
  `id_aginecologicos` int(11) NOT NULL AUTO_INCREMENT,
  `id_expediente` int(11) NULL DEFAULT NULL,
  `embarazo` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `anticonceptivos` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `anticonceptivos_cual` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `anticonceptivos_dosis` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `climaterio` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `climaterio_fecha` date NULL DEFAULT NULL,
  `reemplazo_hormonal` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `reemplazo_hormonal_cual` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `reemplazo_hormonal_dosis` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_aginecologicos`) USING BTREE,
  INDEX `id_expediente`(`id_expediente`) USING BTREE,
  CONSTRAINT `a_ginecologico_ibfk_1` FOREIGN KEY (`id_expediente`) REFERENCES `expedientes` (`id_expediente`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of a_ginecologico
-- ----------------------------
INSERT INTO `a_ginecologico` VALUES (1, 34, '', '', '--', '--', 'No', '2019-08-14', 'No', '--', '--');
INSERT INTO `a_ginecologico` VALUES (2, 35, 'Si', 'No', 'no', 'no', 'No', '2019-08-15', 'No', 'no', 'no');
INSERT INTO `a_ginecologico` VALUES (3, 36, 'No', 'No', 'si', 'isi', 'Si', '2019-08-15', 'Si', 'si', 'si');

-- ----------------------------
-- Table structure for a_salud
-- ----------------------------
DROP TABLE IF EXISTS `a_salud`;
CREATE TABLE `a_salud`  (
  `id_a_salud` int(11) NOT NULL AUTO_INCREMENT,
  `id_expediente` int(11) NULL DEFAULT NULL,
  `diarrea` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `estrenimiento` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `gastritis` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ulcera` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pirosis` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `colitis` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `vomito` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `dentadura` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `otros` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `observaciones` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `enfermedad_diagnosticada` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ed_cuales` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `fecha_diagnostico` date NULL DEFAULT NULL,
  `t_medicamento` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `med_cual` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `dosis` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `laxante` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `diuretico` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `antiacido` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `analgesico` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cirugia` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `fecha_indigestion` date NULL DEFAULT NULL,
  PRIMARY KEY (`id_a_salud`) USING BTREE,
  INDEX `id_expediente`(`id_expediente`) USING BTREE,
  CONSTRAINT `a_salud_ibfk_1` FOREIGN KEY (`id_expediente`) REFERENCES `expedientes` (`id_expediente`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of a_salud
-- ----------------------------
INSERT INTO `a_salud` VALUES (9, 34, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', '--', 'ninguna', 'No', '--', '2019-08-14', 'no', '--', '--', 'No', 'No', 'No', 'No', '', '2019-08-14');
INSERT INTO `a_salud` VALUES (10, 35, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', '--', 'todo perfectisimo', 'No', 'ninguna', '2019-08-15', 'no', 'no', 'no', 'No', 'No', 'No', 'No', 'no', '2019-08-15');
INSERT INTO `a_salud` VALUES (11, 36, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', '', 'si', 'No', 'si', '2019-08-15', 'si', 'si', 'isi', 'No', 'No', 'No', 'No', 'si', '2019-08-15');

-- ----------------------------
-- Table structure for consumo
-- ----------------------------
DROP TABLE IF EXISTS `consumo`;
CREATE TABLE `consumo`  (
  `id_consumo` int(11) NOT NULL AUTO_INCREMENT,
  `id_expediente` int(11) NULL DEFAULT NULL,
  `alcohol` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alcohol_frecuencia` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alcohol_cantidad` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tabaco` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tabaco_frecuencia` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tabaco_cantidad` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cafe` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cafe_frecuencia` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cafe_cantidad` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_consumo`) USING BTREE,
  INDEX `id_expediente`(`id_expediente`) USING BTREE,
  CONSTRAINT `consumo_ibfk_1` FOREIGN KEY (`id_expediente`) REFERENCES `expedientes` (`id_expediente`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of consumo
-- ----------------------------
INSERT INTO `consumo` VALUES (1, 34, 'No', '--', '--', 'No', '--', '--', 'No', '--', '--');
INSERT INTO `consumo` VALUES (2, 35, 'No', 'no', 'no', 'No', 'no', 'no', 'No', 'no', 'no');
INSERT INTO `consumo` VALUES (3, 36, 'No', 'sis', 'si', 'Si', 'si', 'is', 'Si', 'si', 'si');

-- ----------------------------
-- Table structure for coordinacion
-- ----------------------------
DROP TABLE IF EXISTS `coordinacion`;
CREATE TABLE `coordinacion`  (
  `id_coordinacion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `estado` tinyint(2) NULL DEFAULT 1,
  PRIMARY KEY (`id_coordinacion`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of coordinacion
-- ----------------------------
INSERT INTO `coordinacion` VALUES (1, 'villanueva', 1);

-- ----------------------------
-- Table structure for d_actividad
-- ----------------------------
DROP TABLE IF EXISTS `d_actividad`;
CREATE TABLE `d_actividad`  (
  `id_dactividades` int(11) NOT NULL AUTO_INCREMENT,
  `id_expediente` int(11) NULL DEFAULT NULL,
  `actividad` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ejercicio_tipo` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ejercicio_frecuencia` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ejercicio_duracion` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ejercicio_cuando_inicio` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_dactividades`) USING BTREE,
  INDEX `id_expediente`(`id_expediente`) USING BTREE,
  CONSTRAINT `d_actividad_ibfk_1` FOREIGN KEY (`id_expediente`) REFERENCES `expedientes` (`id_expediente`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of d_actividad
-- ----------------------------
INSERT INTO `d_actividad` VALUES (1, 34, 'muy ligera', '--', '--', '--', '2019-08-14');
INSERT INTO `d_actividad` VALUES (2, 35, 'muy ligera', 'no', 'no', 'no', '2019-08-15');
INSERT INTO `d_actividad` VALUES (3, 36, 'muy ligera', 'si', 'si', 'si', '2019-08-15');

-- ----------------------------
-- Table structure for events
-- ----------------------------
DROP TABLE IF EXISTS `events`;
CREATE TABLE `events`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `start_event` datetime(0) NOT NULL,
  `end_event` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of events
-- ----------------------------
INSERT INTO `events` VALUES (3, 'hola', '2019-07-10 08:00:00', '2019-07-10 08:30:00');
INSERT INTO `events` VALUES (4, 'hola', '2019-07-11 10:00:00', '2019-07-11 16:30:00');

-- ----------------------------
-- Table structure for expedientes
-- ----------------------------
DROP TABLE IF EXISTS `expedientes`;
CREATE TABLE `expedientes`  (
  `id_expediente` int(11) NOT NULL AUTO_INCREMENT,
  `id_persona` int(11) NULL DEFAULT NULL,
  `expediente` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `fecha_expediente` date NULL DEFAULT NULL,
  `motivo_consulta` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_expediente`) USING BTREE,
  INDEX `id_persona`(`id_persona`) USING BTREE,
  CONSTRAINT `expedientes_ibfk_1` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id_persona`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of expedientes
-- ----------------------------
INSERT INTO `expedientes` VALUES (34, 3, '1', '2019-08-14', 'dolor de cabeza');
INSERT INTO `expedientes` VALUES (35, 4, '2', '2019-08-15', 'dolor de vientre');
INSERT INTO `expedientes` VALUES (36, 3, '3', '2019-08-15', 'zxcvbnm');

-- ----------------------------
-- Table structure for hospital
-- ----------------------------
DROP TABLE IF EXISTS `hospital`;
CREATE TABLE `hospital`  (
  `id_hospital` int(255) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `estado` tinyint(2) NULL DEFAULT 1,
  PRIMARY KEY (`id_hospital`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of hospital
-- ----------------------------
INSERT INTO `hospital` VALUES (3, 'santa clara', 1);
INSERT INTO `hospital` VALUES (4, '5 febrero', 1);

-- ----------------------------
-- Table structure for idieteticos
-- ----------------------------
DROP TABLE IF EXISTS `idieteticos`;
CREATE TABLE `idieteticos`  (
  `id_dieteticos` int(11) NOT NULL AUTO_INCREMENT,
  `id_expediente` int(11) NULL DEFAULT NULL,
  `comidasxdia` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `desayuno` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `desayuno_colacion` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `comida` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `colacion` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cena` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `quien_prepara_alimentos` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `come_entre_comidas` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `come_entre_comidas_que` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `modificacion_alimentos` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `modificacion_alimentos_porque` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `modificacion_alimentos_como` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `apetito` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `hora_mas_hambre` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alimentos_preferidos` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alimentos_agradan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alimentos_malestar` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alergia_alimentaria` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alergia_alimentaria_cual` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `intolerancia_alimentaria` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `intolerancia_alimentaria_cual` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `toma_suplemento` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `toma_suplemento_cual` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `toma_suplemento_dosis` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `toma_suplemento_porque` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `toma_suplemento_varia` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `toma_suplemento_varia_porque` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `agrega_sal` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `grasa_preparar_comida` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `dieta_especial` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tipo_dieta` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tipo_dieta_hace_cuando` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tipo_dieta_cuanto_tiempo` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tipo_dieta_razon` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tipo_dieta_apego` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tipo_dieta_resultados` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `medicamentos_baja_peso` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `medicamentos_baja_peso_cuales` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_dieteticos`) USING BTREE,
  INDEX `id_expediente`(`id_expediente`) USING BTREE,
  CONSTRAINT `idieteticos_ibfk_1` FOREIGN KEY (`id_expediente`) REFERENCES `expedientes` (`id_expediente`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of idieteticos
-- ----------------------------
INSERT INTO `idieteticos` VALUES (1, 34, '3 veces', 'huevo', '1', 'cerdo', '1', 'cafe y pan', 'yo', 'si', 'queso', 'No', '--', '--', 'regular', '1:00', '--', 'Verdura', 'verduras', 'ninguna', '--', 'ninguna', '--', 'No', '--', '--', '--', 'no', '--', 'No', 'vegetal', 'no', 'ninguna', 'nunca', 'nunca', 'nunca', 'nunca', 'no', 'nunca', 'ninguno');
INSERT INTO `idieteticos` VALUES (2, 35, 'no', 'no', 'no', 'n', 'on', 'o', 'on', 'on', 'o', 'No', 'o', 'non', 'bueno', 'on', 'oo', 'o', 'o', 'n', '', 'o', 'on', 'No', 'nono', 'on', 'ono', 'ono', 'no', 'No', 'no', 'no', 'no', 'n', 'on', 'non', 'no', 'non', 'no', 'no');
INSERT INTO `idieteticos` VALUES (3, 36, 'si', 'isi', 'si', 'isi', 'iis', 'is', 'isis', 'isi', 'is', 'No', 'sis', 'i', 'bueno', 'sis', 'i', 'isi', 'si', 'sis', 'i', 'sis', 'iis', 'No', 'is', 'is', 'ii', 'si', 'sis', 'No', 'isis', 'is', 'i', 'sis', 'is', 'iis', '', 'isis', 'i', 'sis');

-- ----------------------------
-- Table structure for patologia
-- ----------------------------
DROP TABLE IF EXISTS `patologia`;
CREATE TABLE `patologia`  (
  `id_patologia` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `estado` tinyint(2) NULL DEFAULT 1,
  PRIMARY KEY (`id_patologia`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of patologia
-- ----------------------------
INSERT INTO `patologia` VALUES (4, 'diabtes', 1);

-- ----------------------------
-- Table structure for permisos
-- ----------------------------
DROP TABLE IF EXISTS `permisos`;
CREATE TABLE `permisos`  (
  `id_permiso` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_permiso`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of permisos
-- ----------------------------
INSERT INTO `permisos` VALUES (1, 'Paciente');
INSERT INTO `permisos` VALUES (2, 'Medico');
INSERT INTO `permisos` VALUES (3, 'Patologia');
INSERT INTO `permisos` VALUES (4, 'Hospital');
INSERT INTO `permisos` VALUES (5, 'Coordinacion');
INSERT INTO `permisos` VALUES (6, 'Carnet');
INSERT INTO `permisos` VALUES (7, 'Citas');
INSERT INTO `permisos` VALUES (8, 'Reporte de Ventas');
INSERT INTO `permisos` VALUES (9, 'Usuarios');
INSERT INTO `permisos` VALUES (10, 'Empresa');

-- ----------------------------
-- Table structure for persona
-- ----------------------------
DROP TABLE IF EXISTS `persona`;
CREATE TABLE `persona`  (
  `id_persona` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_persona` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nombre` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `edad` int(11) NULL DEFAULT NULL,
  `sexo` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `fecha_nacimiento` date NULL DEFAULT NULL,
  `estado_civil` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `escolaridad` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ocupacion` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `direccion` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `telefono` int(11) NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `estado` tinyint(2) NULL DEFAULT 1,
  PRIMARY KEY (`id_persona`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of persona
-- ----------------------------
INSERT INTO `persona` VALUES (3, 'paciente', 'eduan alfredo villegas ruiz', 23, 'hombre', '1995-11-24', 'soltero', 'Licenciatura', 'Desarrollo de software', 'Colonia 5 febrero', 1234567890, 'eduan@gmail.com', 1);
INSERT INTO `persona` VALUES (4, 'paciente', 'ana gabriela cardenas gomez', 22, 'mujer', '2019-07-24', 'casado', 'universidad', 'profesora', 'Colonia 5 febrero', 1234667890, 'anagabriela@mail.com', 1);
INSERT INTO `persona` VALUES (6, 'medico', 'dr. ana', 22, 'mujer', '2019-07-01', 'casado', 'Licenciatura', 'profesora', 'Conocido', 2147483647, 'anagabriela@mail.com', 1);

-- ----------------------------
-- Table structure for seguimiento
-- ----------------------------
DROP TABLE IF EXISTS `seguimiento`;
CREATE TABLE `seguimiento`  (
  `id_seguimiento` int(11) NOT NULL AUTO_INCREMENT,
  `id_persona` int(11) NULL DEFAULT NULL,
  `fecha` date NULL DEFAULT NULL,
  `peso` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `talla` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cintura` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cadera` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `imc` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_seguimiento`) USING BTREE,
  INDEX `id_persona`(`id_persona`) USING BTREE,
  CONSTRAINT `seguimiento_ibfk_1` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id_persona`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for usuario_permiso
-- ----------------------------
DROP TABLE IF EXISTS `usuario_permiso`;
CREATE TABLE `usuario_permiso`  (
  `id_usuario_permiso` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `id_permiso` int(11) NOT NULL,
  PRIMARY KEY (`id_usuario_permiso`) USING BTREE,
  INDEX `fk_usuario_permiso_usuario_idx`(`id_usuario`) USING BTREE,
  INDEX `fk_usuario_permiso_permiso_idx`(`id_permiso`) USING BTREE,
  CONSTRAINT `fk_usuario_permiso_permiso` FOREIGN KEY (`id_permiso`) REFERENCES `permisos` (`id_permiso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_permiso_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 188 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of usuario_permiso
-- ----------------------------
INSERT INTO `usuario_permiso` VALUES (32, 2, 1);
INSERT INTO `usuario_permiso` VALUES (33, 2, 2);
INSERT INTO `usuario_permiso` VALUES (110, 1, 1);
INSERT INTO `usuario_permiso` VALUES (111, 1, 2);
INSERT INTO `usuario_permiso` VALUES (112, 1, 3);
INSERT INTO `usuario_permiso` VALUES (113, 1, 4);
INSERT INTO `usuario_permiso` VALUES (114, 1, 5);
INSERT INTO `usuario_permiso` VALUES (115, 1, 6);
INSERT INTO `usuario_permiso` VALUES (116, 1, 7);
INSERT INTO `usuario_permiso` VALUES (117, 1, 8);
INSERT INTO `usuario_permiso` VALUES (118, 1, 9);
INSERT INTO `usuario_permiso` VALUES (119, 1, 10);
INSERT INTO `usuario_permiso` VALUES (178, 3, 1);
INSERT INTO `usuario_permiso` VALUES (179, 3, 2);
INSERT INTO `usuario_permiso` VALUES (180, 3, 3);
INSERT INTO `usuario_permiso` VALUES (181, 3, 4);
INSERT INTO `usuario_permiso` VALUES (182, 3, 5);
INSERT INTO `usuario_permiso` VALUES (183, 3, 6);
INSERT INTO `usuario_permiso` VALUES (184, 3, 7);
INSERT INTO `usuario_permiso` VALUES (185, 3, 8);
INSERT INTO `usuario_permiso` VALUES (186, 3, 9);
INSERT INTO `usuario_permiso` VALUES (187, 3, 10);

-- ----------------------------
-- Table structure for usuarios
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios`  (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `apellidos` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `cedula` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `telefono` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `correo` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `direccion` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `cargo` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `usuario` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password2` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `fecha_ingreso` date NOT NULL,
  `estado` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_usuario`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of usuarios
-- ----------------------------
INSERT INTO `usuarios` VALUES (1, 'eyter', 'higuera', '1', '123456', 'eyter@gmail.com', 'san jose', '1', 'daniel', 'Qw/*12345678', 'Qw/*12345678', '2017-12-26', '1');
INSERT INTO `usuarios` VALUES (2, 'alejandro', 'perez', '12', '12968566', 'alejandro@gmail.com', 'california', '0', 'alejandro', 'Qw/*12345678', 'Qw/*12345678', '2018-02-13', '1');
INSERT INTO `usuarios` VALUES (3, 'Eduan', 'Villegas Ruiz', '88', '9621881868', 'eduan@gmail.com', '16 de septiembre', '1', 'eduan', '12345678', '12345678', '2019-07-04', '1');

-- ----------------------------
-- Procedure structure for sp_Generar_Codigo
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_Generar_Codigo`;
delimiter ;;
CREATE PROCEDURE `sp_Generar_Codigo`(idpersona INT, fecha_exp DATE,motivo VARCHAR(255))
BEGIN
	DECLARE contador INT;
	
	SET contador = (SELECT MAX(ex.expediente)+1 AS expediente FROM expedientes ex );#expediente siguiente
	
	IF contador IS NULL THEN
	 SET contador=1;
END IF;

	INSERT INTO expedientes(id_persona, expediente, fecha_expediente, motivo_consulta) 
  VALUES (idpersona,contador,fecha_exp,motivo);
	SELECT id_expediente,expediente from expedientes where expediente=contador;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
