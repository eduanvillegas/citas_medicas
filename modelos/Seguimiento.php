<?php

require_once("../config/conexion.php");

class Seguimiento extends Conectar
{

    public function get_filas_seguimiento()
    {

        $conectar = parent::conexion();

        $sql = "select * from ";

        $sql = $conectar->prepare($sql);

        $sql->execute();

        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        return $sql->rowCount();
    }


    //método para seleccionar registros

    public function get_seguimiento()
    {

        $conectar = parent::conexion();
        parent::set_names();

        $sql = "select * from seguimiento";

        $sql = $conectar->prepare($sql);
        $sql->execute();

        return $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
    }

    //método para mostrar los datos de un registro a modificar
    public function get_seguimiento_por_id($id_seguimiento)
    {
        $conectar = parent::conexion();
        parent::set_names();

        $sql = "select * from seguimiento where id_seguimiento=?";

        $sql = $conectar->prepare($sql);

        $sql->bindValue(1, $id_seguimiento);
        $sql->execute();
        return $resultado = $sql->fetchAll();
    }


    //método para insertar registros

    public function registrar_seguimiento($id_persona, $id_seguimiento, $fecha, $peso, $talla, $cintura, $cadera, $imc)
    {
        $conectar = parent::conexion();
        parent::set_names();

        $sql = "INSERT INTO `seguimiento`(`id_persona`, `fecha`, `peso`, `talla`, `cintura`, `cadera`, `imc`) 
        VALUES (?,?,?,?,?,?,?)";

        //echo $sql;

        $sql = $conectar->prepare($sql);

        $sql->bindValue(1, $id_persona);
        $sql->bindValue(2, $fecha);
        $sql->bindValue(3, $peso);
        $sql->bindValue(4, $talla);
        $sql->bindValue(5, $cintura);
        $sql->bindValue(6, $cadera);
        $sql->bindValue(7, $imc);
        $sql->execute();

        //print_r($_POST);

    }

    public function editar_seguimiento($id_persona,$id_seguimiento,$fecha,$peso,$talla,$cintura,$cadera,$imc)
    {
        $conectar = parent::conexion();
        parent::set_names();

        $sql = "UPDATE `seguimiento` SET
        `id_persona`=?,
        `fecha`=?,
        `peso`=?,
        `talla`=?,
        `cintura`=?,
        `cadera`=?,
        `imc`=?
        WHERE `id_seguimiento`=?";

        $sql = $conectar->prepare($sql);

        $sql->bindValue(1, $id_persona);
        $sql->bindValue(2, $fecha);
        $sql->bindValue(3, $peso);
        $sql->bindValue(4, $talla);
        $sql->bindValue(5, $cintura);
        $sql->bindValue(6, $cadera);
        $sql->bindValue(7, $imc);
        $sql->bindValue(8, $id_seguimiento);
        $sql->execute();
    }


    //método para activar Y/0 desactivar el estado de el medico

    public function editar_estado($id_patologia, $estado)
    {

        $conectar = parent::conexion();

        //si el estado es igual a 0 entonces el estado cambia a 1
        //el parametro est se envia por via ajax
        if ($_POST["est"] == "0") {

            $estado = 1;
        } else {

            $estado = 0;
        }

        $sql = "update patologia set 
              estado=?
              where 
              id_patologia=?;";

        $sql = $conectar->prepare($sql);
        $sql->bindValue(1, $estado);
        $sql->bindValue(2, $id_patologia);
        $sql->execute();
    }


    //método si el medico existe en la base de datos

    public function get_nombre_seguimiento($nombre)
    {

        $conectar = parent::conexion();

        $sql = "select * from patologia where nombre=?";

        //echo $sql; exit();

        $sql = $conectar->prepare($sql);

        $sql->bindValue(1, $nombre);
        $sql->execute();

        //print_r($email); exit();

        return $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
    }

    //método para eliminar un registro
    public function eliminar_seguimiento($id_seguimiento)
    {
        $conectar = parent::conexion();
        $sql = "delete from seguimiento where id_seguimiento=?";
        $sql = $conectar->prepare($sql);
        $sql->bindValue(1, $id_seguimiento);
        $sql->execute();
        return $resultado = $sql->fetch();
    }
}
