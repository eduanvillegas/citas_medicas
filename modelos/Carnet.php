<?php

require_once("../config/conexion.php");

class Carnet extends Conectar{

    public function get_filas_patologia(){

        $conectar= parent::conexion();

        $sql="select * from patologia";

        $sql=$conectar->prepare($sql);

        $sql->execute();

        $resultado= $sql->fetchAll(PDO::FETCH_ASSOC);
        return $sql->rowCount();

    }


    //método para seleccionar registros

    public function get_carnet(){

        $conectar=parent::conexion();
        parent::set_names();

        $sql="select id_carnet,folio,fecha,p.nombre as paciente,m.nombre as medico,pa.nombre as patologia,h.nombre as hospital,co.nombre as coordinacion
        from carnet c
        inner join paciente p ON c.id_paciente=p.id_paciente 
        inner join medico m ON c.id_medico=m.id_medico
        inner join patologia pa ON c.id_patologia=pa.id_patologia
        inner join hospital h ON c.id_hospital=h.id_hospital
        inner join coordinacion co ON c.id_coordinacion=co.id_coordinacion;";

        $sql=$conectar->prepare($sql);
        $sql->execute();
        return $resultado=$sql->fetchAll(PDO::FETCH_ASSOC);
    }

    //método para mostrar los datos de un registro a modificar
    public function get_carnet_por_id($id_carnet){
        $conectar= parent::conexion();
        parent::set_names();
        $sql="select * from carnet where id_carnet=?";
        $sql=$conectar->prepare($sql);
        $sql->bindValue(1, $id_carnet);
        $sql->execute();
        return $resultado=$sql->fetchAll();
    } 


    //método para insertar registros

    public function registrar_carnet($folio,$id_paciente,$id_medico,$id_patologia,$id_hospital,$id_coordinacion,$fecha){
        $conectar= parent::conexion();
        parent::set_names();
        $sql="INSERT INTO carnet(folio, id_paciente, id_medico, id_patologia, id_hospital, id_coordinacion, fecha) 
        VALUES (?,?,?,?,?,?,?)";
        //echo $sql;
        //var_dump($sql);
        $sql=$conectar->prepare($sql);

        $sql->bindValue(1,$_POST["folio"]);
        $sql->bindValue(2,$_POST["id_paciente"]);
        $sql->bindValue(3,$_POST["id_medico"]);
        $sql->bindValue(4,$_POST["id_patologia"]);
        $sql->bindValue(5,$_POST["id_hospital"]);
        $sql->bindValue(6,$_POST["id_coordinacion"]);
        $sql->bindValue(7,$_POST["fecha"]);
        $sql->execute();
        //print_r($_POST["folio"]);
    }

    public function editar_carnet($id_carnet){

        $conectar=parent::conexion();
        parent::set_names();

        $sql="UPDATE carnet set
        folio=?,
        id_paciente=?,
        id_medico=?,
        id_patologia=?,
        id_hospital=?,
        id_coordinacion=?,
        fecha=?
        where id_carnet=?;";

        $sql=$conectar->prepare($sql);

        $sql->bindValue(1,$_POST["folio"]);
        $sql->bindValue(2,$_POST["id_paciente"]);
        $sql->bindValue(3,$_POST["id_medico"]);
        $sql->bindValue(4,$_POST["id_patologia"]);
        $sql->bindValue(5,$_POST["id_hospital"]);
        $sql->bindValue(6,$_POST["id_coordinacion"]);
        $sql->bindValue(7,$_POST["fecha"]);
        $sql->bindValue(8,$_POST["id_carnet"]);
        $sql->execute();
    }

    //método para activar Y/0 desactivar el estado de el medico

    public function editar_estado($id_patologia,$estado){

        $conectar=parent::conexion();

        //si el estado es igual a 0 entonces el estado cambia a 1
        //el parametro est se envia por via ajax
        if($_POST["est"]=="0"){

            $estado=1;

        } else {

            $estado=0;
        }

        $sql="update patologia set 
              estado=?
              where 
              id_patologia=?;";

        $sql=$conectar->prepare($sql);
        $sql->bindValue(1,$estado);
        $sql->bindValue(2,$id_patologia);
        $sql->execute();
        
    }


    //método si el medico existe en la base de datos

    public function get_nombre_carnet($id_carnet){
        $conectar=parent::conexion();
        $sql="select * from carnet where id_carnet=?";
        //echo $sql; exit();
        $sql=$conectar->prepare($sql);
        $sql->bindValue(1,$id_carnet);
        $sql->execute();
        //print_r($sql); exit();
        return $resultado=$sql->fetchAll(PDO::FETCH_ASSOC);
    }

    //método para eliminar un registro
    public function eliminar_carnet($id_carnet){
        $conectar=parent::conexion();
        $sql="delete from carnet where id_carnet=?";
        $sql=$conectar->prepare($sql);
        $sql->bindValue(1,$id_carnet);
        $sql->execute();
        return $resultado=$sql->fetch();
    }
}
