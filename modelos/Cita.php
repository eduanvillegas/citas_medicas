<?php

require_once("../config/conexion.php");

class Cita extends Conectar
{

    public function get_filas_cita()
    {

        $conectar = parent::conexion();

        $sql = "SELECT * from citas";

        $sql = $conectar->prepare($sql);

        $sql->execute();

        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        return $sql->rowCount();
    }

    public function get_filas_cita_pendientes()
    {

        $conectar = parent::conexion();

        $sql = "SELECT fecha_cita from citas WHERE fecha_cita>CURRENT_DATE";

        $sql = $conectar->prepare($sql);

        $sql->execute();

        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        return $sql->rowCount();
    }


    //método para seleccionar registros

    public function get_cita()
    {

        $conectar = parent::conexion();
        parent::set_names();

        $sql = "SELECT ca.folio as carnet,folio_citas,fecha_cita,oct_od,oct_oi,av_od,av_oi,aplicacion_od,aplicacion_oi,laser_od,laser_oi,comentario 
        FROM citas c 
        INNER JOIN carnet ca ON c.folio_carnet=ca.id_carnet";

        $sql = $conectar->prepare($sql);
        $sql->execute();

        return $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
    }

    //método para mostrar los datos de un registro a modificar
    public function get_cita_por_id($folio_cita)
    {
        $conectar = parent::conexion();
        parent::set_names();

        $sql = "SELECT ca.id_carnet,ca.folio as carnet,folio_citas,fecha_cita,oct_od,oct_oi,av_od,av_oi,aplicacion_od,aplicacion_oi,laser_od,laser_oi,comentario 
        FROM citas c 
        INNER JOIN carnet ca ON c.folio_carnet=ca.id_carnet
        where folio_citas=?";

        $sql = $conectar->prepare($sql);

        $sql->bindValue(1, $folio_cita);
        $sql->execute();
        return $resultado = $sql->fetchAll();
    }


    //método para insertar registros
    public function registrar_cita($id_carnet, $fecha, $oct_od, $oct_oi, $av_od, $av_oi, $ap_od, $ap_oi, $laser_od, $laser_oi, $comentarios)
    {
        $conectar = parent::conexion();
        parent::set_names();

        $sql = "INSERT INTO citas VALUES(null,?,?,?,?,?,?,?,?,?,?,?);";
        //var_dump($sql);
        $sql = $conectar->prepare($sql);

        $sql->bindValue(1, $_POST["id_carnet"]);
        $sql->bindValue(2, $_POST["fecha"]);
        $sql->bindValue(3, $_POST["oct_od"]);
        $sql->bindValue(4, $_POST["oct_oi"]);
        $sql->bindValue(5, $_POST["av_od"]);
        $sql->bindValue(6, $_POST["av_oi"]);
        $sql->bindValue(7, $_POST["ap_od"]);
        $sql->bindValue(8, $_POST["ap_oi"]);
        $sql->bindValue(9, $_POST["laser_od"]);
        $sql->bindValue(10, $_POST["laser_oi"]);
        $sql->bindValue(11, $_POST["comentarios"]);
        $sql->execute();
        //print_r($_POST);
    }

    public function editar_citas($folio_cita)
    {

        $conectar = parent::conexion();
        parent::set_names();

        $sql = "UPDATE citas SET 
        folio_carnet=?,
        fecha_cita=?,
        oct_od=?,
        oct_oi=?,
        av_od=?,
        av_oi=?,
        aplicacion_od=?,
        aplicacion_oi=?,
        laser_od=?,
        laser_oi=?,
        comentario=?
        WHERE folio_citas=?";

        $sql = $conectar->prepare($sql);

        $sql->bindValue(1, $_POST["id_carnet"]);
        $sql->bindValue(2, $_POST["fecha"]);
        $sql->bindValue(3, $_POST["oct_od"]);
        $sql->bindValue(4, $_POST["oct_oi"]);
        $sql->bindValue(5, $_POST["av_od"]);
        $sql->bindValue(6, $_POST["av_oi"]);
        $sql->bindValue(7, $_POST["ap_od"]);
        $sql->bindValue(8, $_POST["ap_oi"]);
        $sql->bindValue(9, $_POST["laser_od"]);
        $sql->bindValue(10, $_POST["laser_oi"]);
        $sql->bindValue(11, $_POST["comentarios"]);
        $sql->bindValue(12, $_POST["folio_citas"]);
        $sql->execute();
    }


    //método para activar Y/0 desactivar el estado de el medico

    public function editar_estado($id_coordinacion, $estado)
    {

        $conectar = parent::conexion();

        //si el estado es igual a 0 entonces el estado cambia a 1
        //el parametro est se envia por via ajax
        if ($_POST["est"] == "0") {

            $estado = 1;
        } else {

            $estado = 0;
        }

        $sql = "update coordinacion set 
              estado=?
              where 
              id_coordinacion=?;";

        $sql = $conectar->prepare($sql);
        $sql->bindValue(1, $estado);
        $sql->bindValue(2, $id_coordinacion);
        $sql->execute();
    }

    //método si el medico existe en la base de datos
    public function get_nombre_cita($folio_carnet, $fecha)
    {
        $conectar = parent::conexion();
        $sql = "select * from citas where folio_carnet=? and fecha_cita=? ";
        $sql = $conectar->prepare($sql);
        $sql->bindValue(1, $folio_carnet);
        $sql->bindValue(2, $fecha);
        $sql->execute();
        return $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
    }

    //método para eliminar un registro
    public function eliminar_cita($folio_cita)
    {
        $conectar = parent::conexion();
        $sql = "delete from citas where folio_citas=?;";
        $sql = $conectar->prepare($sql);
        $sql->bindValue(1, $folio_cita);
        $sql->execute();
        return $resultado = $sql->fetch();
    }

    public function get_citas_anio_actual()
    {
        $conectar = parent::conexion();
        parent::set_names();
        $sql = "SELECT YEAR(fecha_cita) as ano, MONTHname(fecha_cita) as mes, SUM(folio_carnet) as total FROM citas WHERE YEAR(fecha_cita)=YEAR(CURDATE()) GROUP BY MONTHname(fecha_cita) desc";
        $sql = $conectar->prepare($sql);
        $sql->execute();
        return $resultado = $sql->fetchAll();
    }


    public function get_citas_anio_actual_grafica()
    {
        $conectar = parent::conexion();
        parent::set_names();
        $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $sql = "SELECT  MONTHname(fecha_cita) as mes, SUM(folio_carnet) as total FROM citas WHERE YEAR(fecha_cita)=YEAR(CURDATE())  GROUP BY MONTHname(fecha_cita) desc";
        $sql = $conectar->prepare($sql);
        $sql->execute();
        $resultado = $sql->fetchAll();
        //recorro el array y lo imprimo
        foreach ($resultado as $row) {
            $mes = $output["mes"] = $meses[date("n", strtotime($row["mes"])) - 1];
            $p = $output["total"] = $row["total"];
            echo $grafica = "{name:'" . $mes . "', y:" . $p . "},";
        }
    }
  
    public function registrar_evento($title, $start, $end) {
        $conectar = parent::conexion();
        parent::set_names();

        $sql = "INSERT INTO `events`(`title`, `start_event`, `end_event`) 
        VALUES (?,?,?)";
        $sql = $conectar->prepare($sql);

        $sql->bindValue(1, $title);
        $sql->bindValue(2, $start);
        $sql->bindValue(3, $end);
        $sql->execute();
        //$sql->debugDumpParams();
        //$gsent->debugDumpParams();
        //echo $start_event . $end_event;

        //print_r($_POST);

    }
    public function load_evento()
    {
        $conectar = parent::conexion();
        parent::set_names();

        $sql = "SELECT * from events";

        $sql = $conectar->prepare($sql);
        $sql->execute();

        return $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
    }

    public function editar_evento($title, $start, $end, $id)
    {
        $conectar = parent::conexion();
        parent::set_names();
        $sql = "UPDATE events SET title=?,start_event=?,end_event=? WHERE id=?";
        $sql = $conectar->prepare($sql);
        $sql->bindValue(1, $title);
        $sql->bindValue(2, $start);
        $sql->bindValue(3, $end);
        $sql->bindValue(4, $id);
        $sql->execute();
    }

    public function eliminar_evento($id)
    {
        $conectar = parent::conexion();
        parent::set_names();
        $sql = "delete from events WHERE id=?";
        $sql = $conectar->prepare($sql);
        $sql->bindValue(1, $id);
        $sql->execute();
    }
}
