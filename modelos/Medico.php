<?php

require_once("../config/conexion.php");

class Medico extends Conectar{


    public function get_filas_medico(){

        $conectar= parent::conexion();

        $sql="select * from persona";

        $sql=$conectar->prepare($sql);

        $sql->execute();

        $resultado= $sql->fetchAll(PDO::FETCH_ASSOC);

        return $sql->rowCount();

    }


    //método para seleccionar registros

    public function get_medico(){

        $conectar=parent::conexion();
        parent::set_names();

        $sql="select * from persona where tipo_persona='medico'";

        $sql=$conectar->prepare($sql);
        $sql->execute();

        return $resultado=$sql->fetchAll(PDO::FETCH_ASSOC);
    }

    //método para mostrar los datos de un registro a modificar
    public function get_medico_por_id($id_medico){


        $conectar= parent::conexion();
        parent::set_names();

        $sql="select * from persona where id_persona=?";

        $sql=$conectar->prepare($sql);

        $sql->bindValue(1, $id_medico);
        $sql->execute();
        return $resultado=$sql->fetchAll();
    } 


    //método para insertar registros

    public function registrar_medico($medico,$edad,$sexo,$fecha,$est_civ,$escolaridad,$ocupacion,$direccion,$telefono,$correo){


        $conectar= parent::conexion();
        parent::set_names();

        $sql="INSERT INTO `persona`(`tipo_persona`,`nombre`, `edad`, `sexo`, `fecha_nacimiento`, `estado_civil`, `escolaridad`, `ocupacion`, `direccion`, `telefono`, `email`) 
        VALUES ('medico',?,?,?,?,?,?,?,?,?,?)";
        //echo $sql;
        $sql=$conectar->prepare($sql);
        $sql->bindValue(1,$medico);
        $sql->bindValue(2,$edad);
        $sql->bindValue(3,$sexo);
        $sql->bindValue(4,$fecha);
        $sql->bindValue(5,$est_civ);
        $sql->bindValue(6,$escolaridad);
        $sql->bindValue(7,$ocupacion);
        $sql->bindValue(8,$direccion);
        $sql->bindValue(9,$telefono);
        $sql->bindValue(10,$correo);
       
        $sql->execute();
        //print_r($_POST);
    }

    public function editar_medico($idmedico,$medico,$edad,$sexo,$fecha,$est_civ,$escolaridad,$ocupacion,$direccion,$telefono,$correo){

        $conectar=parent::conexion();
        parent::set_names();

        require_once("medicos.php");

        $medicos= new medicos();

        $sql="UPDATE `persona` SET 
        `nombre`=?,
        `edad`=?,
        `sexo`=?,
        `fecha_nacimiento`=?,
        `estado_civil`=?,
        `escolaridad`=?,
        `ocupacion`=?,
        `direccion`=?,
        `telefono`=?,
        `email`=?
        where `id_persona`=?";

        $sql=$conectar->prepare($sql);
        $sql->bindValue(1,$medico);
        $sql->bindValue(2,$edad);
        $sql->bindValue(3,$sexo);
        $sql->bindValue(4,$fecha);
        $sql->bindValue(5,$est_civ);
        $sql->bindValue(6,$escolaridad);
        $sql->bindValue(7,$ocupacion);
        $sql->bindValue(8,$direccion);
        $sql->bindValue(9,$telefono);
        $sql->bindValue(10,$correo);
        $sql->bindValue(11,$idmedico);
        $sql->execute();
        print_r($sql);
    }


    //método para activar Y/0 desactivar el estado de la categoria

    public function editar_estado($id_medico,$estado){

        $conectar=parent::conexion();

        //si el estado es igual a 0 entonces el estado cambia a 1
        //el parametro est se envia por via ajax
        if($_POST["est"]=="0"){

            $estado=1;

        } else {

            $estado=0;
        }

        $sql="update persona set 

              estado=?
              where 
              id_persona=?";

        $sql=$conectar->prepare($sql);
        $sql->bindValue(1,$estado);
        $sql->bindValue(2,$id_medico);
        $sql->execute();
    }


    //método si la categoria existe en la base de datos

    public function get_nombre_medico($medico){

        $conectar=parent::conexion();

        $sql="select * from persona where nombre=? and tipo_persona='medico'";

        //echo $sql; exit();

        $sql=$conectar->prepare($sql);

        $sql->bindValue(1,$medico);
        $sql->execute();

        //print_r($email); exit();

        return $resultado=$sql->fetchAll(PDO::FETCH_ASSOC);
    }


    //método para eliminar un registro
    public function eliminar_medico($id_medico){
        $conectar=parent::conexion();
        $sql="delete from persona where id_persona=?";
        $sql=$conectar->prepare($sql);
        $sql->bindValue(1,$id_medico);
        $sql->execute();

        return $resultado=$sql->fetch();
    }


    public function get_categoria_por_id_usuario($id_usuario){

        $conectar= parent::conexion();

        $sql="select * from categoria where id_usuario=?";

        $sql=$conectar->prepare($sql);

        $sql->bindValue(1, $id_usuario);
        $sql->execute();

        return $resultado= $sql->fetchAll(PDO::FETCH_ASSOC);
    }


    //consulta si el id_categoria tiene una compra asociada
    public function get_categoria_por_id_compras($id_medico){

        $conectar=parent::conexion();
        parent::set_names();

        $sql="select *
              from persona 
              where id_persona=?";

        $sql=$conectar->prepare($sql);
        $sql->bindValue(1,$id_medico);
        $sql->execute();
        return $resultado=$sql->fetchAll(PDO::FETCH_ASSOC);
    }


    //consulta si el id_categoria tiene un detalle_compra asociado
    public function get_categoria_por_id_detalle_compras($id_categoria){

        $conectar=parent::conexion();
        parent::set_names();


        $sql="select c.id_categoria,d.id_categoria
           from categoria c 

              INNER JOIN detalle_compras d ON c.id_categoria=d.id_categoria


              where c.id_categoria=?

              ";

        $sql=$conectar->prepare($sql);
        $sql->bindValue(1,$id_categoria);
        $sql->execute();

        return $resultado=$sql->fetchAll(PDO::FETCH_ASSOC);

    }



}


?>
