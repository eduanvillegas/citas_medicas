<?php

require_once("../config/conexion.php");

class Patologia extends Conectar{

    public function get_filas_patologia(){

        $conectar= parent::conexion();

        $sql="select * from patologia";

        $sql=$conectar->prepare($sql);

        $sql->execute();

        $resultado= $sql->fetchAll(PDO::FETCH_ASSOC);

        return $sql->rowCount();

    }


    //método para seleccionar registros

    public function get_patologia(){

        $conectar=parent::conexion();
        parent::set_names();

        $sql="select * from patologia";

        $sql=$conectar->prepare($sql);
        $sql->execute();

        return $resultado=$sql->fetchAll(PDO::FETCH_ASSOC);
    }

    //método para mostrar los datos de un registro a modificar
    public function get_patologia_por_id($id_patologia){
        $conectar= parent::conexion();
        parent::set_names();

        $sql="select * from patologia where id_patologia=?";

        $sql=$conectar->prepare($sql);

        $sql->bindValue(1, $id_patologia);
        $sql->execute();
        return $resultado=$sql->fetchAll();
    }


    //método para insertar registros

    public function registrar_patologia($nombre){
        $conectar= parent::conexion();
        parent::set_names();

        $sql="insert into patologia(nombre) 
           values (?);";

        //echo $sql;

        $sql=$conectar->prepare($sql);

        $sql->bindValue(1,$_POST["nombre"]);
        $sql->execute();

        //print_r($_POST);

    }

    public function editar_patologia($nombre){

        $conectar=parent::conexion();
        parent::set_names();

        $sql="update patologia set nombre=? where id_patologia=?;";

        $sql=$conectar->prepare($sql);

        $sql->bindValue(1,$_POST["nombre"]);
        $sql->bindValue(2,$_POST["id_patologia"]);

        $sql->execute();
    }


    //método para activar Y/0 desactivar el estado de el medico

    public function editar_estado($id_patologia,$estado){

        $conectar=parent::conexion();

        //si el estado es igual a 0 entonces el estado cambia a 1
        //el parametro est se envia por via ajax
        if($_POST["est"]=="0"){

            $estado=1;

        } else {

            $estado=0;
        }

        $sql="update patologia set 
              estado=?
              where 
              id_patologia=?;";

        $sql=$conectar->prepare($sql);
        $sql->bindValue(1,$estado);
        $sql->bindValue(2,$id_patologia);
        $sql->execute();
        
    }


    //método si el medico existe en la base de datos

    public function get_nombre_patologia($nombre){

        $conectar=parent::conexion();

        $sql="select * from patologia where nombre=?";

        //echo $sql; exit();

        $sql=$conectar->prepare($sql);

        $sql->bindValue(1,$nombre);
        $sql->execute();

        //print_r($email); exit();

        return $resultado=$sql->fetchAll(PDO::FETCH_ASSOC);
    }

    //método para eliminar un registro
    public function eliminar_patologia($id_patologia){
        $conectar=parent::conexion();
        $sql="delete from patologia where id_patologia=?";
        $sql=$conectar->prepare($sql);
        $sql->bindValue(1,$id_patologia);
        $sql->execute();
        return $resultado=$sql->fetch();
    }
}
?>
