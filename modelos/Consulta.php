<?php

require_once("../config/conexion.php");

class Consulta extends Conectar
{

    //método para mostrar los datos de un registro a modificar
    public function get_reporte_por_idpaciente($id)
    {
        $conectar = parent::conexion();
        parent::set_names();
        $sql = "select p.id_persona,p.nombre,p.edad,p.sexo,p.fecha_nacimiento,p.estado_civil,p.escolaridad,p.ocupacion,p.direccion,p.telefono,p.email,exp.id_expediente,exp.expediente,p.estado from persona p 
        INNER JOIN expedientes exp ON exp.id_persona=p.id_persona
        where p.id_persona=? ";
        $sql = $conectar->prepare($sql);
        $sql->bindValue(1, $id);
        $sql->execute();
        return $resultado = $sql->fetchAll();
        //return $sql;
    }
}
