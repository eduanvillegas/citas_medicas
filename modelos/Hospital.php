<?php

require_once("../config/conexion.php");

class Hospital extends Conectar{


    public function get_filas_hospital(){

        $conectar= parent::conexion();

        $sql="select * from hospital;";

        $sql=$conectar->prepare($sql);

        $sql->execute();

        $resultado= $sql->fetchAll(PDO::FETCH_ASSOC);

        return $sql->rowCount();

    }


    public function get_hospital(){

        $conectar=parent::conexion();
        parent::set_names();

        $sql="select * from hospital";

        $sql=$conectar->prepare($sql);
        $sql->execute();

        return $resultado=$sql->fetchAll(PDO::FETCH_ASSOC);
    }

    //método para mostrar los datos de un registro a modificar
    public function get_hospital_por_id($id_hospital){

        $conectar= parent::conexion();
        parent::set_names();

        $sql="select * from hospital where id_hospital=?";

        $sql=$conectar->prepare($sql);

        $sql->bindValue(1, $id_hospital);
        $sql->execute();
        return $resultado=$sql->fetchAll();
    } 


    //método para insertar registros

    public function registrar_hospital($nombre){

        $conectar= parent::conexion();
        parent::set_names();

        $sql="insert into hospital(nombre) 
           values (?);";

        //echo $sql;

        $sql=$conectar->prepare($sql);

        $sql->bindValue(1,$_POST["nombre"]);
        $sql->execute();
        //print_r($_POST);
    }

    public function editar_hospital($nombre){
        $conectar=parent::conexion();
        parent::set_names();

        $sql="update hospital set nombre=? where id_hospital=?;";

        $sql=$conectar->prepare($sql);

        $sql->bindValue(1,$_POST["nombre"]);
        $sql->bindValue(2,$_POST["id_hospital"]);

        $sql->execute();
    }


    //método para activar Y/0 desactivar el estado de el medico

    public function editar_estado($id_hospital,$estado){

        $conectar=parent::conexion();

        //si el estado es igual a 0 entonces el estado cambia a 1
        //el parametro est se envia por via ajax
        if($_POST["est"]=="0"){

            $estado=1;

        } else {

            $estado=0;
        }
        $sql="update hospital set 
              estado=?
              where 
              id_hospital=?;";
        $sql=$conectar->prepare($sql);
        $sql->bindValue(1,$estado);
        $sql->bindValue(2,$id_hospital);
        $sql->execute();
    }

    //método si el medico existe en la base de datos
    public function get_nombre_hospital($nombre){
        $conectar=parent::conexion();
        $sql="select * from hospital where nombre=?";
        $sql=$conectar->prepare($sql);
        $sql->bindValue(1,$nombre);
        $sql->execute();
        return $resultado=$sql->fetchAll(PDO::FETCH_ASSOC);
    }

    //método para eliminar un registro
    public function eliminar_hospital($id_hospital){
        $conectar=parent::conexion();
        $sql="delete from hospital where id_hospital=?;";
        $sql=$conectar->prepare($sql);
        $sql->bindValue(1,$id_hospital);
        $sql->execute();
        return $resultado=$sql->fetch();
    }
}


?>
