<?php

require_once("../config/conexion.php");

class Coordinacion extends Conectar{

    public function get_filas_coordinacion(){

        $conectar= parent::conexion();

        $sql="select * from coordinacion";

        $sql=$conectar->prepare($sql);

        $sql->execute();

        $resultado= $sql->fetchAll(PDO::FETCH_ASSOC);

        return $sql->rowCount();

    }


    //método para seleccionar registros

    public function get_coordinacion(){

        $conectar=parent::conexion();
        parent::set_names();

        $sql="select * from coordinacion;";

        $sql=$conectar->prepare($sql);
        $sql->execute();

        return $resultado=$sql->fetchAll(PDO::FETCH_ASSOC);
    }

    //método para mostrar los datos de un registro a modificar
    public function get_coordinacion_por_id($id_coordinacion){
        $conectar= parent::conexion();
        parent::set_names();

        $sql="select * from coordinacion where id_coordinacion=?";

        $sql=$conectar->prepare($sql);

        $sql->bindValue(1, $id_coordinacion);
        $sql->execute();
        return $resultado=$sql->fetchAll();
    } 


    //método para insertar registros

    public function registrar_coordinacion($nombre){
        $conectar= parent::conexion();
        parent::set_names();

        $sql="insert into coordinacion(nombre) 
           values (?);";

        //echo $sql;

        $sql=$conectar->prepare($sql);

        $sql->bindValue(1,$_POST["nombre"]);
        $sql->execute();

        //print_r($_POST);

    }

    public function editar_coordinacion($nombre){

        $conectar=parent::conexion();
        parent::set_names();

        $sql="update coordinacion set nombre=? where id_coordinacion=?;";

        $sql=$conectar->prepare($sql);

        $sql->bindValue(1,$_POST["nombre"]);
        $sql->bindValue(2,$_POST["id_coordinacion"]);

        $sql->execute();
    }


    //método para activar Y/0 desactivar el estado de el medico

    public function editar_estado($id_coordinacion,$estado){

        $conectar=parent::conexion();

        //si el estado es igual a 0 entonces el estado cambia a 1
        //el parametro est se envia por via ajax
        if($_POST["est"]=="0"){

            $estado=1;

        } else {

            $estado=0;
        }

        $sql="update coordinacion set 
              estado=?
              where 
              id_coordinacion=?;";

        $sql=$conectar->prepare($sql);
        $sql->bindValue(1,$estado);
        $sql->bindValue(2,$id_coordinacion);
        $sql->execute();
        
    }


    //método si el medico existe en la base de datos

    public function get_nombre_coordinacion($nombre){
        $conectar=parent::conexion();
        $sql="select * from coordinacion where nombre=?";
        $sql=$conectar->prepare($sql);
        $sql->bindValue(1,$nombre);
        $sql->execute();
        return $resultado=$sql->fetchAll(PDO::FETCH_ASSOC);
    }

    //método para eliminar un registro
    public function eliminar_coordinacion($id_coordinacion){
        $conectar=parent::conexion();
        $sql="delete from coordinacion where id_coordinacion=?;";
        $sql=$conectar->prepare($sql);
        $sql->bindValue(1,$id_coordinacion);
        $sql->execute();
        return $resultado=$sql->fetch();
    }
}
?>
