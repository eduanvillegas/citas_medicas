<?php

require_once("../config/conexion.php");

class Salud extends Conectar
{


    public function get_filas_salud()
    {

        $conectar = parent::conexion();

        $sql = "select * from hospital;";

        $sql = $conectar->prepare($sql);

        $sql->execute();

        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        return $sql->rowCount();
    }


    public function get_hospital()
    {

        $conectar = parent::conexion();
        parent::set_names();

        $sql = "select * from hospital";

        $sql = $conectar->prepare($sql);
        $sql->execute();

        return $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
    }

    //método para mostrar los datos de un registro a modificar
    public function get_hospital_por_id($id_hospital)
    {

        $conectar = parent::conexion();
        parent::set_names();

        $sql = "select * from hospital where id_hospital=?";

        $sql = $conectar->prepare($sql);

        $sql->bindValue(1, $id_hospital);
        $sql->execute();
        return $resultado = $sql->fetchAll();
    }

    public function registrar_expediente($id_persona,$expediente,$fechaExp,$observacionesExp)
    {
        $conectar = parent::conexion();
        parent::set_names();

        $sql = "CALL sp_Generar_Codigo(?,?,?);";

        $sql = $conectar->prepare($sql);

        $sql->bindValue(1, $id_persona);
        $sql->bindValue(2, $fechaExp);
        $sql->bindValue(3, $observacionesExp);
        $sql->execute();
        return $resultado=$sql->fetchAll();
        //$sql->debugDumpParams();
    }

    //método para insertar registros
    public function registrar_salud(
        $id_persona,
        $Ediarrea,
        $Eestrenimiento,
        $Egastritis,
        $Eulcera,
        $Epirosis,
        $Ecolitis,
        $Evomito,
        $Edentadura,
        $otros,
        $observaciones,
        $PadEnfermedad,
        $PadEnfermedadCual,
        $fechaDigestion,
        $Tmedicamento,
        $TmedicamentoCual,
        $dosis,
        $FechaIngestion,
        $laxante,
        $diuretico,
        $antiacido,
        $analgesico,
        $diurcirugiaetico
    ) {

        $conectar = parent::conexion();
        parent::set_names();

        $sql = "INSERT INTO `a_salud`( `id_expediente`, `diarrea`, `estrenimiento`, 
        `gastritis`, `ulcera`, `pirosis`, `colitis`,
         `vomito`, `dentadura`, `otros`, `observaciones`, `enfermedad_diagnosticada`, `ed_cuales`, 
         `fecha_diagnostico`,
          `t_medicamento`, `med_cual`, `dosis`, `laxante`, `diuretico`, `antiacido`, `analgesico`, `cirugia`, `fecha_indigestion`) 
        VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        //echo $sql;

        $sql = $conectar->prepare($sql);

        $sql->bindValue(1, $id_persona);
        $sql->bindValue(2, $Ediarrea);
        $sql->bindValue(3, $Eestrenimiento);
        $sql->bindValue(4, $Egastritis);
        $sql->bindValue(5, $Eulcera);
        $sql->bindValue(6, $Epirosis);
        $sql->bindValue(7, $Ecolitis);
        $sql->bindValue(8, $Evomito);
        $sql->bindValue(9, $Edentadura);
        $sql->bindValue(10, $otros);
        $sql->bindValue(11, $observaciones);
        $sql->bindValue(12, $PadEnfermedad);
        $sql->bindValue(13, $PadEnfermedadCual);
        $sql->bindValue(14, $fechaDigestion);
        $sql->bindValue(15, $Tmedicamento);
        $sql->bindValue(16, $TmedicamentoCual);
        $sql->bindValue(17, $dosis);
        $sql->bindValue(18, $laxante);
        $sql->bindValue(19, $diuretico);
        $sql->bindValue(20, $antiacido);
        $sql->bindValue(21, $analgesico);
        $sql->bindValue(22, $diurcirugiaetico);
        $sql->bindValue(23, $FechaIngestion);
        $sql->execute();
        $sql->debugDumpParams();
    }

    public function registrar_afamiliares( $id_persona,$obecidad, $diabetes, $hta, $hipertrigliceridemia, $hipercolesterolemia, $cancer)
    {

        $conectar = parent::conexion();
        parent::set_names();

        $sql = "INSERT INTO `a_familiar`(`id_expediente`, `obesidad`, `diabetes`, `hta`, `hipertrigliceridemia`, `hipercolesterolemia`, `cancer`) 
        VALUES (?,?,?,?,?,?,?)";

        //echo $sql;

        $sql = $conectar->prepare($sql);

        $sql->bindValue(1, $id_persona);
        $sql->bindValue(2, $obecidad);
        $sql->bindValue(3, $diabetes);
        $sql->bindValue(4, $hta);
        $sql->bindValue(5, $hipertrigliceridemia);
        $sql->bindValue(6, $hipercolesterolemia);
        $sql->bindValue(7, $cancer);
        $sql->execute();
        $sql->debugDumpParams();
    }

    public function registrar_aginecologicos(
        $id_persona,
        $embarazo,
        $anticonceptivos,
        $anticonceptivos_cual,
        $anticonceptivos_dosis,
        $climaterio,
        $climaterio_fecha,
        $terapia,
        $terapia_cual,
        $terapia_dosis
    ) {

        $conectar = parent::conexion();
        parent::set_names();

        $sql = "INSERT INTO `a_ginecologico`( `id_expediente`, `embarazo`, `anticonceptivos`, `anticonceptivos_cual`,
         `anticonceptivos_dosis`, `climaterio`, `climaterio_fecha`, `reemplazo_hormonal`,`reemplazo_hormonal_cual`,
          `reemplazo_hormonal_dosis`) 
        VALUES (?,?,?,?,?,?,?,?,?,?)";

        $sql = $conectar->prepare($sql);

        $sql->bindValue(1, $id_persona);
        $sql->bindValue(2, $embarazo);
        $sql->bindValue(3, $anticonceptivos);
        $sql->bindValue(4, $anticonceptivos_cual);
        $sql->bindValue(5, $anticonceptivos_dosis);
        $sql->bindValue(6, $climaterio);
        $sql->bindValue(7, $climaterio_fecha);
        $sql->bindValue(8, $terapia);
        $sql->bindValue(9, $terapia_cual);
        $sql->bindValue(10, $terapia_dosis);
        $sql->execute();
        $sql->debugDumpParams();
    }

    public function registrar_dactividades($id_persona, $actividad, $ejercicio_tipo, $ejercicio_frecuencia, $ejercicio_duracion, $ejercicio_cuando_inicio)
    {

        $conectar = parent::conexion();
        parent::set_names();

        $sql = "INSERT INTO `d_actividad`( `id_expediente`, `actividad`, `ejercicio_tipo`, `ejercicio_frecuencia`, `ejercicio_duracion`, `ejercicio_cuando_inicio`) 
        VALUES (?,?,?,?,?,?)";

        $sql = $conectar->prepare($sql);

        $sql->bindValue(1, $id_persona);
        $sql->bindValue(2, $actividad);
        $sql->bindValue(3, $ejercicio_tipo);
        $sql->bindValue(4, $ejercicio_frecuencia);
        $sql->bindValue(5, $ejercicio_duracion);
        $sql->bindValue(6, $ejercicio_cuando_inicio);
        $sql->execute();
        $sql->debugDumpParams();
    }


    public function registrar_consumo(
        $id_persona,
        $consumo_alcohol,
        $consumo_alcohol_frecuencia,
        $consumo_alcohol_cantidad,
        $consumo_tabaco,
        $consumo_tabaco_frecuencia,
        $consumo_tabaco_cantidad,
        $consumo_cafe,
        $consumo_cafe_frecuencia,
        $consumo_cafe_cantidad
    ) {

        $conectar = parent::conexion();
        parent::set_names();

        $sql = "INSERT INTO `consumo`( `id_expediente`, `alcohol`, `alcohol_frecuencia`, `alcohol_cantidad`, `tabaco`, `tabaco_frecuencia`, `tabaco_cantidad`, `cafe`, `cafe_frecuencia`, `cafe_cantidad`) 
        VALUES (?,?,?,?,?,?,?,?,?,?)";

        $sql = $conectar->prepare($sql);

        $sql->bindValue(1, $id_persona);
        $sql->bindValue(2, $consumo_alcohol);
        $sql->bindValue(3, $consumo_alcohol_frecuencia);
        $sql->bindValue(4, $consumo_alcohol_cantidad);
        $sql->bindValue(5, $consumo_tabaco);
        $sql->bindValue(6, $consumo_tabaco_frecuencia);
        $sql->bindValue(7, $consumo_tabaco_cantidad);
        $sql->bindValue(8, $consumo_cafe);
        $sql->bindValue(9, $consumo_cafe_frecuencia);
        $sql->bindValue(10, $consumo_cafe_cantidad);
        $sql->execute();
        $sql->debugDumpParams();
    }

    public function registrar_dieteticos(
        $id_persona,
        $comidasxdia,
        $desayuno,
        $desayuno_colacion,
        $comida,
        $comida_colacion,
        $cena,
        $quienprepaali,
        $comeEntreComidas,
        $QueComeEntreComidas,
        $modificacionAlimentos,
        $modificacionAlimentosPorque,
        $modificacionAlimentosComo,
        $modificacionAlimentosApetito,
        $horaHambre,
        $alimentospreferidos,
        $alimentosAgradan,
        $alimentosMalestar,
        $alergiaAlimentaria,
        $alergiaAlimentariaCual,
        $intoleranciaAlimentaria,
        $intoleranciaAlimentariaCual,
        $tomaSuplemento,
        $tomaSuplementoCual,
        $tomaSuplementoDosis,
        $tomaSuplementoPorque,
        $consumoVaria,
        $consumoVariaPorque,
        $agregaSalComida,
        $utilizanGrasa,
        $dietaEspecial,
        $tipoDieta,
        $tipoDietacuando,
        $tipoDietaTiempo,
        $tipoDietaRazon,
        $tipoDietaApego,
        $tipoDietaResultados,
        $medicamentoBajarPeso,
        $medicamentoBajarPesoCuales
    ) {

        $conectar = parent::conexion();
        parent::set_names();

        $sql = "INSERT INTO `idieteticos`(`id_expediente`, `comidasxdia`, `desayuno`, `desayuno_colacion`, `comida`, 
        `colacion`, `cena`, `quien_prepara_alimentos`, `come_entre_comidas`, `come_entre_comidas_que`, 
        `modificacion_alimentos`, `modificacion_alimentos_porque`, `modificacion_alimentos_como`, `apetito`, 
        `hora_mas_hambre`, `alimentos_preferidos`, `alimentos_agradan`, `alimentos_malestar`,
         `alergia_alimentaria`, `alergia_alimentaria_cual`, `intolerancia_alimentaria`, `intolerancia_alimentaria_cual`,
          `toma_suplemento`, `toma_suplemento_cual`, `toma_suplemento_dosis`, `toma_suplemento_porque`, `toma_suplemento_varia`, `toma_suplemento_varia_porque`, `agrega_sal`, `grasa_preparar_comida`, `dieta_especial`, `tipo_dieta`, `tipo_dieta_hace_cuando`, `tipo_dieta_cuanto_tiempo`, `tipo_dieta_razon`, `tipo_dieta_apego`, `tipo_dieta_resultados`, `medicamentos_baja_peso`, `medicamentos_baja_peso_cuales`) 
        VALUES (?,?,?,?,?,?,?,?,?,?,
        ?,?,?,?,?,?,?,?,?,?,
        ?,?,?,?,?,?,?,?,?,?,
        ?,?,?,?,?,?,?,?,?)";

        $sql = $conectar->prepare($sql);

        $sql->bindValue(1, $id_persona);
        $sql->bindValue(2, $comidasxdia);
        $sql->bindValue(3, $desayuno);
        $sql->bindValue(4, $desayuno_colacion);
        $sql->bindValue(5, $comida);
        $sql->bindValue(6, $comida_colacion);
        $sql->bindValue(7, $cena);
        $sql->bindValue(8, $quienprepaali);
        $sql->bindValue(9, $comeEntreComidas);
        $sql->bindValue(10, $QueComeEntreComidas);
        $sql->bindValue(11, $modificacionAlimentos);
        $sql->bindValue(12, $modificacionAlimentosPorque);
        $sql->bindValue(13, $modificacionAlimentosComo);
        $sql->bindValue(14, $modificacionAlimentosApetito);
        $sql->bindValue(15, $horaHambre);
        $sql->bindValue(16, $alimentospreferidos);
        $sql->bindValue(17, $alimentosAgradan);
        $sql->bindValue(18, $alimentosMalestar);
        $sql->bindValue(19, $alergiaAlimentaria);
        $sql->bindValue(20, $alergiaAlimentariaCual);
        $sql->bindValue(21, $intoleranciaAlimentaria);
        $sql->bindValue(22, $intoleranciaAlimentariaCual);
        $sql->bindValue(23, $tomaSuplemento);
        $sql->bindValue(24, $tomaSuplementoCual);
        $sql->bindValue(25, $tomaSuplementoDosis);
        $sql->bindValue(26, $tomaSuplementoPorque);
        $sql->bindValue(27, $consumoVaria);
        $sql->bindValue(28, $consumoVariaPorque);
        $sql->bindValue(29, $agregaSalComida);
        $sql->bindValue(30, $utilizanGrasa);
        $sql->bindValue(31, $dietaEspecial);
        $sql->bindValue(32, $tipoDieta);
        $sql->bindValue(33, $tipoDietacuando);
        $sql->bindValue(34, $tipoDietaTiempo);
        $sql->bindValue(35, $tipoDietaRazon);
        $sql->bindValue(36, $tipoDietaApego);
        $sql->bindValue(37, $tipoDietaResultados);
        $sql->bindValue(38, $medicamentoBajarPeso);
        $sql->bindValue(39, $medicamentoBajarPesoCuales);
        $sql->execute();
        $sql->debugDumpParams();
    }
}
