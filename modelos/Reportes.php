<?php

require_once("../../config/conexion.php");

class Reportes extends Conectar
{

    //método para mostrar los datos de un registro a modificar
    public function get_reporte_por_idpaciente($id)
    {
        $conectar = parent::conexion();
        parent::set_names();
        $sql = "select p.id_persona,p.nombre,p.edad,p.sexo,p.fecha_nacimiento,p.estado_civil,p.escolaridad,p.ocupacion,p.direccion,p.telefono,p.email,exp.id_expediente,exp.expediente,p.estado from persona p 
        INNER JOIN expedientes exp ON exp.id_persona=p.id_persona
        where p.id_persona=?";
        $sql = $conectar->prepare($sql);
        $sql->bindValue(1, $id);
        $sql->execute();
        return $resultado = $sql->fetchAll();
        //return $sql;
    }

      //método para mostrar los datos de un registro a modificar
      public function datos_personal_expediente($id)
      {
          $conectar = parent::conexion();
          parent::set_names();
          $sql = "select * from expedientes where id_expediente=?";
          $sql = $conectar->prepare($sql);
          $sql->bindValue(1, $id);
          $sql->execute();
          return $resultado = $sql->fetchAll();
          //return $sql;
      }
    //método para mostrar los datos de un registro a modificar
    public function get_reporte_salud($id)
    {
        $conectar = parent::conexion();
        parent::set_names();
        $sql = "select * from a_salud where id_expediente=?";
        $sql = $conectar->prepare($sql);
        $sql->bindValue(1, $id);
        $sql->execute();
        return $resultado = $sql->fetchAll();
    }

    //método para mostrar los datos de un registro a modificar
    public function get_reporte_familiar($id)
    {
        $conectar = parent::conexion();
        parent::set_names();
        $sql = "select * from a_familiar where id_expediente=?";
        $sql = $conectar->prepare($sql);
        $sql->bindValue(1, $id);
        $sql->execute();
        return $resultado = $sql->fetchAll();
    }

    //método para mostrar los datos de un registro a modificar
    public function get_reporte_ginecologico($id)
    {
        $conectar = parent::conexion();
        parent::set_names();
        $sql = "select * from a_ginecologico where id_expediente=?";
        $sql = $conectar->prepare($sql);
        $sql->bindValue(1, $id);
        $sql->execute();
        return $resultado = $sql->fetchAll();
    }

    //método para mostrar los datos de un registro a modificar
    public function get_reporte_vida($id)
    {
        $conectar = parent::conexion();
        parent::set_names();
        $sql = "select * from d_actividad where id_expediente=?";
        $sql = $conectar->prepare($sql);
        $sql->bindValue(1, $id);
        $sql->execute();
        return $resultado = $sql->fetchAll();
    }
    //método para mostrar los datos de un registro a modificar
    public function get_reporte_consumo($id)
    {
        $conectar = parent::conexion();
        parent::set_names();
        $sql = "select * from consumo where id_expediente=?";
        $sql = $conectar->prepare($sql);
        $sql->bindValue(1, $id);
        $sql->execute();
        return $resultado = $sql->fetchAll();
    }
      //método para mostrar los datos de un registro a modificar
      public function get_reporte_dieteticos($id)
      {
          $conectar = parent::conexion();
          parent::set_names();
          $sql = "select * from idieteticos where id_expediente=?";
          $sql = $conectar->prepare($sql);
          $sql->bindValue(1, $id);
          $sql->execute();
          return $resultado = $sql->fetchAll();
      }
}
