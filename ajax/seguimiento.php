<?php
//llamo a la conexion de la base de datos 
require_once("../config/conexion.php");
//llamo al modelo patologia
require_once("../modelos/Seguimiento.php");

$seguimiento = new Seguimiento();


//declaramos las variables de los valores que se envian por el formulario y que recibimos por ajax y decimos que si existe el parametro que estamos recibiendo

//los valores vienen del atributo name de los campos del formulario
/*el valor id_usuario y id_medico se carga en el campo hidden cuando se edita un registro*/
//se copian los campos de la tabla medico
$id_seguimiento=isset($_POST["id_seguimiento"]) ? $_POST['id_seguimiento'] : "";
$id_persona=isset($_POST["id_persona"]) ? $_POST['id_persona'] : "";
$peso=isset($_POST["peso"]) ? $_POST['peso'] : "";
$talla=isset($_POST["talla"]) ? $_POST['talla'] : "";
$cintura=isset($_POST["cintura"]) ? $_POST['cintura'] : "";
$cadera=isset($_POST["cadera"]) ? $_POST['cadera'] : "";
$imc=isset($_POST["imc"]) ? $_POST['imc'] : "";
$fecha=isset($_POST["fecha"]) ? $_POST['fecha'] : "";
switch($_GET["op"]){


    case "guardaryeditar":

        /*si el id no existe entonces lo registra
	           importante: se debe poner el $_POST sino no funciona*/
        if(empty($_POST["id_patologia"])){
            /*verificamos si existe el medico en la base de datos, si ya existe un registro con el medico entonces no se registra*/
            //importante: se debe poner el $_POST sino no funciona
                $seguimiento->registrar_seguimiento($id_persona,$id_seguimiento,$fecha,$peso,$talla,$cintura,$cadera,$imc);
                echo $seguimiento ? "<script> swal({
                title: '¡Bien Hecho!',
                text: 'Seguimiento Antropométrico registrado correctamente',
                type: 'success',
                });</script>" :

                "<script> swal({
                title: '¡ERROR!',
                text: 'Ocurrio un error al registrar Seguimiento Antropométrico',
                type: 'error',
                });</script>";
        }//cierre de empty
        else {
            /*si ya existe entonces editamos el medico*/
            $seguimiento->editar_seguimiento($id_persona,$id_seguimiento,$fecha,$peso,$talla,$cintura,$cadera,$imc);
            echo $seguimiento ? "<script> swal({
                title: '¡Bien Hecho!',
                text: 'La patologia se edito correctamente',
                type: 'success',
                });</script>" :

            "<script> swal({
                title: '¡ERROR!',
                text: 'Ocurrio un error al editar la patologia',
                type: 'error',
                });</script>";
        }
        break;


    case 'mostrar':
        //el parametro id_medico se envia por AJAX cuando se edita la medico
        $datos=$seguimiento->get_seguimiento_por_id($_POST["id_seguimiento"]);
        foreach($datos as $row)
        {
            $output["id_seguimiento"] = $row["id_seguimiento"];
            $output["id_persona"] = $row["id_persona"];
             $output["fecha"] = $row["fecha"];
             $output["peso"] = $row["peso"];
             $output["talla"] = $row["talla"];
             $output["cintura"] = $row["cintura"];
             $output["cadera"] = $row["cadera"];
             $output["imc"] = $row["imc"];
        }//cierre el else
        echo json_encode($output);
        break;

    case "activarydesactivar":
        //los parametros id_medico y est vienen por via ajax
        $datos=$patologias->get_patologia_por_id($_POST["id_patologia"]);

        // si existe el id de la medico entonces recorre el array
        if(is_array($datos)==true and count($datos)>0){

            //edita el estado de la medico
            $patologias->editar_estado($_POST["id_patologia"],$_POST["est"]);
            echo $patologias ? "<script> swal({
                title: '¡Bien Hecho!',
                text: 'La patologia cambio de estado correctamente',
                type: 'success',
                });</script>" :

            "<script> swal({
                title: '¡ERROR!',
                text: 'Ocurrio un error al cambiar de estado',
                type: 'error',
                });</script>";
            //edita el estado del producto
            //$productos->editar_estado_producto_por_categoria($_POST["id_pacinete"],$_POST["est"]);
        } 
        break;


    case "listar":

        $datos=$seguimiento->get_seguimiento();

        //Vamos a declarar un array
        $data= Array();

        foreach($datos as $row)
        {
            $sub_array = array();

            $sub_array[] = $row["fecha"];
            $sub_array[] = $row["peso"];
            $sub_array[] = $row["talla"];
            $sub_array[] = $row["cintura"];
            $sub_array[] = $row["cadera"];
            $sub_array[] = $row["imc"];
            $sub_array[] =
                '<button type="button" onClick="mostrar('.$row["id_seguimiento"].');"  id="'.$row["id_seguimiento"].'" class="btn btn-warning btn-md update"><i class="glyphicon glyphicon-edit"></i> </button>'.

                '<button type="button" onClick="eliminar('.$row["id_seguimiento"].');"  id="'.$row["id_seguimiento"].'" class="btn btn-danger btn-md"><i class="glyphicon glyphicon-trash"></i> </button>';

            $data[] = $sub_array;
        }
        $results = array(
            "sEcho"=>1, //Información para el datatables
            "iTotalRecords"=>count($data), //enviamos el total registros al datatable
            "iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
            "aaData"=>$data);
        echo json_encode($results);

        break;

    case "eliminar_seguimiento":

        //verificamos si la medico existe en la base de datos en la tabla categoria, si existe entonces lo elimina

        $datos= $seguimiento->get_seguimiento_por_id($_POST["id_seguimiento"]);

        if(is_array($datos)==true and count($datos)>0){

            $seguimiento->eliminar_seguimiento($_POST["id_seguimiento"]);
            echo $seguimiento ? "<script> swal({
                title: '¡Bien Hecho!',
                text: 'Patologia se elimino correctamente',
                type: 'success',
                });</script>" :

            "<script> swal({
                title: '¡ERROR!',
                text: 'Ocurrio un error al eliminar la patologia',
                type: 'error',
                });</script>";
        }
        break;
}
?>
