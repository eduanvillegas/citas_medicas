<?php
//llamo a la conexion de la base de datos 
require_once("../config/conexion.php");
//llamo al modelo patologia
require_once("../modelos/Carnet.php");
$carnets = new Carnet();
//declaramos las variables de los valores que se envian por el formulario y que recibimos por ajax y decimos que si existe el parametro que estamos recibiendo
//los valores vienen del atributo name de los campos del formulario
/*el valor id_usuario y id_medico se carga en el campo hidden cuando se edita un registro*/
//se copian los campos de la tabla medico
$id_carnet=isset($_POST["id_carnet"]);
$folio=isset($_POST["folio"]);
$id_paciente=isset($_POST["id_paciente"]);
$id_medico=isset($_POST["id_medico"]);
$id_patologia=isset($_POST["id_patologia"]);
$id_hospital=isset($_POST["id_hospital"]);
$id_coordinacion=isset($_POST["id_coordinacion"]);
$fi=isset($_POST["fecha_inicio"]);
$ff=isset($_POST["fecha_final"]);
$hi=isset($_POST["hora_inicio"]);
$hf=isset($_POST["hora_final"]);

switch($_GET["op"]){

    case "guardaryeditar":
        /*si el id no existe entonces lo registra
	           importante: se debe poner el $_POST sino no funciona*/
        if(empty($_POST["id_carnet"])){
            /*verificamos si existe el medico en la base de datos, si ya existe un registro con el medico entonces no se registra*/
            //importante: se debe poner el $_POST sino no funciona
            $datos = $carnets->get_nombre_carnet($_POST["id_carnet"]);
            if(is_array($datos)==true and count($datos)==0){
                //no existe el medico por lo tanto hacemos el registros
                $carnets->registrar_carnet($folio,$id_paciente,$id_medico,$id_patologia,$id_hospital,$id_coordinacion,$fecha);
                echo $carnets ? "<script> swal({
                title: '¡Bien Hecho!',
                text: 'Carnet registrado correctamente',
                type: 'success',
                });</script>" :

                "<script> swal({
                title: '¡ERROR!',
                text: 'Ocurrio un error al registrar un Carnet',
                type: 'error',
                });</script>";
                //$messages[]="La categoría se registró correctamente";
            } //cierre de validacion de $datos 
            /*si ya existes el medico entonces aparece el mensaje*/
            else {

                echo "<script> swal({
                title: '¡ERROR!',
                text: 'El nombre de la carnet ya existe',
                type: 'error',
                });</script>";
            }
        }//cierre de empty
        else {
            /*si ya existe entonces editamos el medico*/
            $carnets->editar_carnet($id_carnet);
            echo $carnets ? "<script> swal({
                title: '¡Bien Hecho!',
                text: 'el carnet se edito correctamente',
                type: 'success',
                });</script>" :

            "<script> swal({
                title: '¡ERROR!',
                text: 'Ocurrio un error al editar el carnet',
                type: 'error',
                });</script>";
        }
        break;


    case 'mostrar':
        //el parametro id_medico se envia por AJAX cuando se edita la medico
        $datos=$carnets->get_carnet_por_id($_POST["id_carnet"]);
        foreach($datos as $row)
        {
            $output["id_carnet"] = $row["id_carnet"];
            $output["folio"] = $row["folio"];
            $output["id_paciente"] = $row["id_paciente"];
            $output["id_medico"] = $row["id_medico"];
            $output["id_patologia"] = $row["id_patologia"];
            $output["id_coordinacion"] = $row["id_coordinacion"];
            $output["id_hospital"] = $row["id_hospital"];
            $output["fecha"] = $row["fecha"];
        }//cierre el else
        echo json_encode($output);
        break;

    case "activarydesactivar":

        //los parametros id_medico y est vienen por via ajax
        $datos=$carnets->get_carnet_por_id($_POST["id_carnet"]);

        // si existe el id de la medico entonces recorre el array
        if(is_array($datos)==true and count($datos)>0){

            //edita el estado de la medico
            $carnets->editar_estado($_POST["folio"],$_POST["est"]);
            echo $patologias ? "<script> swal({
                title: '¡Bien Hecho!',
                text: 'La patologia cambio de estado correctamente',
                type: 'success',
                });</script>" :

            "<script> swal({
                title: '¡ERROR!',
                text: 'Ocurrio un error al cambiar de estado',
                type: 'error',
                });</script>";
            //edita el estado del producto
            //$productos->editar_estado_producto_por_categoria($_POST["id_pacinete"],$_POST["est"]);
        } 
        break;


    case "listar":

        $datos=$carnets->get_carnet();

        //Vamos a declarar un array
        $data= Array();

        foreach($datos as $row)
        {
            $sub_array = array();

            //ESTADO
            $est = '';

            $atrib = "btn btn-primary btn-md estado";
            $icono="glyphicon glyphicon-ok";
            /*if($row["estado"] == 0){
                $est = 'INACTIVO';
                $atrib = "btn btn-success btn-md estado";
                $icono = "glyphicon glyphicon-remove";
            }
            else{
                if($row["estado"] == 1){
                    $est = 'ACTIVO';
                } 
            }*/
            $sub_array[] = $row["folio"];
            $sub_array[] = $row["paciente"];
            $sub_array[] = $row["medico"];
            $sub_array[] = $row["patologia"];
            $sub_array[] = $row["hospital"];
            $sub_array[] = $row["coordinacion"];
            $sub_array[] = $row["fecha"];
            //$sub_array[] = '<button type="button" onClick="cambiarEstado('.$row["id_carnet"].','.$row["estado"].');" name="estado"                id="'.$row["id_patologia"].'" class="'.$atrib.'"><i class="'.$icono.'"></i></button>'.

            $sub_array[] =  '<button type="button" onClick="mostrar('.$row["id_carnet"].');"  id="'.$row["id_carnet"].'" class="btn btn-warning btn-md update"><i class="glyphicon glyphicon-edit"></i> </button>'.

                '<button type="button" onClick="eliminar('.$row["id_carnet"].');"  id="'.$row["id_carnet"].'" class="btn btn-danger btn-md"><i class="glyphicon glyphicon-trash"></i> </button>';

            $data[] = $sub_array;
        }
        $results = array(
            "sEcho"=>1, //Información para el datatables
            "iTotalRecords"=>count($data), //enviamos el total registros al datatable
            "iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
            "aaData"=>$data);
        echo json_encode($results);
        break;

    case "eliminar_carnet":
        //verificamos si la medico existe en la base de datos en la tabla categoria, si existe entonces lo elimina
        $datos= $carnets->get_carnet_por_id($_POST["id_carnet"]);
        if(is_array($datos)==true and count($datos)>0){
            $carnets->eliminar_carnet($_POST["id_carnet"]);
            echo $carnets ? "<script> swal({
                title: '¡Bien Hecho!',
                text: 'Patologia se elimino correctamente',
                type: 'success',
                });</script>" :

            "<script> swal({
                title: '¡ERROR!',
                text: 'Ocurrio un error al eliminar la patologia',
                type: 'error',
                });</script>";
        }
        break;
}
