<?php
//llamo a la conexion de la base de datos 
require_once("../config/conexion.php");
//llamo al modelo hospital
require_once("../modelos/Cita.php");
$citas = new Cita();
//declaramos las variables de los valores que se envian por el formulario y que recibimos por ajax y decimos que si existe el parametro que estamos recibiendo
//los valores vienen del atributo name de los campos del formulario
/*el valor id_usuario y id_medico se carga en el campo hidden cuando se edita un registro*/
//se copian los campos de la tabla medico
$title = isset($_POST["title"]) ? $_POST['title'] : "";
$start_event = isset($_POST["start"]) ? $_POST['start'] : "";
$end_event = isset($_POST["end"]) ? $_POST['end'] : "";
$id = isset($_POST["id"]) ? $_POST['id'] : "";

/*$id_paciente=isset($_POST["id_paciente"]) ? $_POST['id_paciente'] : "";
$id_medico=isset($_POST["id_medico"]) ? $_POST['id_medico'] : "";
$id_patologia=isset($_POST["id_patologia"]) ? $_POST['id_patologia'] : "";
$id_hospital=isset($_POST["id_hospital"]) ? $_POST['id_hospital'] : "";
$id_coordinacion=isset($_POST["id_coordinacion"]) ? $_POST['id_coordinacion'] : "";
$fi=isset($_POST["fecha_inicio"]) ? $_POST['fecha_inicio'] : "";
$ff=isset($_POST["fecha_final"]) ? $_POST['fecha_final'] : "";
$hi=isset($_POST["hora_inicio"]) ? $_POST['hora_inicio'] : "";
$hf=isset($_POST["hora_final"]) ? $_POST['hora_final'] : "";
$id = isset($_POST["id"]) ? $_POST['id'] : "";*/
switch ($_GET["op"]) {

    case "guardar":
        $citas->registrar_evento($_POST["title"], $_POST["start"], $_POST["end"]);
        break;

    case "load":
        $datos = $citas->load_evento();
        //Vamos a declarar un array
        $data = array();
        foreach ($datos as $row) {
            $data[] = array(
                'id'   => $row["id"],
                'title'   => $row["title"],
                'start'   => $row["start_event"],
                'end'   => $row["end_event"]
            );
        } //cierre el else
        echo json_encode($data);
        break;

    case "actualizar":
        $citas->editar_evento($_POST["title"], $_POST["start"], $_POST["end"], $_POST["id"]);
        break;

    case "eliminar":
        $citas->eliminar_evento($_POST["id"]);
        echo $citas ? "1":"0";
        break;
   /* case "guardar":
        $citas->registrar_evento( $_POST["id_paciente"], $_POST["id_medico"],
        $_POST["id_patologia"], $_POST["id_hospital"], $_POST["id_coordinacion"],
        $_POST["fecha_inicio"], $_POST["fecha_final"], $_POST["hora_inicio"], $_POST["hora_final"]);
        break;

    case "load":
        $datos = $citas->load_evento();
        //Vamos a declarar un array
        $data = array();
        foreach ($datos as $row) {
            $data[] = array(
                'id'   => $row["id"],
                'id_paciente'   => $row["id_paciente"],
                'id_medico'   => $row["id_medico"],
                'id_patologia'   => $row["id_patologia"],
                'id_hospital'   => $row["id_hospital"],
                'id_coordinacion'   => $row["id_coordinacion"],
                'fecha_inicio'   => $row["fecha_inicio"],
                'hora_inicio'   => $row["hora_inicio"],
                'fecha_final'   => $row["fecha_final"],
                'hora_final'   => $row["hora_final"]
            );
        } //cierre el else
        //
        echo json_encode($data);
        break;

    case "actualizar":
        $citas->editar_evento($_POST["title"], $_POST["start"], $_POST["end"], $_POST["id"]);
        break;

    case "eliminar":
        $citas->eliminar_evento($_POST["id"]);
        echo $citas ? "1":"0";
        break;
*/
}
