<?php
//llamo a la conexion de la base de datos 
require_once("../config/conexion.php");
//llamo al modelo hospital
require_once("../modelos/Cita.php");
$citas = new Cita();
//declaramos las variables de los valores que se envian por el formulario y que recibimos por ajax y decimos que si existe el parametro que estamos recibiendo
//los valores vienen del atributo name de los campos del formulario
/*el valor id_usuario y id_medico se carga en el campo hidden cuando se edita un registro*/
//se copian los campos de la tabla medico
$id_carnet=isset($_POST["id_carnet"]);
$fecha=isset($_POST["fecha"]);
$oct_od=isset($_POST["oct_od"]);
$oct_oi=isset($_POST["oct_oi"]);
$av_od=isset($_POST["av_od"]);
$av_oi=isset($_POST["av_oi"]);
$ap_od=isset($_POST["ap_od"]);
$ap_oi=isset($_POST["ap_oi"]);
$laser_od=isset($_POST["laser_od"]);
$laser_oi=isset($_POST["laser_oi"]);
$comentarios=isset($_POST["comentarios"]);
$folio_citas=isset($_POST["folio_citas"]);

switch($_GET["op"]){
    case "guardaryeditar":
        /*si el id no existe entonces lo registra
	           importante: se debe poner el $_POST sino no funciona*/
        if(empty($_POST["folio_citas"])){
            /*verificamos si existe el medico en la base de datos, si ya existe un registro con el medico entonces no se registra*/
            //importante: se debe poner el $_POST sino no funciona
            $datos = $citas->get_nombre_cita($_POST["id_carnet"],$_POST["fecha"]);
            if(is_array($datos)==true and count($datos)==0){
                //no existe el medico por lo tanto hacemos el registros
                $citas->registrar_cita($id_carnet,$fecha,$oct_od,$oct_oi,$av_od,$av_oi,$ap_od,$ap_oi,$laser_od,$laser_oi,$comentarios);
                echo $citas ? "<script> swal({
                title: '¡Bien Hecho!',
                text: 'Cita registrado correctamente',
                type: 'success',
                });</script>" :

                "<script> swal({
                title: '¡ERROR!',
                text: 'Ocurrio un error al registrar la cita',
                type: 'error',
                });</script>";
                //$messages[]="La categoría se registró correctamente";
            } //cierre de validacion de $datos 
            /*si ya existes el medico entonces aparece el mensaje*/
            else {
                echo "<script> swal({
                title: '¡ERROR!',
                text: 'la fecha de la cita ya existe para el paciente',
                type: 'error',
                });</script>";
            }
        }//cierre de empty
        else {
            /*si ya existe entonces editamos el medico*/
            $citas->editar_citas($folio_citas);
            echo $citas ? "<script> swal({
                title: '¡Bien Hecho!',
                text: 'La cita se edito correctamente',
                type: 'success',
                });</script>" :

            "<script> swal({
                title: '¡ERROR!',
                text: 'Ocurrio un error al editar la cita',
                type: 'error',
                });</script>";
        }
        break;


    case 'mostrar':
        //el parametro id_medico se envia por AJAX cuando se edita la medico
        $datos=$citas->get_cita_por_id($_POST["folio_citas"]);
        foreach($datos as $row)
        {
            // $output["id_paciente"] = $row["id_paciente"];
            $output["folio_citas"] = $row["folio_citas"];
            $output["fecha_cita"] = $row["fecha_cita"];
            $output["carnet"] = $row["id_carnet"];
            $output["oct_od"] = $row["oct_od"];
            $output["oct_oi"] = $row["oct_oi"];
            $output["av_od"] = $row["av_od"];
            $output["av_oi"] = $row["av_oi"];
            $output["aplicacion_od"] = $row["aplicacion_od"];
            $output["aplicacion_oi"] = $row["aplicacion_oi"];
            $output["laser_od"] = $row["laser_od"];
            $output["laser_oi"] = $row["laser_oi"];
            $output["comentario"] = $row["comentario"];

        }//cierre el else
        echo json_encode($output);
        break;

    case "activarydesactivar":

        //los parametros id_medico y est vienen por via ajax
        $datos=$hospitales->get_hospital_por_id($_POST["id_hospital"]);

        // si existe el id de la medico entonces recorre el array
        if(is_array($datos)==true and count($datos)>0){

            //edita el estado de la medico
            $hospitales->editar_estado($_POST["id_hospital"],$_POST["est"]);
            echo $hospitales ? "<script> swal({
                title:'¡Bien Hecho!',
                text:'El Hospital cambio de estado correctamente',
                type:'success',
                });</script>" :

            "<script> swal({
                title: '¡ERROR!',
                text: 'Ocurrio un error al cambiar de estado',
                type: 'error',
                });</script>";
            //edita el estado del producto
            //$productos->editar_estado_producto_por_categoria($_POST["id_pacinete"],$_POST["est"]);
        } 
        break;


    case "listar":

        $datos=$citas->get_cita();

        //Vamos a declarar un array
        $data= Array();

        foreach($datos as $row)
        {
            $sub_array = array();

            //ESTADO
            $est = '';

            $atrib = "btn btn-primary btn-md estado";
            $icono="glyphicon glyphicon-ok";
            /*if($row["estado"] == 0){
                $est = 'INACTIVO';
                $atrib = "btn btn-success btn-md estado";
                $icono = "glyphicon glyphicon-remove";
            }
            else{
                if($row["estado"] == 1){
                    $est = 'ACTIVO';
                } 
            }*/
            $sub_array[] = $row["folio_citas"];
            $sub_array[] = $row["carnet"];
            $sub_array[] = $row["fecha_cita"];
            $sub_array[] = $row["oct_od"];
            $sub_array[] = $row["oct_oi"];
            $sub_array[] = $row["av_od"];
            $sub_array[] = $row["av_oi"];
            $sub_array[] = $row["aplicacion_od"];
            $sub_array[] = $row["aplicacion_oi"];
            $sub_array[] = $row["laser_od"];
            $sub_array[] = $row["laser_oi"];
            $sub_array[] = $row["comentario"];

           // $sub_array[] = '<button type="button" onClick="cambiarEstado('.$row["id_hospital"].','.$row["estado"].');" name="estado" id="'.$row["id_hospital"].'" class="'.$atrib.'"><i class="'.$icono.'"></i></button>'.

           $sub_array[] =   '<button type="button" onClick="mostrar('.$row["folio_citas"].');"  id="'.$row["folio_citas"].'" class="btn btn-warning btn-md update"><i class="glyphicon glyphicon-edit"></i> </button>'.

                '<button type="button" onClick="eliminar('.$row["folio_citas"].');"  id="'.$row["folio_citas"].'" class="btn btn-danger btn-md"><i class="glyphicon glyphicon-trash"></i> </button>';

            $data[] = $sub_array;
        }
        $results = array(
            "sEcho"=>1, //Información para el datatables
            "iTotalRecords"=>count($data), //enviamos el total registros al datatable
            "iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
            "aaData"=>$data);
        echo json_encode($results);

        break;

    case "eliminar_cita":
        //verificamos si la medico existe en la base de datos en la tabla categoria, si existe entonces lo elimina
        $datos= $citas->get_cita_por_id($_POST["folio_citas"]);
        if(is_array($datos)==true and count($datos)>0){
            $citas->eliminar_cita($_POST["folio_citas"]);
            echo $citas ? "<script> swal({
                title: '¡Bien Hecho!',
                text: 'El hospital se elimino correctamente',
                type: 'success',
                });</script>" :

                "<script> swal({
                title: '¡ERROR!',
                text: 'Ocurrio un error al eliminar el hospital',
                type: 'error',
                });</script>";
        }
        break;
}
?>
