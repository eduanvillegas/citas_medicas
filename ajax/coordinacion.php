<?php
//llamo a la conexion de la base de datos 
require_once("../config/conexion.php");
//llamo al modelo patologia
require_once("../modelos/Coordinacion.php");

$coordinacion = new Coordinacion();
//declaramos las variables de los valores que se envian por el formulario y que recibimos por ajax y decimos que si existe el parametro que estamos recibiendo
//los valores vienen del atributo name de los campos del formulario
/*el valor id_usuario y id_medico se carga en el campo hidden cuando se edita un registro*/
//se copian los campos de la tabla medico
$id_coordinacion=isset($_POST["id_coordinacion"]);
$nombre=isset($_POST["nombre"]);
switch($_GET["op"]){
    case "guardaryeditar":
        /*si el id no existe entonces lo registra
	           importante: se debe poner el $_POST sino no funciona*/
        if(empty($_POST["id_coordinacion"])){
            /*verificamos si existe el medico en la base de datos, si ya existe un registro con el medico entonces no se registra*/
            //importante: se debe poner el $_POST sino no funciona
            $datos = $coordinacion->get_nombre_coordinacion($_POST["nombre"]);
            if(is_array($datos)==true and count($datos)==0){
                //no existe el medico por lo tanto hacemos el registros
                $coordinacion->registrar_coordinacion($nombre);
                echo $coordinacion ? "<script> swal({
                title: '¡Bien Hecho!',
                text: 'coordinacion registrado correctamente',
                type: 'success',
                });</script>" :

                "<script> swal({
                title: '¡ERROR!',
                text: 'Ocurrio un error al registrar la coordinacion',
                type: 'error',
                });</script>";
                //$messages[]="La categoría se registró correctamente";
            } //cierre de validacion de $datos 
            /*si ya existes el medico entonces aparece el mensaje*/
            else {
                echo "<script> swal({
                title: '¡ERROR!',
                text: 'El nombre de la coordinacion ya existe',
                type: 'error',
                });</script>";
            }
        }//cierre de empty
        else {
            /*si ya existe entonces editamos el medico*/
            $coordinacion->editar_coordinacion($nombre);
            echo $coordinacion ? "<script> swal({
                title: '¡Bien Hecho!',
                text: 'La coordinacion se edito correctamente',
                type: 'success',
                });</script>" :

            "<script> swal({
                title: '¡ERROR!',
                text: 'Ocurrio un error al editar la coordinacion',
                type: 'error',
                });</script>";
        }
        break;


    case 'mostrar':

        //el parametro id_medico se envia por AJAX cuando se edita la medico
        $datos=$coordinacion->get_coordinacion_por_id($_POST["id_coordinacion"]);
        foreach($datos as $row)
        {
            // $output["id_paciente"] = $row["id_paciente"];
            $output["id_coordinacion"] = $row["id_coordinacion"];
            $output["nombre"] = $row["nombre"];
            $output["estado"] = $row["estado"];
        }//cierre el else
        echo json_encode($output);
        break;

    case "activarydesactivar":

        //los parametros id_medico y est vienen por via ajax
        $datos=$coordinacion->get_coordinacion_por_id($_POST["id_coordinacion"]);

        // si existe el id de la medico entonces recorre el array
        if(is_array($datos)==true and count($datos)>0){
            //edita el estado de la medico
            $coordinacion->editar_estado($_POST["id_coordinacion"],$_POST["est"]);
            echo $coordinacion ? "<script> swal({
                title: '¡Bien Hecho!',
                text: 'La coordiancion cambio de estado correctamente',
                type: 'success',
                });</script>" :

            "<script> swal({
                title: '¡ERROR!',
                text: 'Ocurrio un error al cambiar de estado',
                type: 'error',
                });</script>";
            //edita el estado del producto
            //$productos->editar_estado_producto_por_categoria($_POST["id_pacinete"],$_POST["est"]);
        } 
        break;


    case "listar":

        $datos=$coordinacion->get_coordinacion();

        //Vamos a declarar un array
        $data= Array();

        foreach($datos as $row)
        {
            $sub_array = array();

            //ESTADO
            $est = '';

            $atrib = "btn btn-primary btn-md estado";
            $icono="glyphicon glyphicon-ok";
            if($row["estado"] == 0){
                $est = 'INACTIVO';
                $atrib = "btn btn-success btn-md estado";
                $icono = "glyphicon glyphicon-remove";
            }
            else{
                if($row["estado"] == 1){
                    $est = 'ACTIVO';
                } 
            }

            $sub_array[] = $row["nombre"];
            $sub_array[] = '<button type="button" onClick="cambiarEstado('.$row["id_coordinacion"].','.$row["estado"].');" name="estado" id="'.$row["id_coordinacion"].'" class="'.$atrib.'"><i class="'.$icono.'"></i></button>'.
                '<button type="button" onClick="mostrar('.$row["id_coordinacion"].');"  id="'.$row["id_coordinacion"].'" class="btn btn-warning btn-md update"><i class="glyphicon glyphicon-edit"></i> </button>'.
                '<button type="button" onClick="eliminar('.$row["id_coordinacion"].');"  id="'.$row["id_coordinacion"].'" class="btn btn-danger btn-md"><i class="glyphicon glyphicon-trash"></i> </button>';
            $data[] = $sub_array;
        }
        $results = array(
            "sEcho"=>1, //Información para el datatables
            "iTotalRecords"=>count($data), //enviamos el total registros al datatable
            "iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
            "aaData"=>$data);
        echo json_encode($results);

        break;

    case "eliminar_coordinacion":

        //verificamos si la medico existe en la base de datos en la tabla categoria, si existe entonces lo elimina

        $datos= $coordinacion->get_coordinacion_por_id($_POST["id_coordinacion"]);

        if(is_array($datos)==true and count($datos)>0){
            $coordinacion->eliminar_coordinacion($_POST["id_coordinacion"]);
            echo $coordinacion ? "<script> swal({
                title: '¡Bien Hecho!',
                text: 'coordiancion se elimino correctamente',
                type: 'success',
                });</script>" :

            "<script> swal({
                title: '¡ERROR!',
                text: 'Ocurrio un error al eliminar la coordinacion',
                type: 'error',
                });</script>";
        }
        break;
}
?>
