<?php
//llamo a la conexion de la base de datos 
require_once("../config/conexion.php");
//llamo al modelo Reporte
require_once("../modelos/Consulta.php");
$reporte = new Consulta();
//$nombre=isset($_POST["nombre"]);
switch ($_GET["op"]) {

    case "consultahistorial":
        $id_paciente = $_REQUEST["id_paciente"];

        $datos=$reporte->get_reporte_por_idpaciente($id_paciente);

        //Vamos a declarar un array
        $data= Array();

        foreach($datos as $row)
        {
            $sub_array = array();
            //ESTADO
            $est = '';
            $atrib = "btn btn-primary btn-md estado";
            $icono="fa fa-eye";
            if($row["estado"] == 0){
                $est = 'INACTIVO';
                $atrib = "btn btn-success btn-md estado";
                $icono = "fa fa-eye";
            }
            else{
                if($row["estado"] == 1){
                    $est = 'ACTIVO';
                }
            }

            $sub_array[] = $row["nombre"];
            $sub_array[] = $row["edad"];
            $sub_array[] = $row["sexo"];
            $sub_array[] = $row["fecha_nacimiento"];
            $sub_array[] = $row["direccion"];
            $sub_array[] = $row["telefono"];
            $sub_array[] = $row["email"];
            $sub_array[] = $row["expediente"];
            $sub_array[] = '<button type="button" onClick="imprimir('.$row["id_expediente"].','.$row["id_persona"].','.$row["estado"].');" name="estado" id="'.$row["id_expediente"].'" class="'.$atrib.'"><i class="'.$icono.'"></i></button>';
            $data[] = $sub_array;
        }
        $results = array(
            "sEcho"=>1, //Información para el datatables
            "iTotalRecords"=>count($data), //enviamos el total registros al datatable
            "iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
            "aaData"=>$data);
        echo json_encode($results);

        break;
}
