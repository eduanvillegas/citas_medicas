<?php
//llamo a la conexion de la base de datos 
require_once("../config/conexion.php");
//llamo al modelo hospital
require_once("../modelos/Salud.php");
$salud = new Salud();
//declaramos las variables de los valores que se envian por el formulario y que recibimos por ajax y decimos que si existe el parametro que estamos recibiendo
//los valores vienen del atributo name de los campos del formulario
/*el valor id_usuario y id_medico se carga en el campo hidden cuando se edita un registro*/
//se copian los campos de la tabla medico
$id_persona = isset($_POST["id_persona"])  ? $_POST['id_persona'] : "";
$fechaExp = isset($_POST["fechaExp"])  ? $_POST['fechaExp'] : "";
$expediente = isset($_POST["expediente"])  ? $_POST['expediente'] : "";
$observacionesExp = isset($_POST["observacionesExp"])  ? $_POST['observacionesExp'] : "";

$id_persona6 = isset($_POST["id_persona6"])  ? $_POST['id_persona6'] : "";
$fechaExpediente = isset($_POST["fechaExpediente"])  ? $_POST['fechaExpediente'] : "";
$Ediarrea = isset($_POST["Ediarrea"])  ? $_POST['Ediarrea'] : "";
$Eestrenimiento = isset($_POST["Eestrenimiento"])  ? $_POST['Eestrenimiento'] : "";
$Egastritis = isset($_POST["Egastritis"])  ? $_POST['Egastritis'] : "";
$Eulcera = isset($_POST["Eulcera"])  ? $_POST['Eulcera'] : "";
$Epirosis = isset($_POST["Epirosis"])  ? $_POST['Epirosis'] : "";
$Ecolitis = isset($_POST["Ecolitis"])  ? $_POST['Ecolitis'] : "";
$Evomito = isset($_POST["Evomito"])  ? $_POST['Evomito'] : "";
$Edentadura = isset($_POST["Edentadura"])  ? $_POST['Edentadura'] : "";
$otros = isset($_POST["otros"])  ? $_POST['otros'] : "";
$observaciones = isset($_POST["observaciones"])  ? $_POST['observaciones'] : "";
$PadEnfermedad = isset($_POST["PadEnfermedad"])  ? $_POST['PadEnfermedad'] : "";
$PadEnfermedadCual = isset($_POST["PadEnfermedadCual"])  ? $_POST['PadEnfermedadCual'] : "";
$fechaDigestion = isset($_POST["fechaDigestion"])  ? $_POST['fechaDigestion'] : "";
$Tmedicamento = isset($_POST["Tmedicamento"])  ? $_POST['Tmedicamento'] : "";
$TmedicamentoCual = isset($_POST["TmedicamentoCual"])  ? $_POST['TmedicamentoCual'] : "";
$dosis = isset($_POST["dosis"])  ? $_POST['dosis'] : "";
$FechaIngestion = isset($_POST["FechaIngestion"])  ? $_POST['FechaIngestion'] : "";
$laxante = isset($_POST["laxante"])  ? $_POST['laxante'] : "";
$diuretico = isset($_POST["diuretico"])  ? $_POST['diuretico'] : "";
$antiacido = isset($_POST["antiacido"])  ? $_POST['antiacido'] : "";
$analgesico = isset($_POST["analgesico"])  ? $_POST['analgesico'] : "";
$diurcirugiaetico = isset($_POST["cirugia"])  ? $_POST['cirugia'] : "";

$id_persona1 = isset($_POST["id_persona1"])  ? $_POST['id_persona1'] : "";
$obecidad = isset($_POST["obecidad"])  ? $_POST['obecidad'] : "";
$diabetes = isset($_POST["diabetes"])  ? $_POST['diabetes'] : "";
$hta = isset($_POST["hta"])  ? $_POST['hta'] : "";
$hipertrigliceridemia = isset($_POST["hipertrigliceridemia"])  ? $_POST['hipertrigliceridemia'] : "";
$hipercolesterolemia = isset($_POST["hipercolesterolemia"])  ? $_POST['hipercolesterolemia'] : "";
$cancer = isset($_POST["cancer"])  ? $_POST['cancer'] : "";

$id_persona2 = isset($_POST["id_persona2"])  ? $_POST['id_persona2'] : "";
$embarazo = isset($_POST["embarazo"])  ? $_POST['embarazo'] : "";
$anticonceptivos = isset($_POST["anticonceptivos"])  ? $_POST['anticonceptivos'] : "";
$anticonceptivos_cual = isset($_POST["anticonceptivos_cual"])  ? $_POST['anticonceptivos_cual'] : "";
$anticonceptivos_dosis = isset($_POST["anticonceptivos_dosis"])  ? $_POST['anticonceptivos_dosis'] : "";
$climaterio = isset($_POST["climaterio"])  ? $_POST['climaterio'] : "";
$climaterio_fecha = isset($_POST["climaterio_fecha"])  ? $_POST['climaterio_fecha'] : "";
$terapia = isset($_POST["terapia"])  ? $_POST['terapia'] : "";
$terapia_cual = isset($_POST["terapia_cual"])  ? $_POST['terapia_cual'] : "";
$terapia_dosis = isset($_POST["terapia_dosis"])  ? $_POST['terapia_dosis'] : "";

$id_persona3 = isset($_POST["id_persona3"])  ? $_POST['id_persona3'] : "";
$actividad = isset($_POST["actividad"])  ? $_POST['actividad'] : "";
$ejercicio_tipo = isset($_POST["ejercicio_tipo"])  ? $_POST['ejercicio_tipo'] : "";
$ejercicio_frecuencia = isset($_POST["ejercicio_frecuencia"])  ? $_POST['ejercicio_frecuencia'] : "";
$ejercicio_duracion = isset($_POST["ejercicio_duracion"])  ? $_POST['ejercicio_duracion'] : "";
$ejercicio_cuando_inicio = isset($_POST["ejercicio_cuando_inicio"])  ? $_POST['ejercicio_cuando_inicio'] : "";

$id_persona4 = isset($_POST["id_persona4"])  ? $_POST['id_persona4'] : "";
$consumo_alcohol = isset($_POST["consumo_alcohol"])  ? $_POST['consumo_alcohol'] : "";
$consumo_alcohol_frecuencia = isset($_POST["consumo_alcohol_frecuencia"])  ? $_POST['consumo_alcohol_frecuencia'] : "";
$consumo_alcohol_cantidad = isset($_POST["consumo_alcohol_cantidad"])  ? $_POST['consumo_alcohol_cantidad'] : "";
$consumo_tabaco = isset($_POST["consumo_tabaco"])  ? $_POST['consumo_tabaco'] : "";
$consumo_tabaco_frecuencia = isset($_POST["consumo_tabaco_frecuencia"])  ? $_POST['consumo_tabaco_frecuencia'] : "";
$consumo_tabaco_cantidad = isset($_POST["consumo_tabaco_cantidad"])  ? $_POST['consumo_tabaco_cantidad'] : "";
$consumo_cafe = isset($_POST["consumo_cafe"])  ? $_POST['consumo_cafe'] : "";
$consumo_cafe_frecuencia = isset($_POST["consumo_cafe_frecuencia"])  ? $_POST['consumo_cafe_frecuencia'] : "";
$consumo_cafe_cantidad = isset($_POST["consumo_cafe_cantidad"])  ? $_POST['consumo_cafe_cantidad'] : "";

$id_persona5 = isset($_POST["id_persona5"])  ? $_POST['id_persona5'] : "";
$comidasxdia = isset($_POST["comidasxdia"])  ? $_POST['comidasxdia'] : "";
$desayuno = isset($_POST["desayuno"])  ? $_POST['desayuno'] : "";
$desayuno_colacion = isset($_POST["desayuno_colacion"])  ? $_POST['desayuno_colacion'] : "";
$comida = isset($_POST["comida"])  ? $_POST['comida'] : "";
$comida_colacion = isset($_POST["comida_colacion"])  ? $_POST['comida_colacion'] : "";
$cena = isset($_POST["cena"])  ? $_POST['cena'] : "";
$quienprepaali = isset($_POST["quienprepaali"])  ? $_POST['quienprepaali'] : "";
$comeEntreComidas = isset($_POST["comeEntreComidas"])  ? $_POST['comeEntreComidas'] : "";
$QueComeEntreComidas = isset($_POST["QueComeEntreComidas"])  ? $_POST['QueComeEntreComidas'] : "";
$modificacionAlimentos = isset($_POST["modificacionAlimentos"])  ? $_POST['modificacionAlimentos'] : "";
$modificacionAlimentosPorque = isset($_POST["modificacionAlimentosPorque"])  ? $_POST['modificacionAlimentosPorque'] : "";
$modificacionAlimentosComo = isset($_POST["modificacionAlimentosComo"])  ? $_POST['modificacionAlimentosComo'] : "";
$modificacionAlimentosApetito = isset($_POST["modificacionAlimentosApetito"])  ? $_POST['modificacionAlimentosApetito'] : "";
$horaHambre = isset($_POST["horaHambre"])  ? $_POST['horaHambre'] : "";
$alimentospreferidos = isset($_POST["alimentospreferidos"])  ? $_POST['alimentospreferidos'] : "";
$alimentosAgradan = isset($_POST["alimentosAgradan"])  ? $_POST['alimentosAgradan'] : "";
$alimentosMalestar = isset($_POST["alimentosMalestar"])  ? $_POST['alimentosMalestar'] : "";
$alergiaAlimentaria = isset($_POST["alergiaAlimentaria"])  ? $_POST['alergiaAlimentaria'] : "";
$alergiaAlimentariaCual = isset($_POST["alergiaAlimentariaCual"])  ? $_POST['alergiaAlimentariaCual'] : "";
$intoleranciaAlimentaria = isset($_POST["intoleranciaAlimentaria"])  ? $_POST['intoleranciaAlimentaria'] : "";
$intoleranciaAlimentariaCual = isset($_POST["intoleranciaAlimentariaCual"])  ? $_POST['intoleranciaAlimentariaCual'] : "";
$tomaSuplemento = isset($_POST["tomaSuplemento"])  ? $_POST['tomaSuplemento'] : "";
$tomaSuplementoCual = isset($_POST["tomaSuplementoCual"])  ? $_POST['tomaSuplementoCual'] : "";
$tomaSuplementoDosis = isset($_POST["tomaSuplementoDosis"])  ? $_POST['tomaSuplementoDosis'] : "";
$tomaSuplementoPorque = isset($_POST["tomaSuplementoPorque"])  ? $_POST['tomaSuplementoPorque'] : "";
$consumoVaria = isset($_POST["consumoVaria"])  ? $_POST['consumoVaria'] : "";
$consumoVariaPorque = isset($_POST["consumoVariaPorque"])  ? $_POST['consumoVariaPorque'] : "";
$agregaSalComida = isset($_POST["agregaSalComida"])  ? $_POST['agregaSalComida'] : "";
$utilizanGrasa = isset($_POST["utilizanGrasa"])  ? $_POST['utilizanGrasa'] : "";
$dietaEspecial = isset($_POST["dietaEspecial"])  ? $_POST['dietaEspecial'] : "";
$tipoDieta = isset($_POST["tipoDieta"])  ? $_POST['tipoDieta'] : "";
$tipoDietacuando = isset($_POST["tipoDietacuando"])  ? $_POST['tipoDietacuando'] : "";
$tipoDietaTiempo = isset($_POST["tipoDietaTiempo"])  ? $_POST['tipoDietaTiempo'] : "";
$tipoDietaRazon = isset($_POST["tipoDietaRazon"])  ? $_POST['tipoDietaRazon'] : "";
$tipoDietaApego = isset($_POST["tipoDietaApego"])  ? $_POST['tipoDietaApego'] : "";
$tipoDietaResultados = isset($_POST["tipoDietaResultados"])  ? $_POST['tipoDietaResultados'] : "";
$medicamentoBajarPeso = isset($_POST["medicamentoBajarPeso"])  ? $_POST['medicamentoBajarPeso'] : "";
$medicamentoBajarPesoCuales = isset($_POST["medicamentoBajarPesoCuales"])  ? $_POST['medicamentoBajarPesoCuales'] : "";

switch ($_GET["op"]) {
    
    case "guardarexpediente":
            $datos=$salud->registrar_expediente( $id_persona,$expediente,$fechaExp,$observacionesExp);
            foreach($datos as $row)
            {
                $output["id_expediente"] = $row["id_expediente"];
                $output["expediente"] = $row["expediente"];
            }//cierre el else

            echo json_encode($output);
        break;

    case "guardaryeditar":
            $salud->registrar_salud(
                $id_persona6,
                $Ediarrea,
                $Eestrenimiento,
                $Egastritis,
                $Eulcera,
                $Epirosis,
                $Ecolitis,
                $Evomito,
                $Edentadura,
                $otros,
                $observaciones,
                $PadEnfermedad,
                $PadEnfermedadCual,
                $fechaDigestion,
                $Tmedicamento,
                $TmedicamentoCual,
                $dosis,
                $FechaIngestion,
                $laxante,
                $diuretico,
                $antiacido,
                $analgesico,
                $diurcirugiaetico
            );
        break;

    case "guardarafamiliares":
            $salud->registrar_afamiliares( $id_persona1,$obecidad, $diabetes, $hta, $hipertrigliceridemia, $hipercolesterolemia, $cancer);
        break;

    case "guardaraginecologicos":
            $salud->registrar_aginecologicos(
                $id_persona2,
                $embarazo,
                $anticonceptivos,
                $anticonceptivos_cual,
                $anticonceptivos_dosis,
                $climaterio,
                $climaterio_fecha,
                $terapia,
                $terapia_cual,
                $terapia_dosis
            );
            echo $salud ? "<script> swal({
                        title: '¡Bien Hecho!',
                        text: 'coordinacion registrado correctamente',
                        type: 'success',
                        });</script>" : "<script> swal({
                        title: '¡ERROR!',
                        text: 'Ocurrio un error al registrar la coordinacion',
                        type: 'error',
                        });</script>";
        break;

    case "guardardactividades":
            $salud->registrar_dactividades($id_persona3, $actividad, $ejercicio_tipo, $ejercicio_frecuencia, $ejercicio_duracion, $ejercicio_cuando_inicio);
        
        break;

    case "guardarconsumo":
            $salud->registrar_consumo(
                $id_persona4,
                $consumo_alcohol,
                $consumo_alcohol_frecuencia,
                $consumo_alcohol_cantidad,
                $consumo_tabaco,
                $consumo_tabaco_frecuencia,
                $consumo_tabaco_cantidad,
                $consumo_cafe,
                $consumo_cafe_frecuencia,
                $consumo_cafe_cantidad
            );
        break;

    case "guardaridieteticos":
            $salud->registrar_dieteticos(
                $id_persona5,
                $comidasxdia,
                $desayuno,
                $desayuno_colacion,
                $comida,
                $comida_colacion,
                $cena,
                $quienprepaali,
                $comeEntreComidas,
                $QueComeEntreComidas,
                $modificacionAlimentos,
                $modificacionAlimentosPorque,
                $modificacionAlimentosComo,
                $modificacionAlimentosApetito,
                $horaHambre,
                $alimentospreferidos,
                $alimentosAgradan,
                $alimentosMalestar,
                $alergiaAlimentaria,
                $alergiaAlimentariaCual,
                $intoleranciaAlimentaria,
                $intoleranciaAlimentariaCual,
                $tomaSuplemento,
                $tomaSuplementoCual,
                $tomaSuplementoDosis,
                $tomaSuplementoPorque,
                $consumoVaria,
                $consumoVariaPorque,
                $agregaSalComida,
                $utilizanGrasa,
                $dietaEspecial,
                $tipoDieta,
                $tipoDietacuando,
                $tipoDietaTiempo,
                $tipoDietaRazon,
                $tipoDietaApego,
                $tipoDietaResultados,
                $medicamentoBajarPeso,
                $medicamentoBajarPesoCuales
            );
        break;
}
