<?php
//llamo a la conexion de la base de datos 
require_once("../config/conexion.php");
//llamo al modelo Categorías
require_once("../modelos/Pacientes.php");

$pacientes = new Pacientes();


//declaramos las variables de los valores que se envian por el formulario y que recibimos por ajax y decimos que si existe el parametro que estamos recibiendo
//los valores vienen del atributo name de los campos del formulario
/*el valor id_usuario y id_categoria se carga en el campo hidden cuando se edita un registro*/
//se copian los campos de la tabla categoria

$id_paciente=isset($_POST["id_paciente"])  ? $_POST['id_paciente'] : "";
$paciente=isset($_POST["paciente"])  ? $_POST['paciente'] : "";
$edad=isset($_POST["edad"])  ? $_POST['edad'] : "";
$sexo=isset($_POST["sexo"])  ? $_POST['sexo'] : "";
$fecha= isset($_POST["fecha"]) ? $_POST['fecha'] : "";
$est_civ= isset($_POST["est_civ"]) ? $_POST['est_civ'] : "";
$escolaridad=isset($_POST["escolaridad"])  ? $_POST['escolaridad'] : "";
$ocupacion=isset($_POST["ocupacion"])  ? $_POST['ocupacion'] : "";
$direccion= isset($_POST["direccion"]) ? $_POST['direccion'] : "";
$telefono= isset($_POST["telefono"]) ? $_POST['telefono'] : "";
$correo= isset($_POST["correo"]) ? $_POST['correo'] : "";

switch($_GET["op"]){


    case "guardaryeditar":

        /*si el id no existe entonces lo registra
	           importante: se debe poner el $_POST sino no funciona*/
        if(empty($_POST["id_paciente"])){
            /*verificamos si existe la categoria en la base de datos, si ya existe un registro con la categoria entonces no se registra*/
            //importante: se debe poner el $_POST sino no funciona
            $datos = $pacientes->get_nombre_paciente($_POST["paciente"]);
            if(is_array($datos)==true and count($datos)==0){
                //no existe la categoria por lo tanto hacemos el registros
                $pacientes->registrar_paciente($paciente,$edad,$sexo,$fecha,$est_civ,$escolaridad,$ocupacion,$direccion,$telefono,$correo);
                echo $pacientes ? "<script> swal({
                title: '¡Bien!',
                text: 'Paciente registrado exitosamente',
                type: 'success',
                });</script>" :

                "<script> swal({
                title: '¡ERROR!',
                text: 'Ocurrio un error al registrar el Paciente',
                type: 'error',
                });</script>";
                //$messages[]="La categoría se registró correctamente";
            } //cierre de validacion de $datos 
            /*si ya existes la categoria entonces aparece el mensaje*/
            else {

                echo "<script> swal({
                title: '¡ERROR!',
                text: 'El nombre del paciente ya existe',
                type: 'error',
                });</script>";
            }
        }//cierre de empty
        else {
            /*si ya existe entonces editamos la categoria*/
            $pacientes->editar_paciente($id_paciente,$paciente,$edad,$sexo,$fecha,$est_civ,$escolaridad,$ocupacion,$direccion,$telefono,$correo);
            //$pacientes->editar_paciente($paciente,$edad,$sexo,$diabetes,$no_diabetes);
            echo $pacientes ? "<script> swal({
                title: '¡Bien!',
                text: 'el paciente se edito correctamente',
                type: 'success',
                });</script>" :

            "<script> swal({
                title: '¡ERROR!',
                text: 'Ocurrio un error al editar el paciente',
                type: 'error',
                });</script>";
        }
        break;


    case 'mostrar':

        //selecciona el id de la categoria

        //el parametro id_categoria se envia por AJAX cuando se edita la categoria
        $datos=$pacientes->get_paciente_por_id($_POST["id_paciente"]);
        foreach($datos as $row)
        {
            $output["id_paciente"] = $row["id_persona"];
            $output["paciente"] = $row["nombre"];
            $output["edad"] = $row["edad"];
            $output["sexo"] = $row["sexo"];
            $output["fecha"] = $row["fecha_nacimiento"];
            $output["est_civ"] = $row["estado_civil"];
            $output["escolaridad"] = $row["escolaridad"];
            $output["ocupacion"] = $row["ocupacion"];
            $output["direccion"] = $row["direccion"];
            $output["telefono"] = $row["telefono"];
            $output["email"] = $row["email"];
            $output["estado"] = $row["estado"];
        }

        /*
        //verifica si el id_categoria tiene registro asociado a compras
        $categoria_compras=$categorias->get_categoria_por_id_compras($_POST["id_paciente"]);


        //verifica si el id_categoria tiene registro asociado a detalle_compras
        $categoria_detalle_compras=$categorias->get_categoria_por_id_detalle_compras($_POST["id_paciente"]);


        //valida si el id_categoria  tiene registros asociados en la tabla compras y detalle_compras
        if(is_array($categoria_compras)==true and count($categoria_compras)==0 and is_array($categoria_detalle_compras)==true and count($categoria_detalle_compras)==0){

            foreach($datos as $row)
            {
               /* $output["categoria"] = $row["categoria"];
                $output["estado"] = $row["estado"];
                $output["id_usuario"] = $row["id_usuario"];*/

        /*   } 
        } else  {
            //si el id_categoria tiene relacion con la tabla compras y detalle_compras entonces se deshabilita la categoria
            foreach($datos as $row)
            {
                $output["categoria_id"] = $row["id_categoria"];
                $output["categoria"] = $row["categoria"];
                $output["estado"] = $row["estado"];
                $output["id_usuario"] = $row["id_usuario"];
            }
        }*/
        //cierre el else
        echo json_encode($output);
        break;

    case "activarydesactivar":

        //los parametros id_categoria y est vienen por via ajax
        $datos=$pacientes->get_paciente_por_id($_POST["id_paciente"]);

        // si existe el id de la categoria entonces recorre el array
        if(is_array($datos)==true and count($datos)>0){

            //edita el estado de la categoria
            $pacientes->editar_estado($_POST["id_paciente"],$_POST["est"]);
            echo $pacientes ? "<script> swal({
                title: '¡Bien Hecho!',
                text: 'El paciente cambio de estado correctamente',
                type: 'success',
                });</script>" :

            "<script> swal({
                title: '¡ERROR!',
                text: 'Ocurrio un error al cambiar de estado',
                type: 'error',
                });</script>";
            //edita el estado del producto
            //$productos->editar_estado_producto_por_categoria($_POST["id_pacinete"],$_POST["est"]);
        } 
        break;


    case "listar":

        $datos=$pacientes->get_paciente();

        //Vamos a declarar un array
        $data= Array();

        foreach($datos as $row)
        {
            $sub_array = array();
            //ESTADO
            $est = '';
            $atrib = "btn btn-primary btn-md estado";
            $icono="fa fa-check";
            if($row["estado"] == 0){
                $est = 'INACTIVO';
                $atrib = "btn btn-success btn-md estado";
                $icono = "fa fa-close";
            }
            else{
                if($row["estado"] == 1){
                    $est = 'ACTIVO';
                }
            }

            $sub_array[] = $row["nombre"];
            $sub_array[] = $row["edad"];
            $sub_array[] = $row["sexo"];
            $sub_array[] = $row["fecha_nacimiento"];
            $sub_array[] = $row["direccion"];
            $sub_array[] = $row["telefono"];
            $sub_array[] = $row["email"];
            $sub_array[] = '<button type="button" onClick="cambiarEstado('.$row["id_persona"].','.$row["estado"].');" name="estado" id="'.$row["id_persona"].'" class="'.$atrib.'"><i class="'.$icono.'"></i></button>'.

                '<button type="button" onClick="mostrar('.$row["id_persona"].');"  id="'.$row["id_persona"].'" class="btn btn-warning btn-md update"><i class="fa fa-pencil"></i> </button>'.

                '<button type="button" onClick="eliminar('.$row["id_persona"].');"  id="'.$row["id_persona"].'" class="btn btn-danger btn-md"><i class="fa fa-trash"></i> </button>';

            /*   $sub_array[] = '<button type="button" onClick="mostrar('.$row["id_paciente"].');"  id="'.$row["id_paciente"].'" class="btn btn-warning btn-md update"><i class="glyphicon glyphicon-edit"></i> Editar</button>';

            $sub_array[] = '<button type="button" onClick="eliminar('.$row["id_paciente"].');"  id="'.$row["id_paciente"].'" class="btn btn-danger btn-md"><i class="glyphicon glyphicon-edit"></i> Eliminar</button>';
*/
            $data[] = $sub_array;
        }
        $results = array(
            "sEcho"=>1, //Información para el datatables
            "iTotalRecords"=>count($data), //enviamos el total registros al datatable
            "iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
            "aaData"=>$data);
        echo json_encode($results);

        break;

    case "eliminar_categoria":



        //verificamos si la categoria existe en la base de datos en la tabla categoria, si existe entonces lo elimina

        $datos= $pacientes->get_paciente_por_id($_POST["id_paciente"]);


        if(is_array($datos)==true and count($datos)>0){

            $pacientes->eliminar_paciente($_POST["id_paciente"]);

            $messages[]="La categoría se eliminó exitosamente";

        }
        //prueba mensaje de success
        if(isset($messages)){

            echo "<script> swal({
           title: '¡Bien!',
           text: 'Paciente eliminado exitosamente',
           type: 'success',
         });</script>";
        }


        //fin mensaje success


        //inicio de mensaje de error

        if(isset($errors)){

            echo "<script> swal({
           title: '¡ERROR!',
           text: 'Ocurrio un error al eliminar el Paciente',
           type: 'error',
         });</script>";
        }
        //fin de mensaje de error
        break;
}
?>
