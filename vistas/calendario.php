<?php

require_once("../config/conexion.php");

if (isset($_SESSION["correo"])) {

    /*Se llaman los modelos y se crean los objetos para llamar el numero de registros en el menu lateral izquierdo y en el home*/
    require_once("../modelos/Proveedores.php");
    require_once("../modelos/Clientes.php");
    require_once("../modelos/Medico.php");
    require_once("../modelos/Pacientes.php");
    require_once("../modelos/Patologia.php");
    require_once("../modelos/Hospital.php");
    require_once("../modelos/Coordinacion.php");
    require_once("../modelos/Cita.php");

    $proveedor = new Proveedor();
    $cliente = new Cliente();
    $medico = new Medico();
    $paciente = new Pacientes();
    $patologia = new Patologia();
    $hospital = new Hospital();
    $coordinacion = new Coordinacion();
    $citas = new Cita();

    $medicos = $medico->get_medico();
    $pacientes = $paciente->get_paciente();
    $patologias = $patologia->get_patologia();
    $coordinaciones = $coordinacion->get_coordinacion();
    $hospitale = $hospital->get_hospital();
    ?>

    <?php require_once("header.php"); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Inicio
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">

            <!--INICIO CONTENIDO-->

            <!--COMPRAS ACTUAL-->
            <div class="row">

                <div class="">

                    <div class="box">

                        <div class="box-body">
                            <div id='calendar'></div>

                        </div>
                        <!--fin box-body-->
                    </div>
                    <!--fin box-->

                </div>
                <!--fin col-sm-6-->
            </div>

            <!--row-->
            <!--FIN CONTENIDO-->
            <!-- Modal  to Add Event -->
            <div id="createEventModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Agregar Cita</h4>
                        </div>
                        <div class="modal-body">
                            <div class="control-group">
                                <label class="control-label" for="inputPatient">Cita:</label>
                                <div class="field desc">
                                    <input class="form-control" id="title" name="title" placeholder="Citas" type="text" value="">
                                </div>
                            </div>

                            <input type="hidden" id="startTime" />
                            <input type="hidden" id="endTime" />



                            <div class="control-group">
                                <label class="control-label" for="when">Informacion:</label>
                                <div class="controls controls-row" id="when" style="margin-top:5px;">
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                            <button type="submit" class="btn btn-primary" id="submitButton">Guardar</button>
                        </div>
                    </div>

                </div>
            </div>

            <!-- Modal to Event Details -->
            <div id="calendarModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Detalle del evento</h4>
                        </div>
                        <div id="modalBody" class="modal-body">
                            <h4 id="modalTitle" class="modal-title"></h4>
                            <div id="modalWhen" style="margin-top:5px;"></div>
                        </div>
                        <input type="hidden" id="eventID" />
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                            <button type="submit" class="btn btn-danger" id="deleteButton">Borrar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--Modal-->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


    <?php require_once("footer.php"); ?>
    <script type="text/javascript" src="js/calendario.js"></script>
<?php

} else {

    header("Location:" . Conectar::ruta() . "index.php");
    exit();
}
?>