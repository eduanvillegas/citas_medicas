<?php
//Activacion de almacenamiento en buffer
ob_start();

//Incluimos archivo Factura.php
require 'Factura.php';
$pdf = new PDF_Invoice('P', 'mm', 'A4');
//Obtenemos los datos de la cabecera de la venta actual
require_once "../../modelos/Reportes.php";
$reportes = new Reportes();
$rsptap = $reportes->get_reporte_por_idpaciente($_GET["idper"]);
//Recorremos todos los valores obtenidos
//$regp = $rsptap->fetch_object();
//echo $rsptap;
//Vamos a declarar un array
$data = array();

foreach ($rsptap as $row) {
    //$sub_array = array();
    //$sub_array[] = $row["nombre"];
    //$data[] = $sub_array;
    $data[0] = $row["nombre"];
    $data[1] = $row["edad"];
    $data[2] = $row["sexo"];
    $data[3] = $row["fecha_nacimiento"];
    $data[4] = $row["estado_civil"];
    $data[5] = $row["escolaridad"];
    $data[6] = $row["ocupacion"];
    $data[7] = $row["direccion"];
    $data[8] = $row["telefono"];
    $data[9] = $row["email"];
}
$pdf->AddPage();
$pdf->datos_personal($data[0], $data[1], $data[2], $data[3], $data[4], $data[5], $data[6], $data[7], $data[8], $data[9]);
$rsptap = $reportes->datos_personal_expediente($_GET["id"]);
//Recorremos todos los valores obtenidos
$dataExpediente = array();
foreach ($rsptap as $row) {
    $dataExpediente[0] = $row["expediente"];
    $dataExpediente[1] = $row["fecha_expediente"];
    $dataExpediente[2] = $row["motivo_consulta"];
}
$pdf->datos_personal_expediente($dataExpediente[0],$dataExpediente[1],$dataExpediente[2]);

$rsptap = $reportes->get_reporte_salud($_GET["id"]);
$dataSalud = array();
foreach ($rsptap as $row) {
    $dataSalud[0] = $row["diarrea"];
    $dataSalud[1] = $row["estrenimiento"];
    $dataSalud[2] = $row["gastritis"];
    $dataSalud[3] = $row["ulcera"];
    $dataSalud[4] = $row["pirosis"];
    $dataSalud[5] = $row["colitis"];
    $dataSalud[6] = $row["vomito"];
    $dataSalud[7] = $row["dentadura"];
    $dataSalud[8] = $row["otros"];
    $dataSalud[9] = $row["observaciones"];
    $dataSalud[10] = $row["enfermedad_diagnosticada"];
    $dataSalud[11] = $row["ed_cuales"];
    $dataSalud[12] = $row["fecha_diagnostico"];
    $dataSalud[13] = $row["t_medicamento"];
    $dataSalud[14] = $row["med_cual"];
    $dataSalud[15] = $row["dosis"];
    $dataSalud[16] = $row["laxante"];
    $dataSalud[17] = $row["diuretico"];
    $dataSalud[18] = $row["antiacido"];
    $dataSalud[19] = $row["analgesico"];
    $dataSalud[20] = $row["cirugia"];
    $dataSalud[21] = $row["fecha_indigestion"];
}
$pdf->AddPage();
$pdf->antecedentes_salud(
    $dataSalud[0],
    $dataSalud[1],
    $dataSalud[2],
    $dataSalud[3],
    $dataSalud[4],
    $dataSalud[5],
    $dataSalud[6],
    $dataSalud[7],
    $dataSalud[8],
    $dataSalud[9],
    $dataSalud[10],
    $dataSalud[11],
    $dataSalud[12],
    $dataSalud[13],
    $dataSalud[14],
    $dataSalud[15],
    $dataSalud[16],
    $dataSalud[17],
    $dataSalud[18],
    $dataSalud[19],
    $dataSalud[20],
    $dataSalud[21]
);

$rsptap = $reportes->get_reporte_familiar($_GET["id"]);
$dataFamiliar = array();
foreach ($rsptap as $row) {
    $dataFamiliar[0] = $row["obesidad"];
    $dataFamiliar[1] = $row["diabetes"];
    $dataFamiliar[2] = $row["hta"];
    $dataFamiliar[3] = $row["hipertrigliceridemia"];
    $dataFamiliar[4] = $row["hipercolesterolemia"];
    $dataFamiliar[5] = $row["cancer"];
}
$pdf->antecedentes_familiares($dataFamiliar[0], $dataFamiliar[1], $dataFamiliar[2], $dataFamiliar[3], $dataFamiliar[4], $dataFamiliar[5]);

$rsptap = $reportes->get_reporte_ginecologico($_GET["id"]);
$dataginecologico = array();
foreach ($rsptap as $row) {
    $dataginecologico[0] = $row["embarazo"];
    $dataginecologico[1] = $row["anticonceptivos"];
    $dataginecologico[2] = $row["anticonceptivos_cual"];
    $dataginecologico[3] = $row["anticonceptivos_dosis"];
    $dataginecologico[4] = $row["climaterio"];
    $dataginecologico[5] = $row["climaterio_fecha"];
    $dataginecologico[6] = $row["reemplazo_hormonal"];
    $dataginecologico[7] = $row["reemplazo_hormonal_cual"];
    $dataginecologico[8] = $row["reemplazo_hormonal_dosis"];
}
$pdf->aspectos_ginecologicos(
    $dataginecologico[0],
    $dataginecologico[1],
    $dataginecologico[2],
    $dataginecologico[3],
    $dataginecologico[4],
    $dataginecologico[5],
    $dataginecologico[6],
    $dataginecologico[7],
    $dataginecologico[8]
);

$rsptap = $reportes->get_reporte_vida($_GET["id"]);
$datavida = array();
foreach ($rsptap as $row) {
    $datavida[0] = $row["actividad"];
    $datavida[1] = $row["ejercicio_tipo"];
    $datavida[2] = $row["ejercicio_frecuencia"];
    $datavida[3] = $row["ejercicio_duracion"];
    $datavida[4] = $row["ejercicio_cuando_inicio"];
}
$pdf->estilo_vida($datavida[0], $datavida[1], $datavida[2], $datavida[3], $datavida[4]);

$pdf->AddPage();
$rsptap = $reportes->get_reporte_consumo($_GET["id"]);
$dataConsumo = array();
foreach ($rsptap as $row) {
    $dataConsumo[0] = $row["alcohol"];
    $dataConsumo[1] = $row["alcohol_frecuencia"];
    $dataConsumo[2] = $row["alcohol_cantidad"];
    $dataConsumo[3] = $row["tabaco"];
    $dataConsumo[4] = $row["tabaco_frecuencia"];
    $dataConsumo[5] = $row["tabaco_cantidad"];
    $dataConsumo[6] = $row["cafe"];
    $dataConsumo[7] = $row["cafe_frecuencia"];
    $dataConsumo[8] = $row["cafe_cantidad"];
}
$pdf->consumo_frecuencia(
    $dataConsumo[0],
    $dataConsumo[1],
    $dataConsumo[2],
    $dataConsumo[3],
    $dataConsumo[4],
    $dataConsumo[5],
    $dataConsumo[6],
    $dataConsumo[7],
    $dataConsumo[8]
);
$rsptap = $reportes->get_reporte_dieteticos($_GET["id"]);
$dataDietetico = array();
foreach ($rsptap as $row) {
    $dataDietetico[0] = $row["comidasxdia"];
    $dataDietetico[1] = $row["desayuno"];
    $dataDietetico[2] = $row["desayuno_colacion"];
    $dataDietetico[3] = $row["comida"];
    $dataDietetico[4] = $row["colacion"];
    $dataDietetico[5] = $row["cena"];
    $dataDietetico[6] = $row["quien_prepara_alimentos"];
    $dataDietetico[7] = $row["come_entre_comidas"];
    $dataDietetico[8] = $row["come_entre_comidas_que"];
    $dataDietetico[9] = $row["modificacion_alimentos"];
    $dataDietetico[10] = $row["modificacion_alimentos_porque"];
    $dataDietetico[11] = $row["modificacion_alimentos_como"];
    $dataDietetico[12] = $row["apetito"];
    $dataDietetico[13] = $row["hora_mas_hambre"];
    $dataDietetico[14] = $row["alimentos_preferidos"];
    $dataDietetico[15] = $row["alimentos_preferidos"];
    $dataDietetico[16] = $row["alimentos_malestar"];
    $dataDietetico[17] = $row["alergia_alimentaria"];
    $dataDietetico[18] = $row["alergia_alimentaria_cual"];
    $dataDietetico[19] = $row["intolerancia_alimentaria"];
    $dataDietetico[20] = $row["intolerancia_alimentaria_cual"];
    $dataDietetico[21] = $row["toma_suplemento"];
    $dataDietetico[22] = $row["toma_suplemento_cual"];
    $dataDietetico[23] = $row["toma_suplemento_dosis"];
    $dataDietetico[24] = $row["toma_suplemento_porque"];
    $dataDietetico[25] = $row["toma_suplemento_varia"];
    $dataDietetico[26] = $row["toma_suplemento_varia_porque"];
    $dataDietetico[27] = $row["agrega_sal"];
    $dataDietetico[28] = $row["grasa_preparar_comida"];
    $dataDietetico[29] = $row["dieta_especial"];
    $dataDietetico[30] = $row["tipo_dieta"];
    $dataDietetico[31] = $row["tipo_dieta_hace_cuando"];
    $dataDietetico[32] = $row["tipo_dieta_cuanto_tiempo"];
    $dataDietetico[33] = $row["tipo_dieta_razon"];
    $dataDietetico[34] = $row["tipo_dieta_apego"];
    $dataDietetico[35] = $row["tipo_dieta_resultados"];
    $dataDietetico[36] = $row["medicamentos_baja_peso"];
    $dataDietetico[37] = $row["medicamentos_baja_peso_cuales"];
}
$pdf->indicadores_dieteticos($dataDietetico[0],$dataDietetico[1],$dataDietetico[2],$dataDietetico[3],$dataDietetico[4],
$dataDietetico[5],$dataDietetico[6],$dataDietetico[7],$dataDietetico[8],$dataDietetico[9],
$dataDietetico[10],$dataDietetico[11],$dataDietetico[12],$dataDietetico[13],$dataDietetico[14],
$dataDietetico[15],$dataDietetico[16],$dataDietetico[17],$dataDietetico[18],$dataDietetico[19],
$dataDietetico[20],$dataDietetico[21],$dataDietetico[22],$dataDietetico[23],$dataDietetico[24],
$dataDietetico[25],$dataDietetico[26],$dataDietetico[27],$dataDietetico[28],$dataDietetico[29],
$dataDietetico[30],$dataDietetico[31],$dataDietetico[32],$dataDietetico[33],$dataDietetico[34],
$dataDietetico[35],$dataDietetico[36],$dataDietetico[37]);
$pdf->Output("Reporte de Venta", "I");

ob_end_flush(); //liberar el espacio del buffer
