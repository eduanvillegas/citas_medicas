<?php
require('../fpdf181/fpdf.php');
class PDF_Invoice extends FPDF
{
	// Page header 
	function Header()
	{
		// GFG logo image 
		//	$this->Image('indice.jpg', 30, 8, 20);

		// GFG logo image 
		$this->Image('indice.jpg', 160, 8, 40, 30);

		// Set font-family and font-size 
		$this->SetFont('Times', 'B', 15);

		// Move to the right 
		$this->Cell(50);

		// Set the title of pages. 
		$this->Cell(30, 30, 'HISTORIAL CLINICO-NUTRIOLOGA', 0, 2, 'C');
		//$this->Cell(30, 20, 'HISTORIAL CLINICO-NUTRIOLOGA', 1, 2, 'C');
		// Break line with given space 
		$this->Ln(5);
	}
	function datos_personal($nombre, $edad, $sexo, $fecha, $est_civ, $escolaridad, $ocupacion, $direccion, $telefono, $mail)
	{
		$x1 = 20;
		$y1 = 40;
		/*$this->SetXY($x1, $y1);
		$this->SetFont('Arial', '', 10);
		$this->Cell(50, 4, 'FECHA:');;
		$this->Line($x1 + 14, $y1 + 4, $x1 + 70,  $y1 + 4);

		$this->SetXY($x1, $y1 + 5);
		$this->Cell(50, 4, 'EXPEDIENTE:');
		$this->Line($x1 + 25, $y1 + 9, $x1 + 70,  $y1 + 9);
*/
		// Set font-family and font-size 
		$this->SetFont('Times', 'B', 12);
		$this->SetXY($x1, $y1 + 15);
		$this->Cell(10, 4, 'DATOS PERSONALES');

		$this->SetFont('Arial', '', 10);
		$this->SetXY($x1, $y1 + 20);
		$this->Cell(50, 4, 'NOMBRE:');
		$this->Line($x1 + 18, $y1 + 23, 190,  $y1 + 23);
		$this->SetXY($x1 + 18, $y1 + 19);
		$this->MultiCell(50, 4, $nombre);

		$this->SetXY($x1, $y1 + 24);
		$this->Cell(50, 4, 'EDAD:');
		$this->SetXY($x1 + 18, $y1 + 24);
		$this->MultiCell(50, 4, $edad);
		$this->Line($x1 + 12, $y1 + 28, 100,  $y1 + 28);

		$this->SetXY($x1 + 90, $y1 + 24);
		$this->Cell(50, 4, 'SEXO:');
		$this->SetXY($x1 + 104, $y1 + 24);
		$this->MultiCell(50, 4, $sexo);
		$this->Line($x1 + 102, $y1 + 28, 190,  $y1 + 28);

		$this->SetXY($x1, $y1 + 29);
		$this->Cell(50, 4, 'FECHA NACIMIENTO:');
		$this->SetXY($x1 + 38, $y1 + 29);
		$this->MultiCell(50, 4, $fecha);
		$this->Line($x1 + 38, $y1 + 33, 100,  $y1 + 33);

		$this->SetXY($x1 + 90, $y1 + 29);
		$this->Cell(50, 4, 'ESTADO CIVIL:');
		$this->SetXY($x1 + 117, $y1 + 29);
		$this->MultiCell(50, 4, $est_civ);
		$this->Line($x1 + 115, $y1 + 33, 190,  $y1 + 33);

		$this->SetXY($x1, $y1 + 34);
		$this->Cell(50, 4, 'ESCOLARIDAD:');
		$this->SetXY($x1 + 30, $y1 + 34);
		$this->MultiCell(50, 4, $escolaridad);
		$this->Line($x1 + 28, $y1 + 38, 100,  $y1 + 38);

		$this->SetXY($x1 + 90, $y1 + 34);
		$this->Cell(50, 4, 'OCUPACION:');
		$this->SetXY($x1 + 115, $y1 + 34);
		$this->MultiCell(50, 4, $ocupacion);
		$this->Line($x1 + 115, $y1 + 38, 190,  $y1 + 38);

		$this->SetXY($x1, $y1 + 39);
		$this->Cell(50, 4, 'DIRECCION:');
		$this->SetXY($x1 + 22, $y1 + 39);
		$this->MultiCell(50, 4, $direccion);
		$this->Line($x1 + 22, $y1 + 43, 190,  $y1 + 43);

		$this->SetXY($x1, $y1 + 44);
		$this->Cell(50, 4, 'TELEFONO:');
		$this->SetXY($x1 + 22, $y1 + 44);
		$this->MultiCell(50, 4, $telefono);
		$this->Line($x1 + 22, $y1 + 48, 190,  $y1 + 48);

		$this->SetXY($x1, $y1 + 49);
		$this->Cell(50, 4, 'EMAIL:');
		$this->SetXY($x1 + 15, $y1 + 49);
		$this->MultiCell(50, 4, $mail);
		$this->Line($x1 + 12, $y1 + 53, 190,  $y1 + 53);

		/*$this->SetXY($x1, $y1 + 58);
		$this->Cell(50, 4, 'MOTIVO DE LA CONSULTA:');
		$this->Line($x1, $y1 + 68, 190,  $y1 + 68);
		$this->Line($x1, $y1 + 73, 190,  $y1 + 73);
		$this->Line($x1, $y1 + 78, 190,  $y1 + 78);
		$this->Line($x1, $y1 + 83, 190,  $y1 + 83);
		$this->Line($x1, $y1 + 88, 190,  $y1 + 88);
		$this->Line($x1, $y1 + 93, 190,  $y1 + 93);
		$this->Line($x1, $y1 + 98, 190,  $y1 + 98);
		$this->Line($x1, $y1 + 103, 190,  $y1 + 103);
		$this->Line($x1, $y1 + 108, 190,  $y1 + 108);*/
	}
	function datos_personal_expediente($expediente, $fecha, $motivo)
	{
		$x1 = 20;
		$y1 = 40;
		$this->SetXY($x1, $y1);
		$this->SetFont('Arial', '', 10);
		$this->Cell(50, 4, 'FECHA:');
		$this->SetXY($x1+20, $y1);
		$this->MultiCell( 150, 5,$fecha);
		$this->Line($x1 + 14, $y1 + 4, $x1 + 70,  $y1 + 4);
		$this->SetXY($x1+25, $y1+5);
		$this->MultiCell( 150, 5,$expediente);

		$this->SetXY($x1, $y1 + 5);
		$this->Cell(50, 4, 'EXPEDIENTE:');
		$this->Line($x1 + 25, $y1 + 9, $x1 + 70,  $y1 + 9);

		$this->SetXY($x1, $y1 + 58);
		$this->Cell(50, 4, 'MOTIVO DE LA CONSULTA:');
		$this->Line($x1, $y1 + 68, 190,  $y1 + 68);
		$this->Line($x1, $y1 + 73, 190,  $y1 + 73);
		$this->Line($x1, $y1 + 78, 190,  $y1 + 78);
		$this->Line($x1, $y1 + 83, 190,  $y1 + 83);
		$this->Line($x1, $y1 + 88, 190,  $y1 + 88);
		$this->Line($x1, $y1 + 93, 190,  $y1 + 93);
		$this->Line($x1, $y1 + 98, 190,  $y1 + 98);
		$this->Line($x1, $y1 + 103, 190,  $y1 + 103);
		$this->Line($x1, $y1 + 108, 190,  $y1 + 108);
		$this->SetXY($x1, $y1+63);
		$this->MultiCell( 150, 5,$motivo);
	}
	function antecedentes_salud(
		$diarrea,
		$estrenimiento,
		$gastritis,
		$ulcera,
		$pirosis,
		$colitis,
		$vomito,
		$dentadura,
		$otros,
		$observaciones,
		$enfermedad_diagnosticada,
		$ed_cuales,
		$fecha_diagnostico,
		$t_medicamento,
		$med_cual,
		$dosis,
		$laxante,
		$diuretico,
		$antiacido,
		$analgesico,
		$cirugia,
		$fecha_indigestion
	) {
		$x1 = 20;
		$y1 = 40;
		$this->SetXY($x1, $y1);
		$this->SetFont('Arial', 'B', 10);
		$this->Cell(50, 4, 'INDICADORES CLINICOS');
		$this->SetXY($x1, $y1 + 5);
		$this->Cell(50, 4, 'ANTECEDENTES SALUD/ENFERMEDAD');
		$this->SetXY($x1, $y1 + 15);
		$this->SetFont('Arial', '', 10);
		$this->Cell(50, 4, 'PROBLEMAS ACTUALES');

		$this->SetXY($x1, $y1 + 25);
		$this->SetFont('Arial', '', 10);
		$this->Cell(18, 4, 'DIARREA:');
		$this->Cell(18, 4, $diarrea);
		$this->Cell(32, 4, utf8_decode('ESTREÑIMIENTO:'));
		$this->Cell(18, 4, $estrenimiento);
		$this->Cell(22, 4, 'GASTRITIS:');
		$this->Cell(18, 4, $gastritis);
		$this->Cell(18, 4, 'ULCERA:');
		$this->Cell(18, 4, $ulcera);

		$this->SetXY($x1, $y1 + 30);
		$this->Cell(18, 4, 'PIROSIS:');
		$this->Cell(18, 4, $pirosis);
		$this->Cell(32, 4, utf8_decode('COLITIS:'));
		$this->Cell(18, 4, $colitis);
		$this->Cell(22, 4, 'VOMITO:');
		$this->Cell(18, 4, $vomito);
		$this->Cell(24, 4, 'DENTADURA:');
		$this->Cell(18, 4, $dentadura);

		$this->SetXY($x1, $y1 + 35);
		$this->Cell(15, 4, 'OTROS:');
		$this->Cell(40, 4, $otros);

		$this->SetXY($x1, $y1 + 45);
		$this->SetFont('Arial', '', 10);
		$this->Cell(18, 4, 'OBSERVACIONES:');
		$this->Line($x1, $y1 + 55, 190,  $y1 + 55);
		$this->Line($x1, $y1 + 60, 190,  $y1 + 60);
		$this->Line($x1, $y1 + 65, 190,  $y1 + 65);
		$this->SetXY($x1, $y1 + 51);
		$this->MultiCell( 150, 5,$observaciones);

		$this->SetXY($x1, $y1 + 70);
		$this->Cell(90, 4, 'PADECE ALGUNA ENFERMEDAD DIAGNOSTICADA');
		$this->Cell(20, 4, $enfermedad_diagnosticada);
		$this->Cell(20, 4, 'CUAL(ES)');
		$this->Cell(40, 4, $med_cual);

		$this->SetXY($x1, $y1 + 75);
		$this->Cell(47, 4, 'FECHA DE DIAGNOSTICO:');
		$this->Cell(40, 4, $fecha_diagnostico);

		$this->SetXY($x1, $y1 + 80);
		$this->Cell(55, 4, 'TOMA ALGUN MEDICAMENTO:');
		$this->Cell(20, 4, $t_medicamento);
		$this->Cell(20, 4, 'CUAL');
		$this->Cell(40, 4, $ed_cuales);

		$this->SetXY($x1, $y1 + 85);
		$this->Cell(15, 4, 'DOSIS');
		$this->Cell(50, 4, $dosis);
		$this->Cell(47, 4, 'FECHA DE INDEGESTION:');
		$this->Cell(40, 4, $fecha_indigestion);

		$this->SetXY($x1, $y1 + 90);
		$this->Cell(13, 4, 'TOMA:');
		$this->Cell(18, 4, 'LAXANTE:');
		$this->Cell(15, 4, $laxante);
		$this->Cell(24, 4, 'DIURETICOS:');
		$this->Cell(15, 4, $diuretico);
		$this->Cell(24, 4, 'ANTIACIDOS:');
		$this->Cell(15, 4, $antiacido);
		$this->Cell(28, 4, 'ANALGESICOS:');
		$this->Cell(15, 4, $analgesico);

		$this->SetXY($x1, $y1 + 95);
		$this->Cell(75, 4, 'LE HAN PRACTICANDO ALGUNA CIRUGIA:');
		$this->Cell(18, 4, $cirugia);
	}

	function antecedentes_familiares($a, $b, $c, $d, $e, $f)
	{
		$x1 = 20;
		$y1 = 140;

		$this->SetXY($x1, $y1 + 10);
		$this->SetFont('Arial', 'B', 10);
		$this->Cell(50, 4, 'ANTECEDENTES FAMILIARES');

		$this->SetXY($x1, $y1 + 20);
		$this->SetFont('Arial', '', 10);
		$this->Cell(20, 4, 'OBESIDAD:');
		$this->Cell(12, 4, $a);
		$this->Cell(20, 4, utf8_decode('DIABETES:'));
		$this->Cell(12, 4, $b);
		$this->Cell(10, 4, 'HTA:');
		$this->Cell(12, 4, $c);
		$this->Cell(44, 4, 'HIPERTRIGLICERIDEMIA:');
		$this->Cell(12, 4, $d);
		$this->SetXY($x1, $y1 + 25);
		$this->Cell(45, 4, 'HIPERCOLESTEROLEMIA:');
		$this->Cell(12, 4, $e);
		$this->Cell(18, 4, 'CANCER:');
		$this->Cell(12, 4, $f);
	}

	function aspectos_ginecologicos($a, $b, $c, $d, $e, $f, $g, $h, $i)
	{
		$x1 = 20;
		$y1 = 165;

		$this->SetXY($x1, $y1 + 10);
		$this->SetFont('Arial', 'B', 10);
		$this->Cell(50, 4, 'ASPECTOS GINECOLOGICOS');

		$this->SetXY($x1, $y1 + 20);
		$this->SetFont('Arial', '', 10);
		$this->Cell(37, 4, 'EMBARAZO ACTUAL:');
		$this->Cell(12, 4, $a);

		$this->SetXY($x1, $y1 + 25);
		$this->Cell(50, 4, 'ANTICONCEPTIVOS ORALES:');
		$this->Cell(12, 4, $b);

		$this->SetXY($x1, $y1 + 30);
		$this->Cell(15, 4, 'CUAL:');
		$this->Cell(12, 4, $c);

		$this->SetXY($x1, $y1 + 35);
		$this->Cell(15, 4, 'DOSIS:');
		$this->Cell(12, 4, $d);

		$this->SetXY($x1, $y1 + 40);
		$this->Cell(25, 4, 'CLIMATERIO:');
		$this->Cell(12, 4, $e);
		$this->Cell(15, 4, 'FECHA:');
		$this->Cell(12, 4, $f);

		$this->SetXY($x1, $y1 + 45);
		$this->Cell(70, 4, 'TERAPIA DE REEMPLAZO HORMONAL:');
		$this->Cell(12, 4, $g);

		$this->SetXY($x1, $y1 + 50);
		$this->Cell(15, 4, 'CUAL:');
		$this->Cell(12, 4, $h);

		$this->SetXY($x1, $y1 + 55);
		$this->Cell(15, 4, 'DOSIS:');
		$this->Cell(12, 4, $i);
	}

	function estilo_vida($a, $b, $c, $d, $e)
	{
		$x1 = 20;
		$y1 = 220;

		$this->SetXY($x1, $y1 + 10);
		$this->SetFont('Arial', 'B', 10);
		$this->Cell(50, 4, 'ESTILOS DE VIDA');
		$this->SetXY($x1, $y1 + 15);
		$this->Cell(50, 4, 'DIARIO DE ACTIVIDADES');

		$this->SetFont('Arial', '', 10);
		$this->SetXY($x1, $y1 + 25);
		$this->Cell(25, 4, 'Actividad:');
		$this->Cell(12, 4, $a);

		$this->SetXY($x1, $y1 + 30);
		$this->Cell(20, 4, 'EJERCICIO:');
		$this->Cell(10, 4, 'TIPO');
		$this->Cell(40, 4, $b);
		$this->Cell(25, 4, 'FRECUECIA:');
		$this->Cell(40, 4, $c);

		$this->SetXY($x1, $y1 + 35);
		$this->Cell(20, 4, 'DURACION:');
		$this->Cell(40, 4, $d);
		$this->Cell(30, 4, utf8_encode('CUANDO INICIO?'));
		$this->Cell(40, 4, $e);
	}

	function consumo_frecuencia($a, $b, $c, $d, $e, $f, $g, $h, $i)
	{
		$x1 = 20;
		$y1 = 40;

		$this->SetXY($x1, $y1);
		$this->SetFont('Arial', 'B', 10);
		$this->Cell(50, 4, 'CONSUMO DE (FRECUENCIA Y CANTIDAD)');

		$this->SetXY($x1, $y1 + 10);
		$this->SetFont('Arial', '', 10);
		$this->Cell(18, 4, 'ALCOHOL:');
		$this->Cell(18, 4, $a);
		$this->Cell(32, 4, 'FRECUENCIA:');
		$this->Cell(18, 4, $b);
		$this->Cell(22, 4, 'CANTIDAD:');
		$this->Cell(18, 4, $c);

		$this->SetXY($x1, $y1 + 15);
		$this->Cell(18, 4, 'TABACO:');
		$this->Cell(18, 4, $d);
		$this->Cell(32, 4, 'FRECUENCIA:');
		$this->Cell(18, 4, $e);
		$this->Cell(22, 4, 'CANTIDAD:');
		$this->Cell(18, 4, $f);

		$this->SetXY($x1, $y1 + 20);
		$this->Cell(18, 4, 'CAFE:');
		$this->Cell(18, 4, $g);
		$this->Cell(32, 4, 'FRECUENCIA:');
		$this->Cell(18, 4, $h);
		$this->Cell(22, 4, 'CANTIDAD:');
		$this->Cell(18, 4, $i);
	}

	function indicadores_dieteticos($a1,$b2,$c3,$d4,$e5,$f6,$g7,$h8,$i9,$j10,$k11,$l12,$m13,$n14,$ñ15,$o16,$p17,
	$q18,$r19,$s20,$t21,$u22,$v23,$w24,$x25,$y26,$z27,$a28,$b29,$c30,$d31,$e32,$f33,$g34,$h35,$i36,$j37,$k38)
	{
		$x1 = 20;
		$y1 = 60;

		$this->SetXY($x1, $y1 + 10);
		$this->SetFont('Arial', 'B', 10);
		$this->Cell(50, 4, 'INDICADORES DIETETICOS');

		$this->SetFont('Arial', '', 10);
		$this->SetXY($x1, $y1 + 20);
		$this->Cell(60, 4, 'CUANTAS COMIDAS HACE AL DIA:');
		$this->Cell(18, 4, $a1);
		$this->SetXY($x1, $y1 + 25);
		$this->Cell(22, 4, 'DESAYUNO:');
		$this->Cell(25, 4, $b2);
		$this->Cell(20, 4, 'COLACION:');
		$this->Cell(18, 4, $c3);
		$this->Cell(16, 4, 'COMIDA:');
		$this->Cell(25, 4, $d4);
		$this->Cell(22, 4, 'COLACION:');
		$this->Cell(18, 4, $e5);

		$this->SetXY($x1, $y1 + 30);
		$this->Cell(20, 4, 'CENA:');
		$this->Cell(60, 4, $f6);

		$this->SetXY($x1, $y1 + 35);
		$this->Cell(65, 4, 'QUIEN PREPARA SUS ALIMINETOS:');
		$this->Cell(60, 4, $g7);
		$this->SetXY($x1, $y1 + 40);
		$this->Cell(45, 4, 'COME ENTRE COMIDAS:');
		$this->Cell(40, 4, $h8);
		$this->Cell(10, 4, 'QUE:');
		$this->Cell(60, 4, $i9);

		$this->SetXY($x1, $y1 + 45);
		$this->Cell(170, 4, 'HA MODIFICADO SU ALIMENTACION EN LOS ULTIMOS 6 MESES(TRABAJO,ESTUDIO,O ACTVIDAD):');
		$this->SetXY($x1, $y1 + 50);
		$this->Cell(12, 4, $j10);
		$this->Cell(20, 4, 'POR QUE');
		$this->Cell(140, 4,$k11);

		$this->SetXY($x1, $y1 + 55);
		$this->Cell(20, 4, 'COMO:');
		$this->Cell(140, 4, $l12);

		$this->SetXY($x1, $y1 + 60);
		$this->Cell(20, 4, 'APETITO:');
		$this->Cell(140, 4, $m13);

		$this->SetXY($x1, $y1 + 65);
		$this->Cell(65, 4, 'A QUE HORA TIENES MAS HAMBRE:');
		$this->Cell(60, 4, $n14);

		$this->SetXY($x1, $y1 + 70);
		$this->Cell(50, 4, 'ALIMENTOS PREFERIDOS:');
		$this->Cell(60, 4, $ñ15);

		$this->SetXY($x1, $y1 + 75);
		$this->Cell(70, 4, 'ALIMENTOS QUE NO LE AGRADAN:');
		$this->Cell(50, 4, $o16);

		$this->SetXY($x1, $y1 + 80);
		$this->Cell(110, 4, 'ALIMENTOS QUE NO LES CAUSAN MALESTAR(ESPECIFICAR):');
		$this->Cell(50, 4, $p17);

		$this->SetXY($x1, $y1 + 85);
		$this->Cell(50, 4, 'ALERGIA ALIMENTARIA:');
		$this->Cell(20, 4, $q18);
		$this->Cell(20, 4, 'CUAL:');
		$this->Cell(80, 4, $r19);

		$this->SetXY($x1, $y1 + 90);
		$this->Cell(60, 4, 'INTOLERANCIA ALIMENTARIA:');
		$this->Cell(20, 4, $s20);
		$this->Cell(20, 4, 'CUAL:');
		$this->Cell(80, 4, $t21);

		$this->SetXY($x1, $y1 + 95);
		$this->Cell(80, 4, 'TOMA ALGUN SUPLEMENTO/COMPLEMENTO:');
		$this->Cell(20, 4, $u22);

		$this->SetXY($x1, $y1 + 100);
		$this->Cell(12, 4, 'CUAL:');
		$this->Cell(60, 4, $v23);
		$this->Cell(14, 4, 'DOSIS:');
		$this->Cell(60, 4, $w24);

		$this->SetXY($x1, $y1 + 105);
		$this->Cell(18, 4, 'PORQUE:');
		$this->Cell(160, 4, $x25);

		$this->SetXY($x1, $y1 + 110);
		$this->Cell(120, 4, 'SU CONSUMO VARIA CUANDO ESTA TRISTE, NERVIOSO O ANSIOSO:');
		$this->Cell(20, 4, $y26);

		$this->SetXY($x1, $y1 + 115);
		$this->Cell(18, 4, 'PORQUE:');
		$this->Cell(160, 4, $z27);

		$this->SetXY($x1, $y1 + 120);
		$this->Cell(80, 4, 'AGREGA SAL A LA COMIDA YA PREPARADA:');
		$this->Cell(20, 4, $a28);

		$this->SetXY($x1, $y1 + 125);
		$this->Cell(110, 4, 'QUE GRASA UTILIZAN EN CASA PARA PREPARAR SU COMIDA:');
		$this->Cell(40, 4, $b29);

		$this->SetXY($x1, $y1 + 130);
		$this->Cell(70, 4, 'HA LLEVADO ALGUNA DIETA ESPECIAL:');
		$this->Cell(40, 4, $c30);

		$this->SetXY($x1, $y1 + 135);
		$this->Cell(40, 4, 'QUE TIPO DE DIETA:');
		$this->Cell(40, 4, $d31);
		$this->Cell(30, 4, 'HACE CUANTO:');
		$this->Cell(40, 4,  $e32);

		$this->SetXY($x1, $y1 + 140);
		$this->Cell(40, 4, 'POR CUANTO TIEMPO:');
		$this->Cell(40, 4,$f33);
		$this->Cell(30, 4, 'PORQUE RAZON:');
		$this->Cell(40, 4, $g34);

		$this->SetXY($x1, $y1 + 145);
		$this->Cell(60, 4, 'QUE TANTO SE APEGO A ELLA:');
		$this->Cell(40, 4, $h35);
		$this->Cell(50, 4, 'OBTUVO LOS RESULTADOS:');
		$this->Cell(20, 4, $i36);

		$this->SetXY($x1, $y1 + 150);
		$this->Cell(100, 4, 'HA UTILIZADO MEDICAMENTOS PARA BAJAR DE PESO:');
		$this->Cell(40, 4, $j37);

		$this->SetXY($x1, $y1 + 155);
		$this->Cell(100, 4, 'CUALES:');
		$this->Cell(40, 4, $k38);
	}
	// Page footer
	function Footer()
	{
		// Position at 1.5 cm from bottom 
		$this->SetY(-20);

		// Set font-family and font-size of footer. 
		$this->SetFont('Arial', 'I', 8);
		$this->Cell(50);
		$this->Cell(0, 10, 'LIC. EN NUTRICION. ALEXIS SIERRANO VILLAFUERTE ', 0, 0, 'R');
		$this->SetY(-15);
		$this->Cell(0, 10, 'CEDULA PROFESIONAL.757188', 0, 0, 'R');
		// set page number 
		//	$this->Cell(0, 10, 'Page ' . $this->PageNo() .
		//		'/{nb}', 0, 0, 'C');
	}
	function historial()
	{ }
}
