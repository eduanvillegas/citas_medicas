<?php
require_once("../config/conexion.php");
if (isset($_SESSION["id_usuario"])) {
    ?>

    <?php
    require_once("header.php");
    ?>
    <?php if ($_SESSION["medico"] == 1) {
        ?>
        <!--Contenido-->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
                <div id="resultados_ajax"></div>
                <h2>Listado de Medico</h2>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h1 class="box-title">
                                    <button class="btn btn-primary btn-lg" id="add_button" onclick="limpiar()" data-toggle="modal" data-target="#categoriaModal"><i class="fa fa-plus" aria-hidden="true"></i> Nuevo Medico</button></h1>
                                <div class="box-tools pull-right">
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <!-- centro -->
                            <div class="panel-body table-responsive">
                                <table id="categoria_data" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="10%">Nombre</th>
                                            <th width="5%">Edad</th>
                                            <th width="5%">Sexo</th>
                                            <th width="5%">Fecha Nacimiento</th>
                                            <th width="10%">Direccion</th>
                                            <th width="5%">Telefono</th>
                                            <th width="10%">Email</th>
                                            <th width="10%">Opciones</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <!--Fin centro -->
                        </div><!-- /.box -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </section><!-- /.content -->

        </div><!-- /.content-wrapper -->
        <!--Fin-Contenido-->

        <!--FORMULARIO VENTANA MODAL-->
        <div id="categoriaModal" class="modal fade">
            <div class="modal-dialog">
                <form method="post" id="categoria_form">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Agregar Paciente</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Nombre</label>
                                <input type="text" name="medico" id="medico" class="form-control" placeholder="" required />
                            </div>

                            <div class="form-group">
                                <label>Sexo</label>
                                <select class="form-control" id="sexo" name="sexo" required>
                                    <option value="">-- Elige el Sexo --</option>
                                    <option value="hombre" selected>Hombre</option>
                                    <option value="mujer">Mujer</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Fecha Nacimiento</label>
                                <input type="date" name="fecha_nac" id="fecha_nac" value="1970-01-01" class="form-control" required />
                            </div>

                            <div class="form-group">
                                <label>Edad</label>
                                <input type="number" name="edad" id="edad" class="form-control" placeholder="Edad" readonly/>
                            </div>

                            <div class=" form-group">
                                <label>Estado Civil</label>
                                <select class="form-control" id="est_civ" name="est_civ" required>
                                    <option value="">-- Elige Estado Civil --</option>
                                    <option value="soltero" selected>Soltero</option>
                                    <option value="casado">Casado</option>
                                    <option value="divorciado" selected>Divorciado</option>
                                    <option value="viudo">Viudo</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Escolaridad</label>
                                <input type="text" name="escolaridad" id="escolaridad" class="form-control" placeholder="Escolaridad" required />
                            </div>
                            <div class="form-group">
                                <label>Ocupacion</label>
                                <input type="text" name="ocupacion" id="ocupacion" class="form-control" placeholder="Ocupacion" required />
                            </div>
                            <div class="form-group">
                                <label>Direccion</label>
                                <input type="text" name="direccion" id="direccion" class="form-control" placeholder="Direccion" required />
                            </div>
                            <div class="form-group">
                                <label>Telefono</label>
                                <input type="text" name="telefono" id="telefono" class="form-control" placeholder="Telefono" required />
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" name="correo" id="correo" class="form-control" placeholder="Email" required />
                            </div>
                        </div>

                        <div class="modal-footer">
                            <input type="hidden" name="id_usuario" id="id_usuario" value="<?php echo $_SESSION["id_usuario"]; ?>" />
                            <input type="hidden" name="id_medico" id="id_medico" />
                            <button type="submit" name="action" id="btnGuardar" class="btn btn-success pull-left" value="Add"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
                            <button type="button" onclick="limpiar()" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Cerrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!--FIN FORMULARIO VENTANA MODAL-->

    <?php  } else {
        require("noacceso.php");
    }
    ?>
    <!--CIERRE DE SESSION DE PERMISO -->

    <?php
    require_once("footer.php");
    ?>

    <script type="text/javascript" src="js/medico.js"></script>
<?php
} else {
    header("Location:" . Conectar::ruta() . "index.php");
}
?>