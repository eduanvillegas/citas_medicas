<?php
require_once("../config/conexion.php");
if (isset($_SESSION["id_usuario"])) {
    require_once("../modelos/Pacientes.php");
    $paciente = new Pacientes();
    $pacientes = $paciente->get_paciente();
    ?>

    <?php
    require_once("header.php");
    ?>
    <?php if ($_SESSION["paciente"] == 1) {
        ?>
        <!--Contenido-->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
                <div id="resultados_ajax"></div>
                <h2>Historial Clinico</h2>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <!-- centro -->
                            <div class="panel-body table-responsive">
                                <div class="form-inline col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                                    <label>Paciente</label>
                                    <select name="id_paciente" id="id_paciente" class="form-control selectpicker" data-live-search="true">
                                        <option value="0">SELECCIONE</option>
                                        <?php
                                        for ($i = 0; $i < sizeof($pacientes); $i++) {
                                            ?>
                                            <option value="<?php echo $pacientes[$i]["id_persona"] ?>"><?php echo $pacientes[$i]["nombre"] ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                    <button type="button" class="btn btn-success" onclick="listar()">mostrar</button>
                                   <!-- <button type="button" class="btn btn-success" onclick="imprimir()">Imprimir</button-->
                                </div>
                                <table id="categoria_data" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="10%">Nombre</th>
                                            <th width="5%">Edad</th>
                                            <th width="5%">Sexo</th>
                                            <th width="5%">Fecha Nacimiento</th>
                                            <th width="10%">Direccion</th>
                                            <th width="5%">Telefono</th>
                                            <th width="10%">Email</th>
                                            <th width="10%">Expediente</th>
                                            <th width="10%">Opciones</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <!--Fin centro -->
                        </div><!-- /.box -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </section><!-- /.content -->

        </div><!-- /.content-wrapper -->
        <!--Fin-Contenido-->

    <?php  } else {
        require("noacceso.php");
    }
    ?>
    <!--CIERRE DE SESSION DE PERMISO -->

    <?php
    require_once("footer.php");
    ?>

    <script type="text/javascript" src="js/reporte_historial_clinico.js"></script>
<?php
} else {
    header("Location:" . Conectar::ruta() . "index.php");
}
?>