<?php
require_once("../config/conexion.php");
if (isset($_SESSION["id_usuario"])) {
    require_once("../modelos/Pacientes.php");
    $paciente = new Pacientes();
    $pacientes = $paciente->get_paciente();
    ?>

<?php
    require_once("header.php");
    ?>
<?php if ($_SESSION["hospital"] == 1) {
        ?>
<!--Contenido-->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
        <div class="box">
            <br />
            <h2 align="center">Cuestionario </h2><br />
            <? //php echo $message; 
                    ?>

            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active_tab1" style="border:1px solid #ccc" id="list_expediente_details">Expediente</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link inactive_tab1" style="border:1px solid #ccc" id="list_login_details">Antecedentes Salud/Enfermedad</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link inactive_tab1" id="list_personal_details" style="border:1px solid #ccc">Antecedentes Familiares</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link inactive_tab1" id="list_contact_details" style="border:1px solid #ccc">Aspectos Ginecologicos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link inactive_tab1" id="list_diario_actividades" style="border:1px solid #ccc">Diario de Actividades</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link inactive_tab1" id="list_consumo" style="border:1px solid #ccc">Consumo de (Frecuencia y Cantidad)</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link inactive_tab1" id="list_indicadores_dieteticos" style="border:1px solid #ccc">Indicadores Dieteticos</a>
                </li>
            </ul>

            <form method="post" id="expediente_form">
                <div class="tab-content" style="margin-top:16px;">
                    <div class="tab-pane active" id="expediente_details">
                        <div class="panel panel-default">
                            <div class="panel-heading">Problemas Actuales</div>
                            <div class="panel-body">
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <label>Nombre del Paciente</label>
                                    <select name="id_persona" id="id_persona" class="form-control selectpicker" data-live-search="true">
                                        <option value="">--Seleccione--</option>
                                        <?php
                                                for ($i = 0; $i < sizeof($pacientes); $i++) {
                                                    ?>

                                        <option value="<?php echo $pacientes[$i]["id_persona"] ?>"><?php echo $pacientes[$i]["nombre"] ?></option>
                                        <?php
                                                }
                                                ?>
                                    </select>
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <label>Expediente</label>
                                    <input type="text" name="expediente" id="expediente" value="<?php echo date("Y-m-d"); ?>" class="form-control" />
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <label>Fecha de Expediente</label>
                                    <input type="date" name="fechaExp" id="fechaExp" value="<?php echo date("Y-m-d"); ?>" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label for="">Observaciones</label>
                                    <textarea class="form-control rounded-0" id="observacionesExp" name="observacionesExp" rows="3"></textarea>
                                </div>

                            </div>
                            <div class="panel-footer" align="center">
                                <button type="sumit" name="btn_expediente_guardar" id="btn_expediente_guardar" class="btn btn-success btn-lg">Guardar</button>
                                <button type="button" name="btn_expediente_details" id="btn_expediente_details" class="btn btn-info btn-lg">Siguiente</button>
                            </div>
                        </div>
                    </div>
            </form>


            <form method="post" id="register_form">
                <div class="tab-content" style="margin-top:16px;">
                    <div class="tab-pane fade" id="login_details">
                        <div class="panel panel-default">
                            <div class="panel-heading">Problemas Actuales</div>
                            <div class="panel-body">
                                <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label for="">Diarrea</label>
                                    <select class="form-control" id="Ediarrea" name="Ediarrea">
                                        <option value="No">No</option>
                                        <option value="Si">Si</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label for="">Estreñiminto</label>
                                    <select class="form-control" id="Eestrenimiento" name="Eestrenimiento">
                                        <option value="No">No</option>
                                        <option value="Si">Si</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label for="">Gastritis</label>
                                    <select class="form-control" id="Egastritis" name="Egastritis">
                                        <option value="No">No</option>
                                        <option value="Si">Si</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label for="">Ulcera</label>
                                    <select class="form-control" id="Eulcera" name="Eulcera">
                                        <option value="No">No</option>
                                        <option value="Si">Si</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label for="">Pirosis</label>
                                    <select class="form-control" id="Epirosis" name="Epirosis">
                                        <option value="No">No</option>
                                        <option value="Si">Si</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label for="">Colitis</label>
                                    <select class="form-control" id="Ecolitis" name="Ecolitis">
                                        <option value="No">No</option>
                                        <option value="Si">Si</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label for="">Vomito</label>
                                    <select class="form-control" id="Evomito" name="Evomito">
                                        <option value="No">No</option>
                                        <option value="Si">Si</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label for="">Dentadura</label>
                                    <select class="form-control" id="Edentadura" name="Edentadura">
                                        <option value="No">No</option>
                                        <option value="Si">Si</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label for="">Otros</label>
                                    <input type="text" name="otros" id="otros" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label for="">Observaciones</label>
                                    <textarea class="form-control rounded-0" id="observaciones" name="observaciones" rows="3"></textarea>
                                </div>
                                <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label for="">Padece alguna enfermedad</label>
                                    <select class="form-control" id="PadEnfermedad" name="PadEnfermedad">
                                        <option value="No">No</option>
                                        <option value="Si">Si</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label>Cual(es)</label>
                                    <input type="text" name="PadEnfermedadCual" id="PadEnfermedadCual" class="form-control" />
                                </div>
                                <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label>Fecha de diagnostico</label>
                                    <input type="date" name="fechaDigestion" id="fechaDigestion" value="<?php echo date("Y-m-d"); ?>" class="form-control" />
                                </div>

                                <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <label>Toma algun medicamento</label>
                                    <input type="text" name="Tmedicamento" id="Tmedicamento" class="form-control" />
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <label>Cual(es)</label>
                                    <input type="text" name="TmedicamentoCual" id="TmedicamentoCual" class="form-control" />
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <label>Dosis</label>
                                    <input type="text" name="dosis" id="dosis" class="form-control" />
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <label>Fecha de diagnostico</label>
                                    <input type="date" name="FechaIngestion" id="FechaIngestion" value="<?php echo date("Y-m-d"); ?>" class="form-control" />
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <label for="">Laxante</label>
                                    <select class="form-control" id="laxante" name="laxante">
                                        <option value="No">No</option>
                                        <option value="Si">Si</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <label for="">Diuretico</label>
                                    <select class="form-control" id="diuretico" name="diuretico">

                                        <option value="No">No</option>
                                        <option value="Si">Si</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <label for="">Antiacido</label>
                                    <select class="form-control" id="antiacido" name="antiacido">

                                        <option value="No">No</option>
                                        <option value="Si">Si</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <label for="">Analgesico</label>
                                    <select class="form-control" id="analgesico" name="analgesico">

                                        <option value="No">No</option>
                                        <option value="Si">Si</option>
                                    </select>
                                </div>

                                <div class="form-group ">
                                    <label>Le Han Practicado Alguna Cirugia</label>
                                    <input type="text" name="cirugia" id="cirugia" class="form-control" />
                                </div>
                            </div>
                            <div class="panel-footer" align="center">
                                <input type="hidden" name="id_persona6" id="id_persona6" class="form-control" />
                                <button type="button" name="previous_btn_login_details" id="previous_btn_login_details" class="btn btn-default btn-lg">Anterior</button>
                                <button type="sumit" name="btn_login_guardar" id="btn_login_guardar" class="btn btn-success btn-lg">Guardar</button>
                                <button type="button" name="btn_login_details" id="btn_login_details" class="btn btn-info btn-lg">Siguiente</button>
                            </div>
                        </div>
                    </div>
            </form>

            <div class="tab-pane fade" id="personal_details">
                <form method="post" id="afamiliares_form">
                    <div class="panel panel-default">
                        <div class="panel-heading">Antedecentes Familiares</div>
                        <div class="panel-body">
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label for="">Obesidad</label>
                                <select class="form-control" id="obecidad" name="obecidad">
                                    <option value="No">No</option>
                                    <option value="Si">Si</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label for="">Diabetes</label>
                                <select class="form-control" id="diabetes" name="diabetes">
                                    <option value="No">No</option>
                                    <option value="Si">Si</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label for="">Hta</label>
                                <select class="form-control" id="hta" name="hta">
                                    <option value="No">No</option>
                                    <option value="Si">Si</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label for="">Hipertrigliceridemia</label>
                                <select class="form-control" id="hipertrigliceridemia" name="hipertrigliceridemia">
                                    <option value="No">No</option>
                                    <option value="Si">Si</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label for="">Hipercolesterolemia</label>
                                <select class="form-control" id="hipercolesterolemia" name="hipercolesterolemia">
                                    <option value="No">No</option>
                                    <option value="Si">Si</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label for="">Cancer</label>
                                <select class="form-control" id="cancer" name="cancer">
                                    <option value="No">No</option>
                                    <option value="Si">Si</option>
                                </select>
                            </div>
                        </div>
                        <div class="panel-footer" align="center">
                            <input type="hidden" name="id_persona1" id="id_persona1" class="form-control" />
                            <button type="button" name="previous_btn_personal_details" id="previous_btn_personal_details" class="btn btn-default btn-lg">Anterior</button>
                            <button type="sumit" name="btn_personal_guardar" id="btn_personal_guardar" class="btn btn-success btn-lg">Guardar</button>
                            <button type="button" name="btn_personal_details" id="btn_personal_details" class="btn btn-info btn-lg">Siguiente</button>
                        </div>
                    </div>
                </form>
            </div>


            <div class="tab-pane fade" id="contact_details">
                <form method="post" id="aginecologicos_form">
                    <div class="panel panel-default">
                        <div class="panel-heading">Aspecto Ginecologicos</div>
                        <div class="panel-body">
                            <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <label for="">Embarazo actual</label>
                                <select class="form-control" id="embarazo" name="embarazo">
                                    <option value="">--Seleccione--</option>
                                    <option value="No">No</option>
                                    <option value="Si">Si</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <label for="">Acticonceptivo Orales</label>
                                <select class="form-control" id="anticonceptivos" name="anticonceptivos">
                                    <option value="">--Seleccione--</option>
                                    <option value="No">No</option>
                                    <option value="Si">Si</option>
                                </select>
                            </div>

                            <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <label for="">Cual</label>
                                <input type="text" name="anticonceptivos_cual" id="anticonceptivos_cual" class="form-control" />
                            </div>
                            <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <label for="">Dosis</label>
                                <input type="text" name="anticonceptivos_dosis" id="anticonceptivos_dosis" class="form-control" />
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label for="">Climaterio</label>
                                <select class="form-control" id="climaterio" name="climaterio">
                                    <option value="">--Seleccione--</option>
                                    <option value="No">No</option>
                                    <option value="Si">Si</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label for="">Fecha</label>
                                <input type="date" name="climaterio_fecha" id="climaterio_fecha" value="<?php echo date("Y-m-d"); ?>" class="form-control" />
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label for="">Terapia de reemplazo hormonal</label>
                                <select class="form-control" id="terapia" name="terapia">
                                    <option value="">--Seleccione--</option>
                                    <option value="No">No</option>
                                    <option value="Si">Si</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label for="">Cual</label>
                                <input type="text" name="terapia_cual" id="terapia_cual" class="form-control" />
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12"">
                                                                <label for="">Dosis</label>
                                                                <input type=" text" name="terapia_dosis" id="terapia_dosis" class="form-control" />
                        </div>
                    </div>
                    <div class="panel-footer" align="center">
                        <input type="hidden" name="id_persona2" id="id_persona2" class="form-control" />
                        <button type="button" name="previous_btn_contact_details" id="previous_btn_contact_details" class="btn btn-default btn-lg">Anterior</button>
                        <button type="sumit" name="btn_contact_guardar" id="btn_contact_guardar" class="btn btn-success btn-lg">Guardar</button>
                        <button type="button" name="btn_contact_details" id="btn_contact_details" class="btn btn-info btn-lg">Siguiente</button>
                    </div>
            </div>
            </form>
        </div>

        <div class="tab-pane fade" id="dactividades_details">
            <form method="post" id="dactividades_form">
                <div class="panel panel-default">
                    <div class="panel-heading">Diario de Actividades</div>
                    <div class="panel-body">
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Actividad</label>
                            <select class="form-control" id="actividad" name="actividad">
                                <option value="muy ligera">Muy ligera</option>
                                <option value="ligera">Ligera</option>
                                <option value="moderada">Moderada</option>
                                <option value="pesada">Pesada</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Ejercicio: Tipo</label>
                            <input type="text" id="ejercicio_tipo" name="ejercicio_tipo" class="form-control">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Frecuencia</label>
                            <input type="text" id="ejercicio_frecuencia" name="ejercicio_frecuencia" class="form-control">
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label for="">Duracion</label>
                            <input type="text" id="ejercicio_duracion" name="ejercicio_duracion" class="form-control">
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label for="">¿Cuanto inicio?</label>
                            <input type="date" id="ejercicio_cuando_inicio" name="ejercicio_cuando_inicio" value="<?php echo date("Y-m-d"); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="panel-footer" align="center">
                        <input type="hidden" name="id_persona3" id="id_persona3" class="form-control" />
                        <button type="button" name="previous_btn_dactividades_details" id="previous_btn_dactividades_details" class="btn btn-default btn-lg">Anterior</button>
                        <button type="sumit" name="btn_guardar_dactividades_details" id="btn_guardar_dactividades_details" class="btn btn-success btn-lg">Guardar</button>
                        <button type="button" name="btn_dactividades_details" id="btn_dactividades_details" class="btn btn-info btn-lg">Siguiente</button>
                    </div>
                </div>
            </form>
        </div>

        <div class="tab-pane fade" id="consumo_details">
            <form method="post" id="consumo_form">
                <div class="panel panel-default">
                    <div class="panel-heading">Consumo de(Frecuencia y Cantidad)</div>
                    <div class="panel-body">
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Alcohol:</label>
                            <select class="form-control" id="consumo_alcohol" name="consumo_alcohol">
                                <option value="No">No</option>
                                <option value="Si">Si</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Frecuencia</label>
                            <input type="text" id="consumo_alcohol_frecuencia" name="consumo_alcohol_frecuencia" class="form-control">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Cantidad</label>
                            <input type="text" id="consumo_alcohol_cantidad" name="consumo_alcohol_cantidad" class="form-control">
                        </div>

                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Tabaco:</label>
                            <select class="form-control" id="consumo_tabaco" name="consumo_tabaco">
                                <option value="No">No</option>
                                <option value="Si">Si</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Frecuencia</label>
                            <input type="text" id="consumo_tabaco_frecuencia" name="consumo_tabaco_frecuencia" class="form-control">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Cantidad</label>
                            <input type="text" id="consumo_tabaco_cantidad" name="consumo_tabaco_cantidad" class="form-control">
                        </div>

                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Cafe:</label>
                            <select class="form-control" id="consumo_cafe" name="consumo_cafe">
                                <option value="No">No</option>
                                <option value="Si">Si</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Frecuencia</label>
                            <input type="text" id="consumo_cafe_frecuencia" name="consumo_cafe_frecuencia" class="form-control">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Cantidad</label>
                            <input type="text" id="consumo_cafe_cantidad" name="consumo_cafe_cantidad" class="form-control">
                        </div>
                    </div>
                    <div class="panel-footer" align="center">
                        <input type="hidden" name="id_persona4" id="id_persona4" class="form-control" />
                        <button type="button" name="previous_btn_consumo_details" id="previous_btn_consumo_details" class="btn btn-default btn-lg">Anterior</button>
                        <button type="sumit" name="btn_guardar_consumo_details" id="btn_guardar_consumo_details" class="btn btn-success btn-lg">Guardar</button>
                        <button type="button" name="btn_consumo_details" id="btn_consumo_details" class="btn btn-info btn-lg">Siguiente</button>
                    </div>
                </div>
            </form>
        </div>

        <div class="tab-pane fade" id="indicadores_dieteticos_details">
            <form method="post" id="idieteticos_form">
                <div class="panel panel-default">
                    <div class="panel-heading">Indicadores Dieteticos</div>
                    <div class="panel-body">
                        <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <label for="">#Comidas al dia</label>
                            <input type="text" id="comidasxdia" name="comidasxdia" class="form-control">
                        </div>
                        <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <label for="">Desayuno</label>
                            <input type="text" id="desayuno" name="desayuno" class="form-control">
                        </div>
                        <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <label for="">Colacion</label>
                            <input type="text" id="desayuno_colacion" name="desayuno_colacion" class="form-control">
                        </div>
                        <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <label for="">Comida</label>
                            <input type="text" id="comida" name="comida" class="form-control">
                        </div>
                        <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <label for="">Colacion</label>
                            <input type="text" id="comida_colacion" name="comida_colacion" class="form-control">
                        </div>
                        <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <label for="">Cena</label>
                            <input type="text" id="cena" name="cena" class="form-control">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Quien Prepara sus Alimentos:</label>
                            <input type="text" id="quienprepaali" name="quienprepaali" class="form-control">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Come entre comidas:</label>
                            <input type="text" id="comeEntreComidas" name="comeEntreComidas" class="form-control">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Que:</label>
                            <input type="text" id="QueComeEntreComidas" name="QueComeEntreComidas" class="form-control">
                        </div>
                        <div class="form-group col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <label for="">Ha modificado su alimentas en los ultimos 6 meses(trabajo,estudio, o actividad):</label>
                            <select class="form-control" id="modificacionAlimentos" name="modificacionAlimentos">
                                <option value="No">No</option>
                                <option value="Si">Si</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Porque:</label>
                            <input type="text" id="modificacionAlimentosPorque" name="modificacionAlimentosPorque" class="form-control">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Como:</label>
                            <input type="text" id="modificacionAlimentosComo" name="modificacionAlimentosComo" class="form-control">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Apetito:</label>
                            <select class="form-control" id="modificacionAlimentosApetito" name="modificacionAlimentosApetito">
                                <option value="bueno">Bueno</option>
                                <option value="malo">Malo</option>
                                <option value="regular">Regular</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">A que hora tienes mas hambre:</label>
                            <input type="text" id="horaHambre" name="horaHambre" class="form-control">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Alimentos preferidos:</label>
                            <input type="text" id="alimentospreferidos" name="alimentospreferidos" class="form-control">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Alimentos que no le agradan:</label>
                            <input type="text" id="alimentosAgradan" name="alimentosAgradan" class="form-control">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Alimentos que les causan malestar:</label>
                            <input type="text" id="alimentosMalestar" name="alimentosMalestar" class="form-control">
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label for="">Alergia Alimentaria</label>
                            <input type="text" id="alergiaAlimentaria" name="alergiaAlimentaria" class="form-control">
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label for="">Cual</label>
                            <input type="text" id="alergiaAlimentariaCual" name="alergiaAlimentariaCual" class="form-control">
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label for="">Intolerancia Alimentaria</label>
                            <input type="text" id="intoleranciaAlimentaria" name="intoleranciaAlimentaria" class="form-control">
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label for="">Cual</label>
                            <input type="text" id="intoleranciaAlimentariaCual" name="intoleranciaAlimentariaCual" class="form-control">
                        </div>
                        <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <label for="">Toma Algun Suplemento</label>
                            <select class="form-control" id="tomaSuplemento" name="tomaSuplemento">
                                <option value="No">No</option>
                                <option value="Si">Si</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <label for="">Cual</label>
                            <input type="text" id="tomaSuplementoCual" name="tomaSuplementoCual" class="form-control">
                        </div>
                        <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <label for="">Dosis</label>
                            <input type="text" id="tomaSuplementoDosis" name="tomaSuplementoDosis" class="form-control">
                        </div>
                        <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <label for="">Porque</label>
                            <input type="text" id="tomaSuplementoPorque" name="tomaSuplementoPorque" class="form-control">
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label for="">Su consumo varia cuando esta triste,nervioso o ansioso:</label>
                            <input type="text" id="consumoVaria" name="consumoVaria" class="form-control">
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label for="">porque:</label>
                            <input type="text" id="consumoVariaPorque" name="consumoVariaPorque" class="form-control">
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label for="">Agrega sal a la comida ya preparada</label>
                            <select class="form-control" id="agregaSalComida" name="agregaSalComida">
                                <option value="No">No</option>
                                <option value="Si">Si</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label for="">Que grasa utilizan en casa para preparar su comida:</label>
                            <input type="text" id="utilizanGrasa" name="utilizanGrasa" class="form-control">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Ha llevado alguna dieta especial:</label>
                            <input type="text" id="dietaEspecial" name="dietaEspecial" class="form-control">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Que tipo de dieta:</label>
                            <input type="text" id="tipoDieta" name="tipoDieta" class="form-control">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Hace cuanto:</label>
                            <input type="text" id="tipoDietacuando" name="tipoDietacuando" class="form-control">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Por cuanto tiempo:</label>
                            <input type="text" id="tipoDietaTiempo" name="tipoDietaTiempo" class="form-control">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Porque Razon:</label>
                            <input type="text" id="tipoDietaRazon" name="tipoDietaRazon" class="form-control">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Que tanto se apego a ella:</label>
                            <input type="text" id="tipoDietaApego" name="tipoDietaApego" class="form-control">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Obtuvo los resultados:</label>
                            <input type="text" id="tipoDietaResultados" name="tipoDietaResultados" class="form-control">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Ha utilizado medicamento para bajar de peso:</label>
                            <input type="text" id="medicamentoBajarPeso" name="medicamentoBajarPeso" class="form-control">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label for="">Cuales:</label>
                            <input type="text" id="medicamentoBajarPesoCuales" name="medicamentoBajarPesoCuales" class="form-control">
                        </div>
                    </div>
                    <div class="panel-footer" align="center">
                        <input type="hidden" name="id_persona5" id="id_persona5" class="form-control" />
                        <button type="button" name="previous_btn_indicadores_dieteticos_details" id="previous_btn_indicadores_dieteticos_details" class="btn btn-default btn-lg">Anterior</button>
                        <button type="sumit" name="btn_guardar_indicadores_dieteticos_details" id="btn_guardar_indicadores_dieteticos_details" class="btn btn-success btn-lg">Guardar</button>
                    </div>
                </div>
            </form>
        </div>

</div>
</section><!-- /.content -->

</div><!-- /.content-wrapper -->
<!--Fin-Contenido-->


<?php  } else {
        require("noacceso.php");
    }
    ?>
<!--CIERRE DE SESSION DE PERMISO -->

<?php
    require_once("footer.php");
    ?>

<script type="text/javascript" src="js/cuestionario.js"></script>
<?php
} else {
    header("Location:" . Conectar::ruta() . "index.php");
}
?>