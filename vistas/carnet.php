<?php
require_once("../config/conexion.php");
if (isset($_SESSION["id_usuario"])) {
    require_once("../modelos/Medico.php");
    require_once("../modelos/Pacientes.php");
    require_once("../modelos/Patologia.php");
    require_once("../modelos/Hospital.php");
    require_once("../modelos/Coordinacion.php");

    $medico = new Medico();
    $paciente = new Pacientes();
    $patologia = new Patologia();
    $hospital = new Hospital();
    $coordinacion = new Coordinacion();

    $medicos = $medico->get_medico();
    $pacientes = $paciente->get_paciente();
    $patologias = $patologia->get_patologia();
    $coordinaciones = $coordinacion->get_coordinacion();
    $hospitale = $hospital->get_hospital();
    ?>
    <?php
    require_once("header.php");
    ?>
    <?php if ($_SESSION["carnet"] == 1) {
        ?>
        <!--Contenido-->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
                <div id="resultados_ajax"></div>
                <h2>Listado de Carnet</h2>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h1 class="box-title">
                                    <button class="btn btn-primary btn-lg" id="add_button" onclick="limpiar()" data-toggle="modal" data-target="#categoriaModal"><i class="fa fa-plus" aria-hidden="true"></i> Nueva Carnet</button></h1>
                                <div class="box-tools pull-right">
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <!-- centro -->
                            <div class="panel-body table-responsive">
                                <table id="categoria_data" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="8%">Folio</th>
                                            <th width="8%">Paciente</th>
                                            <th width="8%">Medico</th>
                                            <th width="8%">Patologia</th>
                                            <th width="8%">Hospital</th>
                                            <th width="8%">coordinacion</th>
                                            <th width="8%">Fecha</th>
                                            <th width="8%">Opciones</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <!--Fin centro -->
                        </div><!-- /.box -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </section><!-- /.content -->

        </div><!-- /.content-wrapper -->
        <!--Fin-Contenido-->

        <!--FORMULARIO VENTANA MODAL-->
        <div id="categoriaModal" class="modal fade">
            <div class="modal-dialog">
                <form method="post" id="categoria_form">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Agregar Patologia</h4>
                        </div>
                        <div class="modal-body">
                            <label>Folio</label>
                            <input type="text" name="folio" id="folio" class="form-control" placeholder="Folio" required />
                            <br>
                            <label>Fecha</label>
                            <input type="date" class="form-control" name="fecha_inicio" id="fecha_inicio" required="">
                            <br>
                            <label>Hora inicio</label>
                            <input type="time" class="form-control" name="hora_inicio" id="hora_inicio" required="">
                            <br>
                            <label>Fecha</label>
                            <input type="date" class="form-control" name="fecha_final" id="fecha_final" required="">
                            <br>
                            <label>Hora final</label>
                            <input type="time" class="form-control" name="hora_final" id="hora_final" required="">
                            <br>

                            
                            <label>Nombre de la Paciente</label>
                            <select name="id_paciente" id="id_paciente" class="form-control selectpicker" data-live-search="true">
                                <option value="0">SELECCIONE</option>
                                <?php
                                for ($i = 0; $i < sizeof($pacientes); $i++) {
                                    ?>
                                    <option value="<?php echo $pacientes[$i]["id_paciente"] ?>"><?php echo $pacientes[$i]["nombre"] ?></option>
                                <?php
                            }
                            ?>
                            </select>
                            <br />
                            <br>
                            <label>Nombre de la Medico</label>
                            <select name="id_medico" id="id_medico" class="form-control selectpicker" data-live-search="true">
                                <option value="0">SELECCIONE</option>
                                <?php
                                for ($i = 0; $i < sizeof($medicos); $i++) {
                                    ?>
                                    <option value="<?php echo $medicos[$i]["id_medico"] ?>"><?php echo $medicos[$i]["nombre"] ?></option>
                                <?php
                            }
                            ?>
                            </select>
                            <br />
                            <br>
                            <label>Nombre de la Patologia</label>
                            <select name="id_patologia" id="id_patologia" class="form-control selectpicker" data-live-search="true">
                                <option value="0">SELECCIONE</option>
                                <?php
                                for ($i = 0; $i < sizeof($patologias); $i++) {
                                    ?>
                                    <option value="<?php echo $patologias[$i]["id_patologia"] ?>"><?php echo $patologias[$i]["nombre"] ?></option>
                                <?php
                            }
                            ?>
                            </select>

                            <br />
                            <br />
                            <label>Nombre de la coordinacion</label>
                            <select name="id_coordinacion" id="id_coordinacion" class="form-control selectpicker" data-live-search="true">
                                <option value="0">SELECCIONE</option>
                                <?php
                                for ($i = 0; $i < sizeof($coordinaciones); $i++) {
                                    ?>
                                    <option value="<?php echo $coordinaciones[$i]["id_coordinacion"] ?>"><?php echo $coordinaciones[$i]["nombre"] ?></option>
                                <?php
                            }
                            ?>
                            </select>
                            <br />
                            <br />
                            <label>Nombre de la Hospital</label>
                            <select name="id_hospital" id="id_hospital" class="form-control selectpicker" data-live-search="true">
                                <option value="0">SELECCIONE</option>
                                <?php
                                for ($i = 0; $i < sizeof($hospitale); $i++) {
                                    ?>
                                    <option value="<?php echo $hospitale[$i]["id_hospital"] ?>"><?php echo $hospitale[$i]["nombre"] ?></option>
                                <?php
                            }
                            ?>
                            </select>
                            <br />
                            <br />
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="id_carnet" id="id_carnet" />
                            <button type="submit" name="action" id="btnGuardar" class="btn btn-success pull-left" value="Add"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
                            <button type="button" onclick="limpiar()" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Cerrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!--FIN FORMULARIO VENTANA MODAL-->

    <?php  } else {
    require("noacceso.php");
}
?>
    <!--CIERRE DE SESSION DE PERMISO -->

    <?php
    require_once("footer.php");
    ?>

    <script type="text/javascript" src="js/carnet.js"></script>
<?php
} else {
    header("Location:" . Conectar::ruta() . "index.php");
}
?>