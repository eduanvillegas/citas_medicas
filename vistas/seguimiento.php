<?php
require_once("../config/conexion.php");
if (isset($_SESSION["id_usuario"])) {
    require_once("../modelos/Pacientes.php");
    $paciente = new Pacientes();
    $pacientes = $paciente->get_paciente();
    ?>
<?php
    require_once("header.php");
    ?>
<?php if ($_SESSION["medico"] == 1) {
        ?>
<!--Contenido-->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div id="resultados_ajax"></div>
        <h2>Listado de Seguimiento Antropométrico</h2>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h1 class="box-title">
                            <button class="btn btn-primary btn-lg" id="add_button" onclick="limpiar()" data-toggle="modal" data-target="#categoriaModal"><i class="fa fa-plus" aria-hidden="true"></i> Nuevo Seguimiento</button></h1>
                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive">
                        <table id="categoria_data" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">Fecha</th>
                                    <th width="5%">Peso</th>
                                    <th width="5%">Talla</th>
                                    <th width="5%">C.Cintura</th>
                                    <th width="5%">C.Cadera</th>
                                    <th width="5%">IMC</th>
                                    <th width="5%">OPCIONES</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <!--Fin centro -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

</div><!-- /.content-wrapper -->
<!--Fin-Contenido-->

<!--FORMULARIO VENTANA MODAL-->
<div id="categoriaModal" class="modal fade">
    <div class="modal-dialog">
        <form method="post" id="categoria_form">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Agregar Paciente</h4>
                </div>
                <div class="modal-body">
                <div class="form-group">
                    <label>Nombre del Paciente</label>
                    <select name="id_persona" id="id_persona" class="form-control selectpicker" data-live-search="true">
                        <?php
                                for ($i = 0; $i < sizeof($pacientes); $i++) {
                                    ?>
                        <option value="<?php echo $pacientes[$i]["id_persona"] ?>"><?php echo $pacientes[$i]["nombre"] ?></option>
                        <?php
                                }
                                ?>
                    </select>
                </div>

                    <div class="form-group">
                        <label>Fecha</label>
                        <input type="date" name="fecha" id="fecha" value="<?php echo date("Y-m-d"); ?>" class="form-control" />
                    </div>

                    <div class="form-group">
                        <label>Peso</label>
                        <input type="text" name="peso" id="peso" class="form-control validate" placeholder="Peso" required />
                    </div>

                    <div class="form-group">
                        <label>Talla</label>
                        <input type="text" name="talla" id="talla" class="form-control validate" placeholder="Talla" required />
                    </div>

                    <div class="form-group">
                        <label>C. Cintura</label>
                        <input type="text" name="cintura" id="cintura" class="form-control validate" placeholder="Cintura" required />
                    </div>

                    <div class=" form-group">
                        <label>C. Cadera</label>
                        <input type="text" name="cadera" id="cadera" class="form-control validate" placeholder="Cadera" required />
                    </div>

                    <div class="form-group">
                        <label>IMC</label>
                        <input type="text" name="imc" id="imc" class="form-control" placeholder="IMC" required />
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="hidden" name="id_usuario" id="id_usuario" value="<?php echo $_SESSION["id_usuario"]; ?>" />
                    <input type="hidden" name="id_seguimiento" id="id_seguimiento" />
                    <button type="submit" name="action" id="btnGuardar" class="btn btn-success pull-left" value="Add"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
                    <button type="button" onclick="limpiar()" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Cerrar</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!--FIN FORMULARIO VENTANA MODAL-->

<?php  } else {
        require("noacceso.php");
    }
    ?>
<!--CIERRE DE SESSION DE PERMISO -->

<?php
    require_once("footer.php");
    ?>

<script type="text/javascript" src="js/seguimiento.js"></script>
<?php
} else {
    header("Location:" . Conectar::ruta() . "index.php");
}
?>