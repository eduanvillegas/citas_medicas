
$(document).ready(function () {
  $('#btn_expediente_details').click(function () {
    $('#list_expediente_details').removeClass('active active_tab1');
    $('#list_expediente_details').removeAttr('href data-toggle');
    $('#expediente_details').removeClass('active');
    $('#list_expediente_details').addClass('inactive_tab1');
    $('#list_login_details').removeClass('inactive_tab1');
    $('#list_login_details').addClass('active_tab1 active');
    $('#list_login_details').attr('href', '#login_details');
    $('#list_login_details').attr('data-toggle', 'tab');
    $('#login_details').addClass('active in');
  });

  $('#previous_btn_login_details').click(function () {
    $('#list_login_details').removeClass('active active_tab1');
    $('#list_login_details').removeAttr('href data-toggle');
    $('#login_details').removeClass('active in');
    $('#list_login_details').addClass('inactive_tab1');
    $('#list_expediente_details').removeClass('inactive_tab1');
    $('#list_expediente_details').addClass('active_tab1 active');
    $('#list_expediente_details').attr('href', '#expediente_details');
    $('#list_expediente_details').attr('data-toggle', 'tab');
    $('#expediente_details').addClass('active in');
  });

  $('#btn_login_details').click(function () {
    $('#list_login_details').removeClass('active active_tab1');
    $('#list_login_details').removeAttr('href data-toggle');
    $('#login_details').removeClass('active');
    $('#list_login_details').addClass('inactive_tab1');
    $('#list_personal_details').removeClass('inactive_tab1');
    $('#list_personal_details').addClass('active_tab1 active');
    $('#list_personal_details').attr('href', '#personal_details');
    $('#list_personal_details').attr('data-toggle', 'tab');
    $('#personal_details').addClass('active in');
  });

  $('#previous_btn_personal_details').click(function () {
    $('#list_personal_details').removeClass('active active_tab1');
    $('#list_personal_details').removeAttr('href data-toggle');
    $('#personal_details').removeClass('active in');
    $('#list_personal_details').addClass('inactive_tab1');
    $('#list_login_details').removeClass('inactive_tab1');
    $('#list_login_details').addClass('active_tab1 active');
    $('#list_login_details').attr('href', '#login_details');
    $('#list_login_details').attr('data-toggle', 'tab');
    $('#login_details').addClass('active in');
  });

  $('#btn_personal_details').click(function () {
    $('#list_personal_details').removeClass('active active_tab1');
    $('#list_personal_details').removeAttr('href data-toggle');
    $('#personal_details').removeClass('active');
    $('#list_personal_details').addClass('inactive_tab1');
    $('#list_contact_details').removeClass('inactive_tab1');
    $('#list_contact_details').addClass('active_tab1 active');
    $('#list_contact_details').attr('href', '#contact_details');
    $('#list_contact_details').attr('data-toggle', 'tab');
    $('#contact_details').addClass('active in');
  });

  $('#previous_btn_contact_details').click(function () {
    $('#list_contact_details').removeClass('active active_tab1');
    $('#list_contact_details').removeAttr('href data-toggle');
    $('#contact_details').removeClass('active in');
    $('#list_contact_details').addClass('inactive_tab1');
    $('#list_personal_details').removeClass('inactive_tab1');
    $('#list_personal_details').addClass('active_tab1 active');
    $('#list_personal_details').attr('href', '#personal_details');
    $('#list_personal_details').attr('data-toggle', 'tab');
    $('#personal_details').addClass('active in');
  });

  $('#btn_contact_details').click(function () {
    /*$('#btn_contact_details').attr("disabled", "disabled");
    $(document).css('cursor', 'prgress');
    $("#register_form").submit();*/
    $('#list_contact_details').removeClass('active active_tab1');
    $('#list_contact_details').removeAttr('href data-toggle');
    $('#contact_details').removeClass('active in');
    $('#list_contact_details').addClass('inactive_tab1');
    $('#list_diario_actividades').removeClass('inactive_tab1');
    $('#list_diario_actividades').addClass('active_tab1 active');
    $('#list_diario_actividades').attr('href', '#dactividades_details');
    $('#list_diario_actividades').attr('data-toggle', 'tab');
    $('#dactividades_details').addClass('active in');
  });

  $('#previous_btn_dactividades_details').click(function () {
    $('#list_diario_actividades').removeClass('active active_tab1');
    $('#list_diario_actividades').removeAttr('href data-toggle');
    $('#dactividades_details').removeClass('active in');
    $('#list_diario_actividades').addClass('inactive_tab1');
    $('#list_contact_details').removeClass('inactive_tab1');
    $('#list_contact_details').addClass('active_tab1 active');
    $('#list_contact_details').attr('href', '#contact_details');
    $('#list_contact_details').attr('data-toggle', 'tab');
    $('#contact_details').addClass('active in');
  });

  $('#btn_dactividades_details').click(function () {
    $('#list_diario_actividades').removeClass('active active_tab1');
    $('#list_diario_actividades').removeAttr('href data-toggle');
    $('#dactividades_details').removeClass('active in');
    $('#list_diario_actividades').addClass('inactive_tab1');
    $('#list_consumo').removeClass('inactive_tab1');
    $('#list_consumo').addClass('active_tab1 active');
    $('#list_consumo').attr('href', '#consumo_details');
    $('#list_consumo').attr('data-toggle', 'tab');
    $('#consumo_details').addClass('active in');
  });

  $('#previous_btn_consumo_details').click(function () {
    $('#list_consumo').removeClass('active active_tab1');
    $('#list_consumo').removeAttr('href data-toggle');
    $('#consumo_details').removeClass('active in');
    $('#list_consumo').addClass('inactive_tab1');
    $('#list_diario_actividades').removeClass('inactive_tab1');
    $('#list_diario_actividades').addClass('active_tab1 active');
    $('#list_diario_actividades').attr('href', '#dactividades_details');
    $('#list_diario_actividades').attr('data-toggle', 'tab');
    $('#dactividades_details').addClass('active in');
  });

  $('#btn_consumo_details').click(function () {
    $('#list_consumo').removeClass('active active_tab1');
    $('#list_consumo').removeAttr('href data-toggle');
    $('#consumo_details').removeClass('active in');
    $('#list_consumo').addClass('inactive_tab1');
    $('#list_indicadores_dieteticos').removeClass('inactive_tab1');
    $('#list_indicadores_dieteticos').addClass('active_tab1 active');
    $('#list_indicadores_dieteticos').attr('href', '#indicadores_dieteticos_details');
    $('#list_indicadores_dieteticos').attr('data-toggle', 'tab');
    $('#indicadores_dieteticos_details').addClass('active in');
  });

  $('#previous_btn_indicadores_dieteticos_details').click(function () {
    $('#list_indicadores_dieteticos').removeClass('active active_tab1');
    $('#list_indicadores_dieteticos').removeAttr('href data-toggle');
    $('#indicadores_dieteticos_details').removeClass('active in');
    $('#list_indicadores_dieteticos').addClass('inactive_tab1');
    $('#list_consumo').removeClass('inactive_tab1');
    $('#list_consumo').addClass('active_tab1 active');
    $('#list_consumo').attr('href', '#consumo_details');
    $('#list_consumo').attr('data-toggle', 'tab');
    $('#consumo_details').addClass('active in');
  });

  /* $('#btn_consumo_details').click(function () {
     $('#list_indicadores_dieteticos').removeClass('active active_tab1');
     $('#list_indicadores_dieteticos').removeAttr('href data-toggle');
     $('#indicadores_dieteticos_details').removeClass('active in');
     $('#list_indicadores_dieteticos').addClass('inactive_tab1');
     $('#list_indicadores_dieteticos').removeClass('inactive_tab1');
     $('#list_indicadores_dieteticos').addClass('active_tab1 active');
     $('#list_indicadores_dieteticos').attr('href', '#indicadores_dieteticos_details');
     $('#list_indicadores_dieteticos').attr('data-toggle', 'tab');
     $('#indicadores_dieteticos_details').addClass('active in');
   });
 */


  $("#aginecologicos_form").on("submit", function (e) {
    // alert("hola");
    e.preventDefault(); //No se activará la acción predeterminada del evento
    var formData = new FormData($("#aginecologicos_form")[0]);
    $.ajax({
      url: "../ajax/cuestionario.php?op=guardaraginecologicos",
      type: "POST",
      data: formData,
      contentType: false,
      processData: false,
      success: function (datos) {
        alert(datos);
      }
    });
  })

  $("#consumo_form").on("submit", function (e) {
    // alert("hola");
    e.preventDefault(); //No se activará la acción predeterminada del evento
    var formData = new FormData($("#consumo_form")[0]);
    $.ajax({
      url: "../ajax/cuestionario.php?op=guardarconsumo",
      type: "POST",
      data: formData,
      contentType: false,
      processData: false,
      success: function (datos) {
        alert(datos);
      }
    });
  })

  $("#idieteticos_form").on("submit", function (e) {
    // alert("hola");
    e.preventDefault(); //No se activará la acción predeterminada del evento
    var formData = new FormData($("#idieteticos_form")[0]);
    $.ajax({
      url: "../ajax/cuestionario.php?op=guardaridieteticos",
      type: "POST",
      data: formData,
      contentType: false,
      processData: false,
      success: function (datos) {
        alert(datos);
      }
    });
  })

  $("#dactividades_form").on("submit", function (e) {
    // alert("hola");
    e.preventDefault(); //No se activará la acción predeterminada del evento
    var formData = new FormData($("#dactividades_form")[0]);
    $.ajax({
      url: "../ajax/cuestionario.php?op=guardardactividades",
      type: "POST",
      data: formData,
      contentType: false,
      processData: false,
      success: function (datos) {
        alert(datos);
      }
    });
  })

  $("#afamiliares_form").on("submit", function (e) {
    // alert("hola");
    e.preventDefault(); //No se activará la acción predeterminada del evento
    var formData = new FormData($("#afamiliares_form")[0]);
    $.ajax({
      url: "../ajax/cuestionario.php?op=guardarafamiliares",
      type: "POST",
      data: formData,
      contentType: false,
      processData: false,
      success: function (datos) {
        alert(datos);
      }
    });
  })

  $("#expediente_form").on("submit", function (e) {
    e.preventDefault(); //No se activará la acción predeterminada del evento
    var formData = new FormData($("#expediente_form")[0]);
    $.ajax({
      url: "../ajax/cuestionario.php?op=guardarexpediente",
      type: "POST",
      data: formData,
      contentType: false,
      processData: false,
      success: function (datos) {
        datos = JSON.parse(datos);
        $('#expediente').val(datos.expediente);
        $("#id_persona1").val(datos.id_expediente);
        $("#id_persona2").val(datos.id_expediente);
        $("#id_persona3").val(datos.id_expediente);
        $("#id_persona4").val(datos.id_expediente);
        $("#id_persona5").val(datos.id_expediente);
        $("#id_persona6").val(datos.id_expediente);
      }
    });
  })

  //cuando se da click al boton submit entonces se ejecuta la funcion guardaryeditar(e);
  $("#register_form").on("submit", function (e) {
    e.preventDefault(); //No se activará la acción predeterminada del evento
    var formData = new FormData($("#register_form")[0]);
    $.ajax({
      url: "../ajax/cuestionario.php?op=guardaryeditar",
      type: "POST",
      data: formData,
      contentType: false,
      processData: false,
      success: function (datos) {
        alert(datos);
      }
    });
  })

});
//funcion para generar folio automaticamente
function CargarID(id) {
  //alert(id);
  $("#id_persona1").val(id);
  $("#id_persona2").val(id);
  $("#id_persona3").val(id);
  $("#id_persona4").val(id);
  $("#id_persona5").val(id);
  $("#id_persona6").val(id);
}