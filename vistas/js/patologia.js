var tabla;

//Función que se ejecuta al inicio
function init() {
    listar();
    //cuando se da click al boton submit entonces se ejecuta la funcion guardaryeditar(e);
    $("#categoria_form").on("submit", function (e) {
        guardaryeditar(e);
    })

    //cambia el titulo de la ventana modal cuando se da click al boton
    $("#add_button").click(function () {
        //habilita los campos cuando se agrega un registro nuevo ya que cuando se editaba un registro asociado entonces aparecia deshabilitado los campos
        $("#categoria").attr('disabled', false);
        $(".modal-title").text("Agregar Patologia");
    });
}

//Función limpiar
/*IMPORTANTE: no limpiar el campo oculto del id_usuario, sino no se registra
el medico*/
function limpiar() {
    $('#nombre').val("");
    $('#id_patologia').val("");
}

//Función Listar
function listar() {
    tabla = $('#categoria_data').dataTable({
        "aProcessing": true, //Activamos el procesamiento del datatables
        "aServerSide": true, //Paginación y filtrado realizados por el servidor
        dom: 'Bfrtip', //Definimos los elementos del control de tabla
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax": {
            url: '../ajax/patologia.php?op=listar',
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "responsive": true,
        "bInfo": true,
        "iDisplayLength": 10, //Por cada 10 registros hace una paginación
        "order": [[0, "desc"]], //Ordenar (columna,orden)

        "language": {

            "sProcessing": "Procesando...",

            "sLengthMenu": "Mostrar _MENU_ registros",

            "sZeroRecords": "No se encontraron resultados",

            "sEmptyTable": "Ningún dato disponible en esta tabla",

            "sInfo": "Mostrando un total de _TOTAL_ registros",

            "sInfoEmpty": "Mostrando un total de 0 registros",

            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",

            "sInfoPostFix": "",

            "sSearch": "Buscar:",

            "sUrl": "",

            "sInfoThousands": ",",

            "sLoadingRecords": "Cargando...",

            "oPaginate": {

                "sFirst": "Primero",

                "sLast": "Último",

                "sNext": "Siguiente",

                "sPrevious": "Anterior"

            },

            "oAria": {

                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",

                "sSortDescending": ": Activar para ordenar la columna de manera descendente"

            }

        } //cerrando language

    }).DataTable();
}

//Mostrar datos del medico en la ventana modal 
function mostrar(id_patologia) {
    $.post("../ajax/patologia.php?op=mostrar", {
        id_patologia: id_patologia
    }, function (data, status) {
        data = JSON.parse(data);

        $('#categoriaModal').modal('show');
        $('.modal-title').text("Editar Paciente");
        $('#id_patologia').val(id_patologia);
        $('#nombre').val(data.nombre);
        $('#estado').val(data.estado);
    });
}


//la funcion guardaryeditar(e); se llama cuando se da click al boton submit
function guardaryeditar(e) {
    e.preventDefault(); //No se activará la acción predeterminada del evento
    var formData = new FormData($("#categoria_form")[0]);

    $.ajax({
        url: "../ajax/patologia.php?op=guardaryeditar",
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            /*bootbox.alert(datos);
            mostrarform(false);
            tabla.ajax.reload();*/

            //alert(datos);

            /*imprimir consulta en la consola debes hacer un print_r($_POST) al final del metodo 
               y si se muestran los valores es que esta bien, y se puede imprimir la consulta desde el metodo
               y se puede ver en la consola o desde el mensaje de alerta luego pegar la consulta en phpmyadmin*/
            //console.log(datos);

            $('#categoria_form')[0].reset();
            $('#categoriaModal').modal('hide');

            $('#resultados_ajax').html(datos);
            $('#categoria_data').DataTable().ajax.reload();

            limpiar();
        }

    });

}


//EDITAR ESTADO DE EL MEDICO
//importante:id_medico, est se envia por post via ajax
function cambiarEstado(id_patologia, est) {
    swal({
        title: "¿Estás seguro?",
        text: "¡Deseas cambiar de estado!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: '!Sí, estoy seguro!',
        cancelButtonText: "No, cancelalo!",
        closeOnConfirm: false
    },
        function () {
            $.ajax({
                url: "../ajax/patologia.php?op=activarydesactivar",
                method: "POST",
                //data:dataString,i
                //toma el valor del id y del estado
                data: {
                    id_patologia: id_patologia,
                    est: est
                },
                //cache: false,
                //dataType:"html",
                success: function (data) {
                    $("#resultados_ajax").html(data);
                    $('#categoria_data').DataTable().ajax.reload();
                }
            });
        });
}

//ELIMINAR MEDICO

function eliminar(id_patologia) {
    //IMPORTANTE: asi se imprime el valor de una funcion
    swal({
        title: "¿Estás seguro?",
        text: "¡Deseas eliminar la patologia!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: '!Sí, estoy seguro!',
        cancelButtonText: "No, cancelalo!",
        closeOnConfirm: false
    },
        function () {
            $.ajax({
                url: "../ajax/patologia.php?op=eliminar_patologia",
                method: "POST",
                data: {
                    id_patologia: id_patologia
                },
                success: function (data) {
                    //alert(data);
                    $("#resultados_ajax").html(data);
                    $("#categoria_data").DataTable().ajax.reload();
                }
            });
            // swal("Deleted!", "Your imaginary file has been deleted.", "success");
        });


    /*bootbox.confirm("¿Está Seguro de eliminar el Paciente?", function (result) {
        if (result) {

            $.ajax({
                url: "../ajax/categoria.php?op=eliminar_categoria",
                method: "POST",
                data: {
                    id_paciente: id_paciente
                },
                success: function (data) {
                    //alert(data);
                    $("#resultados_ajax").html(data);
                    $("#categoria_data").DataTable().ajax.reload();
                }
            });
        }
    }); //bootbox*/
}



init();
