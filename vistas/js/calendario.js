$(document).ready(function(){
    var calendar = $('#calendar').fullCalendar({  // assign calendar
        header:{
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        defaultView: 'agendaWeek',
        editable: true,
        selectable: true,
        allDaySlot: false,
        
        events: "../ajax/calendario.php?op=load",  // request to load current events

        
        eventClick:  function(event, jsEvent, view) {  // when some one click on any event
            endtime = $.fullCalendar.moment(event.end).format('h:mm');
            starttime = $.fullCalendar.moment(event.start).format('dddd, MMMM Do YYYY, h:mm');
            var mywhen = starttime + ' - ' + endtime;
            $('#modalTitle').html(event.title);
            $('#modalWhen').text(mywhen);
            $('#eventID').val(event.id);
            $('#calendarModal').modal();
        },
        
        select: function(start, end, jsEvent) {  // click on empty time slot
            endtime = $.fullCalendar.moment(end).format('h:mm');
            starttime = $.fullCalendar.moment(start).format('dddd, MMMM Do YYYY, h:mm');
            var mywhen = starttime + ' - ' + endtime;
            start = moment(start).format();
            end = moment(end).format();
            $('#createEventModal #startTime').val(start);
            $('#createEventModal #endTime').val(end);
            $('#createEventModal #when').text(mywhen);
            $('#createEventModal').modal('toggle');
       },
       eventDrop: function(event, delta){ // event drag and drop
           $.ajax({
               url: '../ajax/calendario.php?op=actualizar',
               data: { title: event.title, start: moment(event.start).format(), end: moment(event.end).format(), id: event.id },
               type: "POST",
               success: function(json) {
               //alert(json);
               }
           });
       },
       eventResize: function(event) {  // resize to increase or decrease time of event
           $.ajax({
               url: '../ajax/calendario.php?op=actualizar',
               data: { title: event.title, start: moment(event.start).format(), end: moment(event.end).format(), id: event.id },
               type: "POST",
               success: function(json) {
                   //alert(json);
               }
           });
       }
    });
           
   $('#submitButton').on('click', function(e){ // add event submit
       // We don't want this to act as a link so cancel the link action
       e.preventDefault();
       doSubmit(); // send to form submit function
   });
   
   $('#deleteButton').on('click', function(e){ // delete event clicked
       // We don't want this to act as a link so cancel the link action
       e.preventDefault();
       doDelete(); //send data to delete function
   });
   
   function doDelete(){  // delete event 
       $("#calendarModal").modal('hide');
       var eventID = $('#eventID').val();
       $.ajax({
           url: '../ajax/calendario.php?op=eliminar',
           data: { id:eventID},
           type: "POST",
           success: function(json) {
               if(json == 1)
                    $("#calendar").fullCalendar('removeEvents',eventID);
               else
                    return false;
           }
       });
   }
   function doSubmit(){ // add event
       $("#createEventModal").modal('hide');
       var title = $('#title').val();
       var startTime = $('#startTime').val();
       var endTime = $('#endTime').val();
       
       $.ajax({
           url: '../ajax/calendario.php?op=guardar',
           data: 'action=add&title='+title+'&start='+startTime+'&end='+endTime,
           type: "POST",
           success: function(json) {
               $("#calendar").fullCalendar('renderEvent',
               {
                   id: json.id,
                   title: title,
                   start: startTime,
                   end: endTime,
               },
               true);
           }
       });
   }
});
/*$(document).ready(function () {
    
    var calendar = $('#calendar').fullCalendar({
        editable: true,
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        events: '../ajax/calendario.php?op=load',
        selectable: true,
        selectHelper: true,
        select: function (start, end, allDay) {
            var title = prompt("Ingresa Titulo de la Cita");
            if (title) {
                 var start = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
                var end = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");
                $.ajax({
                    url: "../ajax/calendario.php?op=guardar",
                    type: "POST",
                    data: { title: title, start: start, end: end },
                    success: function (datos) {
                        calendar.fullCalendar('refetchEvents');
                        alert("Cita Agregada");
                    }
                })
            }
        },
        editable: true,
        eventResize: function (event) {
            var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
            var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
            var title = event.title;
            var id = event.id;
            $.ajax({
                url: "../ajax/calendario.php?op=actualizar",
                type: "POST",
                data: { title: title, start: start, end: end, id: id },
                success: function () {
                    calendar.fullCalendar('refetchEvents');
                    alert('Cita Actualizada');
                }
            })
        },
        editable: true,
        eventDrop: function (event) {
            var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
            var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
            var title = event.title;
            var id = event.id;
            $.ajax({
                url: "../ajax/calendario.php?op=actualizar",
                type: "POST",
                data: { title: title, start: start, end: end, id: id },
                success: function () {
                    calendar.fullCalendar('refetchEvents');
                    alert("Actualizada");
                }
            });
        },
        eventClick: function (event) {
            if (confirm("Deseas eliminar la cita?")) {
                var id = event.id;
                $.ajax({
                    url: "../ajax/calendario.php?op=eliminar",
                    type: "POST",
                    data: { id: id },
                    success: function () {
                        calendar.fullCalendar('refetchEvents');
                        alert("cita eliminada");
                    }
                })
            }
        },
    });
});*/
