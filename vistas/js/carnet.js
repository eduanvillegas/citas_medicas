var tabla;

//Función que se ejecuta al inicio
function init() {
    listar();
    //cuando se da click al boton submit entonces se ejecuta la funcion guardaryeditar(e);
    $("#categoria_form").on("submit", function (e) {
        guardaryeditar(e);
    })

    //cambia el titulo de la ventana modal cuando se da click al boton
    $("#add_button").click(function () {
        //habilita los campos cuando se agrega un registro nuevo ya que cuando se editaba un registro asociado entonces aparecia deshabilitado los campos
        $("#categoria").attr('disabled', false);
        $(".modal-title").text("Agregar Carnet");
    });
}


//Función limpiar
/*IMPORTANTE: no limpiar el campo oculto del id_usuario, sino no se registra
la categoria*/
function limpiar() {
    $("#folio").val('');
    $("#id_paciente").val('0');
    $('#id_paciente').change();
    $("#id_medico").val('0');
    $('#id_medico').change();
    $("#id_patologia").val('0');
    $('#id_patologia').change();
    $("#id_coordinacion").val('0');
    $('#id_coordinacion').change();
    $("#id_hospital").val('0');
    $('#id_hospital').change();
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var ano = ("0" + (now.getFullYear())).slice(-2); //obteniendo año
    var today = now.getFullYear() + "-" + (month) + "-" + (day);
    $('#fecha').val(today);
}

//Función Listar
function listar() {
    tabla = $('#categoria_data').dataTable({
        "aProcessing": true, //Activamos el procesamiento del datatables
        "aServerSide": true, //Paginación y filtrado realizados por el servidor
        dom: 'Bfrtip', //Definimos los elementos del control de tabla
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax": {
            url: '../ajax/carnet.php?op=listar',
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "responsive": true,
        "bInfo": true,
        "iDisplayLength": 10, //Por cada 10 registros hace una paginación
        "order": [[0, "desc"]], //Ordenar (columna,orden)

        "language": {

            "sProcessing": "Procesando...",

            "sLengthMenu": "Mostrar _MENU_ registros",

            "sZeroRecords": "No se encontraron resultados",

            "sEmptyTable": "Ningún dato disponible en esta tabla",

            "sInfo": "Mostrando un total de _TOTAL_ registros",

            "sInfoEmpty": "Mostrando un total de 0 registros",

            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",

            "sInfoPostFix": "",

            "sSearch": "Buscar:",

            "sUrl": "",

            "sInfoThousands": ",",

            "sLoadingRecords": "Cargando...",

            "oPaginate": {

                "sFirst": "Primero",

                "sLast": "Último",

                "sNext": "Siguiente",

                "sPrevious": "Anterior"

            },

            "oAria": {

                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",

                "sSortDescending": ": Activar para ordenar la columna de manera descendente"

            }

        } //cerrando language

    }).DataTable();
}

//Mostrar datos de la categoria en la ventana modal 
function mostrar(id_carnet) {
    $.post("../ajax/carnet.php?op=mostrar", {
        id_carnet: id_carnet
    }, function (data, status) {
        data = JSON.parse(data);

        $('#categoriaModal').modal('show');
        $('.modal-title').text("Editar Carnet");
        
        $('#id_carnet').val(id_carnet);
        $('#folio').val(data.folio);
        $('#fecha').val(data.fecha);
        $('#id_paciente').val(data.id_paciente);
        $('#id_paciente').change();
        $('#id_medico').val(data.id_medico);
        $('#id_medico').change();
        $('#id_patologia').val(data.id_patologia);
        $('#id_patologia').change();
        $('#id_coordinacion').val(data.id_coordinacion);
        $('#id_coordinacion').change();
        $('#id_hospital').val(data.id_hospital);
        $('#id_hospital').change();
    });
}


//la funcion guardaryeditar(e); se llama cuando se da click al boton submit
function guardaryeditar(e) {
    e.preventDefault(); //No se activará la acción predeterminada del evento
    var formData = new FormData($("#categoria_form")[0]);
    ///alert(formData);
    $.ajax({
        url: "../ajax/carnet.php?op=guardaryeditar",
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            /*bootbox.alert(datos);
            mostrarform(false);
            tabla.ajax.reload();*/

            //alert(datos);

            /*imprimir consulta en la consola debes hacer un print_r($_POST) al final del metodo 
               y si se muestran los valores es que esta bien, y se puede imprimir la consulta desde el metodo
               y se puede ver en la consola o desde el mensaje de alerta luego pegar la consulta en phpmyadmin*/
            console.log(datos);

            $('#categoria_form')[0].reset();
            $('#categoriaModal').modal('hide');

            $('#resultados_ajax').html(datos);
            $('#categoria_data').DataTable().ajax.reload();

            limpiar();
        }
    });
}


//EDITAR ESTADO DE LA CATEGORIA
//importante:id_categoria, est se envia por post via ajax


function cambiarEstado(id_paciente, est) {

  swal({
            title: "¿Estás seguro?",
            text: "¡Deseas cambiar de estado!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: '!Sí, estoy seguro!',
            cancelButtonText: "No, cancelalo!",
            closeOnConfirm: false
        },
        function () {
             $.ajax({
                url: "../ajax/paciente.php?op=activarydesactivar",
                method: "POST",
                //data:dataString,
                //toma el valor del id y del estado
                data: {
                    id_paciente: id_paciente,
                    est: est
                },
                //cache: false,
                //dataType:"html",
                success: function (data) {
                    $("#resultados_ajax").html(data);
                    $('#categoria_data').DataTable().ajax.reload();

                }

            });
            // swal("Deleted!", "Your imaginary file has been deleted.", "success");
        });
/*
    bootbox.confirm("¿Está Seguro de cambiar de estado?", function (result) {
        if (result) {
            $.ajax({
                url: "../ajax/paciente.php?op=activarydesactivar",
                method: "POST",
                //data:dataString,
                //toma el valor del id y del estado
                data: {
                    id_paciente: id_paciente,
                    est: est
                },
                //cache: false,
                //dataType:"html",
                success: function (data) {

                    $('#categoria_data').DataTable().ajax.reload();

                }

            });

        }

    }); //bootbox
*/
}

//ELIMINAR CATEGORIA

function eliminar(id_carnet) {

    swal({
        title: "¿Estás seguro?",
        text: "¡Deseas eliminar el Carnet!",
        type: "warning",showCancelButton: true,
        confirmButtonClass: "btn-danger",
         confirmButtonText: '!Sí, estoy seguro!',
        cancelButtonText: "No, cancelalo!",
        closeOnConfirm: false
    },
         function(){
        $.ajax({
            url: "../ajax/carnet.php?op=eliminar_carnet",
            method: "POST",
            data: {
                id_carnet: id_carnet
            },
            success: function (data) {
                //alert(data);
                $("#resultados_ajax").html(data);
                $("#categoria_data").DataTable().ajax.reload();
            }
        });
        // swal("Deleted!", "Your imaginary file has been deleted.", "success");
    });
    //IMPORTANTE: asi se imprime el valor de una funcion
    //alert(categoria_id);
    /*swal({
        title: "¿Estás seguro?",
        text: "¡Deseas eliminar el paciente!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: '!Sí, estoy seguro!',
        cancelButtonText: "No, cancelalo!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
         function(isConfirm){

        if (isConfirm){
            $.ajax({
                url: "../ajax/categoria.php?op=eliminar_categoria",
                method: "POST",
                data: {
                    id_paciente: id_paciente
                },
                success: function (data) {
                    //alert(data);
                    $("#resultados_ajax").html(data);
                    $("#categoria_data").DataTable().ajax.reload();
                }
            });

        } 
        else {
            swal("Cancelado", "Sea cancelado correctamente" , "error");
            e.preventDefault();
        }
    });*/

    /*bootbox.confirm("¿Está Seguro de eliminar el Paciente?", function (result) {
        if (result) {

            $.ajax({
                url: "../ajax/categoria.php?op=eliminar_categoria",
                method: "POST",
                data: {
                    id_paciente: id_paciente
                },
                success: function (data) {
                    //alert(data);
                    $("#resultados_ajax").html(data);
                    $("#categoria_data").DataTable().ajax.reload();
                }
            });
        }
    }); //bootbox*/
}



init();
