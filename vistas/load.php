<?php
//load.php
$connect = new PDO('mysql:host=localhost;dbname=carnet_citas', 'root', '');
$data = array();
$query = "SELECT * FROM citas";
$statement = $connect->prepare($query);
$statement->execute();
$result = $statement->fetchAll();
foreach($result as $row)
{
 $data[] = array(
  'id'   => $row["folio_citas"],
  'title'   => $row["folio_carnet"]
 );
}
echo json_encode($data);
