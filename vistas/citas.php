<?php
require_once("../config/conexion.php");
if (isset($_SESSION["id_usuario"])) {
    require_once("../modelos/Carnet.php");
    $carnet = new Carnet();
    $carnets = $carnet->get_carnet();
    ?>

    <?php
    require_once("header.php");
    ?>
    <?php if ($_SESSION["hospital"] == 1) {
        ?>
        <!--Contenido-->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
                <div id="resultados_ajax"></div>
                <h2>Listado de Citas</h2>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h1 class="box-title">
                                    <button class="btn btn-primary btn-lg" id="add_button" onclick="limpiar()" data-toggle="modal" data-target="#categoriaModal"><i class="fa fa-plus" aria-hidden="true"></i> Nueva cita</button></h1>
                                <div class="box-tools pull-right">
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <!-- centro -->
                            <div class="panel-body table-responsive">
                                <table id="categoria_data" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="20%">Folio</th>
                                            <th width="20%">Fecha</th>
                                            <th width="10%">OCT_OD</th>
                                            <th width="10%">OCT_OI</th>
                                            <th width="10%">AV_OD</th>
                                            <th width="10%">AV_OI</th>
                                            <th width="10%">APLICACION OD</th>
                                            <th width="10%">APLICACION OI</th>
                                            <th width="10%">LASER OD</th>
                                            <th width="10%">LASER OD</th>
                                            <th width="10%">COMENTARIOS</th>
                                            <th width="10%">OPCIONES</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <!--Fin centro -->
                        </div><!-- /.box -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </section><!-- /.content -->

        </div><!-- /.content-wrapper -->
        <!--Fin-Contenido-->

        <!--FORMULARIO VENTANA MODAL-->
        <div id="categoriaModal" class="modal fade">
            <div class="modal-dialog">
                <form method="post" id="categoria_form">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Agregar Citas</h4>
                        </div>
                        <div class="modal-body">
                        <label>Carnet</label>
                            <select name="id_carnet" id="id_carnet" class="form-control selectpicker" data-live-search="true">
                                <option value="0">SELECCIONE</option>
                                <?php
                                for ($i = 0; $i < sizeof($carnets); $i++) {
                                    ?>
                                    <option value="<?php echo $carnets[$i]["id_carnet"] ?>"><?php echo $carnets[$i]["folio"] ?></option>
                                <?php
                            }
                            ?>
                            </select>
                            <br />
                            <br>

                            <label>Fecha de cita</label>
                            <input type="date" name="fecha" id="fecha" class="form-control" placeholder="" required />
                            <br />

                            <label>OCT_OD</label>
                            <input type="text" name="oct_od" id="oct_od" class="form-control" placeholder="" required />
                            <br />

                            <label>OCT_OI</label>
                            <input type="text" name="oct_oi" id="oct_oi" class="form-control" placeholder="" required />
                            <br />

                            <label>AV_OD</label>
                            <input type="text" name="av_od" id="av_od" class="form-control" placeholder="" required />
                            <br />

                            <label>AV_OI</label>
                            <input type="text" name="av_oi" id="av_oi" class="form-control" placeholder="" required />
                            <br />

                            <label>APLICACION OD</label>
                            <input type="text" name="ap_od" id="ap_od" class="form-control" placeholder="" required />
                            <br />

                            <label>APLICACION OI</label>
                            <input type="text" name="ap_oi" id="ap_oi" class="form-control" placeholder="" required />
                            <br />

                            <label>LASER OD</label>
                            <input type="text" name="laser_od" id="laser_od" class="form-control" placeholder="" required />
                            <br />

                            <label>LASER OI</label>
                            <input type="text" name="laser_oi" id="laser_oi" class="form-control" placeholder="" required />
                            <br />

                            <label>COMENTARIOS</label>
                            <input type="text" name="comentarios" id="comentarios" class="form-control" placeholder="Comentarios" required />
                            <br />
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="folio_citas" id="folio_citas" />
                            <button type="submit" name="action" id="btnGuardar" class="btn btn-success pull-left" value="Add"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
                            <button type="button" onclick="limpiar()" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Cerrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!--FIN FORMULARIO VENTANA MODAL-->

    <?php  } else {
    require("noacceso.php");
}
?>
    <!--CIERRE DE SESSION DE PERMISO -->

    <?php
    require_once("footer.php");
    ?>

    <script type="text/javascript" src="js/citas.js"></script>
<?php
} else {
    header("Location:" . Conectar::ruta() . "index.php");
}
?>